
source ./pyenv/bin/activate

echo "killing old webapp process."
pid=`cat webapp.pid`
kill -9 $pid

pkill celery
nohup celery -A webapp.celery worker --loglevel=INFO 1>> ./celery_stdout.log 2>> ./celery_stderr.log &

nohup python webapp.py 1>> ./app_stdout.log 2>> ./app_stderr.log &
echo $! > webapp.pid


