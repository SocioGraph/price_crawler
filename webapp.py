from flask import Flask, request, jsonify, flash, redirect, render_template, url_for
from flask_cors import CORS
from celery import Celery
import os, json, string, random, csv, requests, datetime, subprocess

from werkzeug.utils import secure_filename

crawler_path = os.path.dirname(os.path.realpath(__file__))

KEY = "key-5029bb836fa6cc7b062b9abcca868301"
DOMAIN = "https://api.mailgun.net/v3/mg.sociographsolutions.in/messages"


UPLOAD_FOLDER_PREFIX = './inputFiles'

ALLOWED_EXTENSIONS = set(['csv', 'xlsx'])

app = Flask(__name__)
CORS(app, resources={r"/uploads/*": {"origins": "*"}})

app.config['CELERY_BROKER_URL'] = 'redis://{}/0'.format(os.environ.get  ("REDIS_URL",'0.0.0.0:6379'))
app.secret_key = 'super345secret#$%^key'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER_PREFIX

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)

validComparisons =  ["lower", "greater", "equal"]

def get_upload_dir_name():
    upload_dir = f"{UPLOAD_FOLDER_PREFIX}/{datetime.datetime.now().strftime('%d-%m-%Y')}"

    if not os.path.isdir(upload_dir):
        os.makedirs(upload_dir)

    return upload_dir

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

@celery.task
def start_crawling():
    print("Starting crawling...")
    bashCommand = f"bash trigger_orchestration.sh"
    process = subprocess.Popen(bashCommand.split()) #, stdout=subprocess.PIPE)
    # output, error = process.communicate()
    # print("Out",output)
    # print("Err",error)

@celery.task
def processData(task):
    print(f"processing task: {task}")
    data = None
    categories = []
    reqComparisons = []
    processData = []
    min_price = task["min_price"]
    max_price = task["max_price"]
    location = task["location"]
    
    #opening data file
    with open("./allFrontendData.json") as fp:
        data = json.loads(fp.read())
   
    if task["category"] == "all":
        categories = data["categories"]
    else:
        categories.append(task["category"])

    if task["comparison"] == "all":
        reqComparisons = validComparisons
    else:
        reqComparisons.append(task["comparison"])

    random_string = id_generator()
    outputFilePath = f"./mailFiles/{random_string}"

    if not os.path.isdir(outputFilePath):
        print(f"Creating dir {outputFilePath}.")
        os.mkdir(outputFilePath)

    for category in categories:
        attachFiles = []
        for competitor in data["competitors"]:
            for comparison in reqComparisons:
                print(competitor)
                to_csv_data = []
                processData = data["data"][task["location"]]["comparison_data"][f"spar_{comparison}"][category][f"{competitor}_json"]
        
                if min_price != 'default' and max_price != 'default':
                    print(f"Filtering for products between {min_price} and {max_price}")
                    for product in processData:
                        print(product)
                        if float(product["competitor_product_price"]) > float(min_price) and float(product["competitor_product_price"]) < float(max_price):
                            to_csv_data.append(product)
                else:
                    to_csv_data = processData

                if not len(processData):
                    continue


                outFileName = f"{outputFilePath}/{competitor}_{category}_{location}_{comparison}_products.csv"
        

                with open(outFileName, "w") as wf:
                    print(f"Writing to CSV file. {outFileName}")
                    #print(json.dumps(output, indent = 4))
                    csv_writer = csv.DictWriter(wf, processData[0].keys())
                    csv_writer.writeheader()
                    csv_writer.writerows(to_csv_data)

                attachFiles.append(("attachment", open(outFileName)))

    print("Sending Emails")
    data = {
                "from": "insights@iamdave.ai",
                "to": task["email"],
                "subject": "Spar Insights - Reports",
                "html" : "<b>Please find attached the requested reports.</b>"
                }

    m=requests.post(DOMAIN, auth=('api', KEY), data=data, files = attachFiles or [])
    print(m.status_code, m.text)

    #print(json.dumps(processData, indent = 4))
    return "result"

@app.route('/filter', methods = ['GET'])
def filterProducts():
    returnStatus = {}
    print(request.args)
    if not 'min_price' in request.args:
        min_price = 'default'
    else:
        min_price =  request.args.get('min_price')

    if not 'max_price' in request.args:
        max_price = 'default'
    else:
        max_price =  request.args.get('max_price')

    if not 'category' in request.args:
        category = 'all'
    else:
        category = request.args.get('category')

    if not 'location' in request.args:
        returnStatus["status"] = "failed"
        returnStatus["message"] = "location required"
        return jsonify(returnStatus)
    else:
        location = request.args.get('location')


    if not 'comparison' in request.args:
        comparison = 'all'
    else:
        comparison = request.args.get('comparison')

        if not comparison in validComparisons:
            returnStatus["status"] = "failed"
            returnStatus["message"] = "wrong comparison keyword"
            return jsonify(returnStatus)
        

    if not 'email' in request.args:
        returnStatus["status"] = "failed"
        returnStatus["message"] = "email required"
        return jsonify(returnStatus)
    else:
        email = request.args.get("email")

        if not email:
            returnStatus["status"] = "failed"
            returnStatus["message"] = "email required"
            return jsonify(returnStatus)


    task = {
            "task" : "filter",
            "min_price" : min_price,
            "max_price" : max_price,
            "category" : category,
            "comparison" : comparison,
            "status" : "TASK_PLACED", 
            "email": email,
            "location" : location
            }

    processData.delay(task)

    return jsonify(task)


@app.route('/uploads', methods=['GET', 'POST'])
def upload_file():
    files_to_upload = {"product_data":"64_products_data.csv", "csp":"64_products_csp.xlsx"}

    delete_keys = []
    for key, value in files_to_upload.items():
        if os.path.isfile(f"{get_upload_dir_name()}/{value}"):
            delete_keys.append(key)
    
    for key in delete_keys:
        del files_to_upload[key]

    if not files_to_upload:
        return "<h1>No files to be uploaded.</h1>", 200

    if request.method == 'POST':
        # check if the post request has the file part
        print(request.files)
        if 'file' not in request.files:
            print('No file part')
            return 'No file part' #redirect(request.url)

        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)

        if file and allowed_file(file.filename):
            save_filename = secure_filename(file.filename)
            ext = save_filename.split(".")[-1]
            if ext == 'csv':
                del files_to_upload["product_data"]
                save_filename = '64_products_data.csv'
            elif ext == 'xlsx':
                del files_to_upload["csp"]
                save_filename = '64_products_csp.xlsx'

            save_filename = os.path.join(get_upload_dir_name(), save_filename)    
            file.save(save_filename)

            if not files_to_upload:
                start_crawling.delay()
                return "<h1>Starting Crawler</h1>"

            return '<h1>Upload Success</h1> <br> <a href = "/uploads"><strong>Uploads Page.</strong></a>' #redirect(url_for('upload_file',filename=filename))
    return f'''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload {list(files_to_upload.keys())} File</h1>
    <form method=post enctype=multipart/form-data>
      <input type=file name=file>
      <input type=submit value=Upload>
    </form>
    '''


if __name__ == '__main__':
    app.run(host = '0.0.0.0', port = 5040, debug = True)
