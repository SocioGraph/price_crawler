import time, re, json
from crawler_tools import CrawlerTools

class CrawlerLogic(CrawlerTools):

    def crawl(self):
        soup = self.updatePageElements()
        driver = self.driver
        
        product_name_price_map = {}
        product_map = {}
        products_list = []
        #print(len(soup.findAll("div", attrs = {"id":"listing_container"})))
    
        for container in soup.findAll("div", attrs = {"id":"listing_container"}):
            #print(container)
            prev_product_card_length = 0
            equatedCount = 0
            for i in range(100):
                product_card_length = driver.execute_script("""return(document.getElementsByClassName("product-listing-row")[0].childElementCount)""")
                print (product_card_length, prev_product_card_length)
                if product_card_length == prev_product_card_length:
                    equatedCount =  equatedCount + 1
                    print("Same product count {} times.".format(equatedCount))
                    if equatedCount > 6:
                        break
                else:
                    equatedCount = 0
                prev_product_card_length = product_card_length
                print ("scrolling")
                driver.execute_script("window.scrollTo(0, {})".format(1080+(i*4000))) 
                bounding_box = driver.execute_script("""return(document.getElementById("locationModal").getBoundingClientRect());""")
                
                if bounding_box["height"] == 0:
                    pass
                else:
                    driver.execute_script("""document.getElementById("locationModal").querySelector("a").click()""")
                time.sleep(10)
        
        soup = self.updatePageElements()
        #print(len(soup.findAll("div", attrs = {"class":"product-listing-row"})[0].))
        for container in soup.findAll("div", attrs = {"class":"product-listing-row"}):
            product_map = {}
            products_list = []
            for product_card in container.findAll("div", attrs = {"class": "js-switch-view"}):
                product_block = product_card.find("div", attrs = {"class":"product-listing-item"}).find("div", attrs = {"class":"product-listing-item__primary"})
                #print(product_block)
                try:
                    image_url = product_block.find("a").find("img", attrs = {"class":"imgProduct"})["src"]
                    #product_item_details = product_block.find("div", attrs = {"class":"product-item-details"})
                    product_title_item = product_block.find("h4", attrs = {"class":"product-listing--title"}).find("a")
                    product_title_original =  product_title_item.text.strip().split(":")
                    product_link = "https://dmart.in"+product_title_item["href"]
                    product_title = product_title_original[0]
                    product_quantity = product_title_original.pop()
                    product_title_striped = " ".join(product_title)
                    #product_quantity = product_title_original.split(" ")[-1]
                    product_listing_details_item = product_block.find("div", attrs = {"class":"product-listing-details"})
    
                    product_price = product_listing_details_item.find("p", attrs = {"class":"product-listing--original-price"}).find("span").text.strip()
    
                    product_selling_price = product_listing_details_item.find("p", attrs = {"class":"product-listing--discounted-price"}).text.split(" ")[1]
    
                    price_difference = product_listing_details_item.find("div", attrs = {"class":"product-listing__save"}).find("span").find("span").text
    
                    print (product_title, product_quantity, image_url, product_link, product_price, product_selling_price, price_difference)
                except Exception as e:
                    print(e)
                    print(product_card)
                    continue
                
                product_map["offer_percentage"] = ""
                product_map["product_title"] = product_title
                product_map["product_quantity"] = product_quantity
                product_map["product_selling_price"] = product_selling_price
                product_map["product_price"] = product_price
                product_map["image_url"] = image_url
                product_map["product_link"] = product_link
                product_map["price_difference"] = price_difference
                               
                products_list.append(product_map)
    
                product_name_price_map[product_map["product_title"]+" "+product_map["product_quantity"]] = [ 
                        float(product_map["product_selling_price"]),
                        float(product_map["product_price"])
                        ]
    
                product_map = {}
       
        #closing the open chromium window.
        driver.close()
        return json.dumps(products_list)
    
    def selectCity(soup):
        #citySelector = soup.find("header", attrs = {"class":"header"}).find("div", attrs = {"class":"header__left"}).find("a", attrs = {"class":"header-item--address"}).find("div", attrs = {"class":"user-address"})
        #print(driver.find_element_by_class_name('location').findAll("div", attrs = {"class":"cities-container"}))
        
        driver.execute_script("""
        var elements = document.querySelectorAll("div.cities-container-list__item"); //[0].querySelector("div.cities-container-list__item-name").innerHTML;
        for (element in elements) {
            if (elements[element].querySelector("div.cities-container-list__item-name").innerHTML == "Bengaluru") {
                elements[element].click()
                break;
            } 
        }
        console.log(elements[element]);
        """)
        time.sleep(10)
    
