import time, re, json
from crawler_tools import CrawlerTools

class CrawlerLogic(CrawlerTools):

    def crawl(self):
        print("crawling url: "+self.url )
        #getting DOM elements.
        soup = self.updatePageElements()

        driver = self.driver

        product_name_price_map = {}
        product_map = {}
        products_list = []
        main_container = soup.findAll("div", attrs = {"class":"s-desktop-content"})[0]
        container = main_container.findAll("div", attrs = {"class":"sg-col-20-of-24"})[0].findAll("div", attrs = {"class":"sg-col-inner"})[0]
        container = container.findAll("span", attrs = {"data-component-type":"s-search-results"})[0].findAll("div", attrs = {"class":"s-search-results"})[0]
    
        for itemCell in container.findAll("div", attrs = {"class":"sg-col-4-of-24"}):
            innerCell = itemCell.find("div", attrs = {"class":"sg-col-inner"}).find("span").find("div").find("div", attrs = {"class":"a-section"})
            product_name = ""
            product_quantity = ""
            product_selling_price = ""
            product_price = ""
    
            for item in innerCell.findAll("div", attrs = {"class":"a-section"}):
                try:
                    product_name = item.find("h2").find("a").find("span").text
                    product_quantity = re.findall(r"[0-9]+.[0-9]+.?[aA-zZ]+$", product_name)[0]
                    product_name = re.sub(r"[0-9]+.[0-9]+.?[aA-zZ]+$", '', product_name)
                    continue
                except Exception as e:
                    pass
    
                try:
                    item2 = item.find("div", attrs = {"class":"a-row"}).find("a").find("span", attrs = {"class":"a-price"}).find("span", attrs = {"class":"a-offscreen"})
                    product_selling_price = item2.text 
                    product_selling_price = re.sub(r'[^0-9'+self.decimal_point_char+r']+', '',product_selling_price )
    
                    item3 = item.find("div", attrs = {"class":"a-row"}).find("a").find("span", attrs = {"class":"a-text-price"}).find("span", attrs = {"class":"a-offscreen"})
                    product_price = item3.text 
                    product_price = re.sub(r'[^0-9'+self.decimal_point_char+r']+', '',product_price)
                    continue
                except Exception as e:
                    print (e)
                    pass
    
            print(product_name, product_quantity, product_selling_price, product_price)
            product_map["product_title"] = product_name
            product_map["product_quantity"] = product_quantity
            product_map["product_selling_price"] = product_selling_price 
            product_map["product_price"] = product_price
    
            products_list.append(product_map)
    
            product_name_price_map[product_map["product_title"]+" "+product_map["product_quantity"]] = [
                    float(product_map["product_selling_price"]),
                    float(product_map["product_price"])
                    ]
            product_map = {}

        #closing the open chromium window.
        driver.close()
        return json.dumps(products_list)


