import time, re, json
from crawler_tools import CrawlerTools

class CrawlerLogic(CrawlerTools):

    def crawl(self):
        url = self.url
        driver = self.driver
    
        category =  self.category
        result  = {}
        print("crawling url: "+url )
        driver.get(url)
    
        if category not in result.keys():
            result[category] = {}
    
        for i in range(100):
            if i < 6:
                print ("scrolling")
                driver.execute_script("window.scrollTo(0, {})".format(1080+(i*4000))) 
                time.sleep(10)
            else:
                #sys.exit()
                #break
                bounding_box = driver.execute_script("""
        elements = document.getElementsByTagName("button");
        (element in elements) {
        try {
            if(elements[element].getAttribute("ng-click") == "vm.pagginator.showmorepage()") {
                console.log(elements[element]);
                elements[element].parentElement.scrollIntoView();
                console.log("clicking!");
                elements[element].click();
                console.log(elements[element].style.display);
                return elements[element].getBoundingClientRect();
            }
        } catch(err) {
            console.log(err);
        }}""")  
                
                #print(get_product())     
                print(bounding_box["height"])
                if bounding_box["height"] == 0:
                    print("exitting");
                    break
            #sys.exit()
            time.sleep(10)
        
        driver.execute_script("""
    var element = document.getElementsByTagName("product-template");
    console.log(element);
    """)
        
        res = self.get_product()
        result[category] = res 
        
        print(result)
        self.generateCSV(result)
        driver.quit()

    def get_product():
        product = {}
        product_data = []
        soup = self.updatePageElements()
    
        for container in soup.findAll("div", attrs = {'class': 'main-content'}):
            pass
            #print (container)
            #sys.exit()
    
        for a in soup.findAll('div', attrs={'class':'items'}):
            #print(len(a.findAll('div', attrs={'qa':'product'})))
            for b in a.findAll('div', attrs={'qa':'product'}): 
                for image_element in b.find('a'): 
                    product["image_url"] = image_element["src"] 
    
                for quantity_elements in b.find('div', attrs={"class":"qnty-selection"}): 
                    quantity_price_map = {} 
                    #print(quantity_elements) 
                    try:
                        if quantity_elements.find('ul'):
                            for quantity_element_list in quantity_elements.find('ul'): 
                                #print(quantity_element_list) 
                                try: 
                                    for quantity_element in quantity_element_list.findAll('span'): 
                                        if quantity_element["ng-bind"] == "allProducts.w": 
                                            #print(quantity_element.text) 
                                            quantity_key = quantity_element.text 
                                            quantity_price_map[quantity_key] = "default" 
                                        if quantity_element["ng-bind"] == "allProducts.pack_desc": 
                                            #print(quantity_element) 
                                            pass 
                                        if quantity_element["ng-bind"] == "allProducts.sp": 
                                            price_value = quantity_element.text 
                                            quantity_price_map[quantity_key] = price_value 
                                            #print(quantity_element.text) 
                                            product["quantity_price_map"] = quantity_price_map if quantity_price_map else "NA" 
                                except Exception as e: 
                                    pass
                        else:
                            for quantity_element in quantity_elements.findAll('span'):
                                #print(quantity_element)
                                for span_element in quantity_element.findAll('span'):
                                    #print(span_element)
                                    if span_element["ng-bind"] == "vm.selectedProduct.w":
                                        quantity_key = span_element.text
                                        #print(quantity_key)
                                        quantity_price_map[quantity_key] = "default"
                            
                            for div_element in b.findAll('div', attrs={"class":"add-bskt"}):
                                #print(div_element)
                                for h4_element in div_element.findAll('h4'):
                                    #print(h4_element)
                                    for span_element in h4_element.findAll('span', attrs={"class":"discnt-price"}):
                                        for span in span_element.findAll('span'):
                                            #print(span["class"])
                                            if  "ng-binding" in span["class"]:
                                                #print(span)
                                                quantity_price_map[quantity_key] = span.text
                                                product["quantity_price_map"] = quantity_price_map if quantity_price_map else "NA" 
    
                    except Exception as e: 
                        #print (e) 
                        pass 
                for elements in b.findAll('div', attrs={"class":"ng-scope"}): 
                    #print (elements) 
                    try: 
                        for element in elements.find('div', attrs={"qa":"product_name"}): 
                            #print ("############: {}".format(element)) 
                            if(element.name == 'h6'): 
                                product["brand"] = element.text 
                            elif (element.name == 'a'): 
                                product["name"] = element.text 
                    except Exception as e: 
                        pass 
                    #print(e) 
    
                for c in b.find('div', attrs={'qa':'offer'}): 
                    #print(c) 
                    #print("##########") 
                    try: 
                        for d in c.find('span'): 
                            product["offer"] = d 
                            #print(d) 
                    except Exception as e: 
                        #print(e) 
                        pass 
                #print(product)
                product_data.append(product)
                product = {}
        
        #global total_product_data
        total_product_data = len(product_data)
        return product_data
