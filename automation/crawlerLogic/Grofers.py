import time
from crawler_tools import CrawlerTools

class CrawlerLogic(CrawlerTools):

    def selectCity(self, soup):
        self.driver.execute_script("""
        var elements = document.querySelectorAll("div.cities-container-list__item"); //[0].querySelector("div.cities-container-list__item-name").innerHTML;
        for (element in elements) {
            try {
                if (elements[element].querySelector("div.cities-container-list__item-name").innerHTML == "Bengaluru") {
                    elements[element].click()
                    break;
                } 
            } catch (e) {
                console.log(e);
            }
        }
        console.log(elements[element]);
        """)
        time.sleep(10)

    def crawl(self):
        print("crawling url: "+self.url )
    
        #getting DOM elements.
        soup = self.updatePageElements()
    
        #Selecting the preferred location.
        self.selectCity(soup)
    
        #reloading the DOM elements for the preferred location.
        soup = self.updatePageElements()
    
        #getting crawled results as map {}
        result = self.crawlThroughCategory(soup)
        result = str(result).encode("ascii", "ignore")
    
        #saving as CSV
        self.generateCSV(category,result)
    
        #saving as json
        y = {}
        y[category] = generate_analytics_base_file(category,json.dumps(self.product_name_price_map))
        self.product_name_price_map = {}
    
        with open("./results/grofers/analytics_{}.json".format(datetime.datetime.now()), "w+") as f:
            print("added to analytics.")
            json.dump(y, f)
    
        #closing the open chromium window.
        self.driver.close()
    
    #crawling logic
    def crawlThroughCategory(self, soup):
        soup = self.updatePageElements()
        driver = self.driver

        for container in soup.findAll("div", attrs = {"class":"products--grid"}):
            prev_product_card_length = 0
            equatedCount = 0

            #Scrolling page max 100 times or unitl number of product cards are not increasing.
            for i in range(100):
                product_card_length = driver.execute_script("""
                    return (document.querySelector("div.products--grid").childElementCount);
                        """)
                print (product_card_length, prev_product_card_length)
                if product_card_length == prev_product_card_length:
                    equatedCount =  equatedCount + 1
                    print("Same product count {} times.".format(equatedCount))
                    if equatedCount > 1:
                        break
                    break
                else:
                    equatedCount = 0
                prev_product_card_length = product_card_length
                print ("scrolling")
                driver.execute_script("window.scrollTo(0, {})".format(1080+(10*i*4000))) 
                time.sleep(10)

        #Going back to top
        driver.execute_script("window.scrollTo(0,0);")

        product_map = {}
        products_list = []

        for elements in driver.find_elements_by_class_name("products--grid")[0].find_elements_by_class_name("product__wrapper"):

            elements.click()

            pd_modal = driver.find_elements_by_class_name("pdp-modal-content")[0]
            for it in pd_modal.find_elements_by_class_name("product-variant__list")[0].find_elements_by_class_name("product-variant__btn"):
                print (it.text)

            time.sleep(2)
            driver.execute_script("""document.getElementsByClassName("ReactModal__Overlay")[0].click();""");
            time.sleep(2);
            driver.execute_script("""if (document.getElementById("wzrk-cancel") != null) {
                                        document.getElementById("wzrk-cancel").click();
                                        console.log("Clicking");
                                     } else {
                                        console.log("unable to find.");
                                     }
                                """);
            time.sleep(2)
            continue


