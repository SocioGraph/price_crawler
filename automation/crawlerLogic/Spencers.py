import time, re, json
from crawler_tools import CrawlerTools

class CrawlerLogic(CrawlerTools):

    def crawl(self):
        print("crawling url: "+self.url )
        #getting DOM elements.
        soup = self.updatePageElements()

        driver = self.driver

        product_name_price_map = {}

        #print(len(soup.findAll("ol", attrs = {"class":"product-items"})))
        for container in soup.findAll("ol", attrs = {"class":"product-items"}):
            #print(container)
            #print(container.findAll("li"))
            prev_product_card_length = 0
            equatedCount = 0
            for i in range(100):
                product_card_length = driver.execute_script("""return(document.getElementsByTagName("ol")[0].childElementCount)""")
                print (product_card_length, prev_product_card_length)
                if product_card_length == prev_product_card_length:
                    equatedCount =  equatedCount + 1
                    print("Same product count {} times.".format(equatedCount))
                    if equatedCount > 6:
                        break
                else:
                    equatedCount = 0
                prev_product_card_length = product_card_length
                print ("scrolling")
                driver.execute_script("window.scrollTo(0, {})".format(1080+(i*4000))) 
                bounding_box = driver.execute_script("""return(document.getElementsByClassName("deliveryLocationSec")[0].getBoundingClientRect());""")
                
                if bounding_box["height"] == 0:
                    pass
                else:
                    driver.execute_script("""document.getElementsByClassName("mfp-content")[0].click()""")
                time.sleep(10)
        
        soup = self.updatePageElements()
        for container in soup.findAll("ol", attrs = {"class":"product-items"}):
            product_map = {}
            products_list = []
            for product_card in container.findAll("li", attrs = {"class":"product-item"}):
                product_block = product_card.find("div", attrs = {"class":"product-block"})
                try:
                    image_url = product_block.find("div", attrs = {"class":"image"}).find("a", attrs = {"class":"product-item-photo"}).findAll("img", attrs = {"class":"product-image-photo"})[0]["src"]
                    product_item_details = product_block.find("div", attrs = {"class":"product-item-details"})
                    product_title_original =  product_item_details.find("a", attrs = {"class":"product-item-link"}).text.strip()
                    product_title = product_title_original.split(" ")
                    product_title.pop()
                    product_title_striped = " ".join(product_title)
                    product_quantity = product_title_original.split(" ")[-1]
                    
                    price_box = product_item_details.find("div", attrs = {"class":"price-box"})
                    
                    if price_box.find("span", attrs = {"class":"old-price"}):
                        product_price = price_box.find("span", attrs = {"data-price-type":"oldPrice"}).find("span", attrs = {"class":"price"}).text
                        product_selling_price = price_box.find("span", attrs = {"class":"special-price"}).find("span", attrs = {"data-price-type":"finalPrice"}).find("span",attrs = {"class":"price"}).text
                    else:
                        product_price = price_box.find("span", attrs = {"data-price-type":"finalPrice"}).find("span",attrs = {"class":"price"}).text
                        product_selling_price = product_price
                except Exception as e:
                    print(e)
                    print(product_card)
                
                print (image_url, product_title_striped, product_quantity, product_price)
                product_map["offer_percentage"] = ""
                product_map["product_title"] = product_title_striped
                product_map["product_quantity"] = product_quantity
                product_map["product_selling_price"] = product_selling_price 
                product_map["product_price"] = product_price
                product_map["image_url"] = image_url
                               
                products_list.append(product_map)
    
                product_name_price_map[product_map["product_title"]+" "+product_map["product_quantity"]] = [
                        float(re.sub(r'[^0-9'+self.decimal_point_char+r']+', '', product_map["product_selling_price"])),
                        float(re.sub(r'[^0-9'+self.decimal_point_char+r']+', '', product_map["product_price"]))
                        ]
    
                #product_name_price_map[product_map["product_title"]+" "+product_map["product_quantity"]] = float(product_map["product_selling_price"])
    
                product_map = {}
        
        #closing the open chromium window.
        driver.close()
        return json.dumps(products_list)
    
    def selectCity(soup):
        #citySelector = soup.find("header", attrs = {"class":"header"}).find("div", attrs = {"class":"header__left"}).find("a", attrs = {"class":"header-item--address"}).find("div", attrs = {"class":"user-address"})
        #print(driver.find_element_by_class_name('location').findAll("div", attrs = {"class":"cities-container"}))
        
        driver.execute_script("""
        var elements = document.querySelectorAll("div.cities-container-list__item"); //[0].querySelector("div.cities-container-list__item-name").innerHTML;
        for (element in elements) {
            if (elements[element].querySelector("div.cities-container-list__item-name").innerHTML == "Bengaluru") {
                elements[element].click()
                break;
            } 
        }
        console.log(elements[element]);
        """)
        time.sleep(10)


