from celery import Celery
import time, sys, os

sys.path.append(os.getcwd())

REDIS_URL=os.environ.get("REDIS_URL", "localhost")

app = Celery('tasks', broker="redis://{}:6379/0".format(REDIS_URL))

@app.task
def crawl(store, location, category, url):
    print(store, category, url)
    mod = __import__("crawlerLogic.{}".format(store), fromlist = ['CrawlerLogic'])
    print(mod)
    crawler = mod.CrawlerLogic(store.lower(), location, category, url, True)
    crawler.crawl()
    time.sleep(5)
    return store

