ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts
git clone git@bitbucket.org:SocioGraph/price_crawler.git
pushd price_crawler
pip3 install -r requirements.txt
pip3 install celery redis
pushd automation
export REDIS_URL=10.148.0.3
nohup xvfb-run celery -A createTasks worker --loglevel=INFO --concurrency=1 1> ./crawler_stdout.log 2> ./crawler_stderr.log &
popd
popd
