from selenium import webdriver
from bs4 import BeautifulSoup
import time, re, json, sys, csv, datetime
from os.path import isfile, join 
import re, locale

class CrawlerTools():


    def __init__(self, store, location, category, url, headless = True):
        
        self.store = store
        self.location = location
        self.category = category
        self.url = url
        self.headless = headless
        
        self.decimal_point_char = locale.localeconv()['decimal_point']

        chrome_options = webdriver.ChromeOptions()
        if self.headless:
            chrome_options.add_argument('--headless')
            chrome_options.add_argument('--no-sandbox')
            self.driver = webdriver.Chrome(executable_path = "/usr/bin/chromedriver", chrome_options=chrome_options)
        else:
            self.driver = webdriver.Chrome(executable_path = "/usr/bin/chromedriver")
        
        self.driver.get(self.url)

    @property
    def _store(self):
        return store

    @_store.setter
    def _store(self, store):
        if isinstance(store, str):
            self.store = store
        else:
            print("init store name failed.")
            sys.exit()

    @property
    def _location(self):
        return location

    @_location.setter
    def _location(self, location):
        if isinstance(location, str):
            self.location = location
        else:
            print("init location failed.")
            sys.exit()

    @property
    def _category(self):
        return category

    @_category.setter
    def _category(self, category):
        if isinstance(category, str):
            self.category = category
        else:
            print("init category failed.") 
            sys.exit()

    @property
    def _url(self):
        return url

    @_url.setter
    def _url(self, url):
        if isinstance(url, str):
            self.url = url
        else:
            print("init url failed.")
            sys.exit()

    @property
    def _headless(self):
        return headless

    @_headless.setter
    def _headless(self, headless):
        if isinstance(headless, bool):
            self.headless = headless
        else:
            print("init headless failed.")
            sys.exit()

    def updatePageElements(self):
        content = self.driver.page_source.encode('utf-8').strip()
        soup = BeautifulSoup(content, features="html.parser")
        return soup

