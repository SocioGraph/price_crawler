from createTasks import crawl

mapp = {
        "Amazon" : {
            "locations" : ["Bengaluru", "Chennai", "Kolkatta", "Hyderbad"],
            "url_map" : {
                "fruits" : "https://www.amazon.in/s?bbn=16392737031&rh=n%3A4859730031&pf_rd_p=e00d45bf-e552-41cc-ac44-6ceb4ab85c92&pf_rd_r=8CXXHRPTHKAR1J4D1TV0&ref=na_dsk_cp_mw_sml_fruits",
                "vegetables" : "https://www.amazon.in/s?bbn=16392737031&rh=n%3A4859730031&pf_rd_p=e00d45bf-e552-41cc-ac44-6ceb4ab85c92&pf_rd_r=8CXXHRPTHKAR1J4D1TV0&ref=na_dsk_cp_mw_sml_fruits"
                }
            },
        "Bigbasket" : {
            "locations" : ["Bengaluru", "Chennai", "Kolkatta", "Hyderbad"],
            "url_map" : {
                }
            },
        "Dmart" : {
            "locations" : ["Bengaluru", "Chennai", "Kolkatta", "Hyderbad"],
            "url_map" : {
                "fruits" : "https://dmart.in/fruits-1"
                }
            },
        "Grofers" : {
            "locations" : ["Bengaluru", "Chennai", "Kolkatta", "Hyderbad"],
            "url_map": {}
            },
        "Spencers" : {
            "locations" : ["Bengaluru", "Chennai", "Kolkatta", "Hyderbad"],
            "url_map" : {
                "fruits_and_vegetables" : "https://www.spencers.in/fruits-vegetables.html"
                }
            }
        }

if __name__ == "__main__":
    for store in mapp.keys():
        for location in mapp[store]["locations"]:
            for category, url in mapp[store]["url_map"].items():
                crawl.delay(store, location, category, url)
