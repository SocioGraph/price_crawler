import argparse, sys, os, csv, json, operator, re, datetime
from time import time

global generated_files_dir
generated_files_dir = None

client = "spar"
competitors = ["bigbasket", "grofers", "dmart", "spencers", "amazon"]
locations = ["bangalore", "mangalore", "delhi","chennai", "hyderabad"]
location_shorthand_map = {
        "bangalore" : "BLR",
        "mangalore" : "MLR",
        "delhi" : "NCR",
        "chennai" : "TN",
        "hyderabad" : "HYD"
        }
consolidated_data_headers = [
    "Division",
    "ITEM_DESC",
    "Item Code",
    "UOM",
    "spar",
    "spencers",
    "bigbasket",
    "dmart",
    "amazon",
    "grofers"
    ]

allFrontendData = {}
allFrontendData["client"] = client
allFrontendData["competitors"] = competitors
allFrontendData["locations"] = locations
allFrontendData["data"] = {}    
category_column = "Division"

scrapedData = {}



regexMissName = []
regexMissQuantity = []
regexCompMissName = []
regexCompMissQuantity = []

clientData = []

global time_now
time_now = None

def writeJsonFile(csv, filename, js = False):
    jsond = None
    #jsond = list(csv)[1:]
    # print(f"var {filename}_data = {json.dumps(jsond)};")
    if js:
        jsond = f"var {filename}_data = {json.dumps(jsond)};"
        filename = f"frontend/jsondata/{filename}.js"
    else:
        print(f"Creating json file {filename}.json")
        filename = filename+".json"
        jsond = json.dumps(csv, indent = 4)
        
    with open(f"{filename}", "w+") as cd:
        cd.write(jsond)

def check_data(data):
    print("\n")
    print(f"Client is : {data.get('client')}")
    print(f"Number of competitors : {len(data.get('competitors'))}")
    print(f"Competitors are : {data.get('competitors')}")
    print(f"Number of categories : {len(data.get('categories'))}")
    print(f"Categories are : {data.get('categories')}")

    print("\n")
    print(f"Total products with links in input sheet:")
    for store in data.get('competitors') + [data.get('client')]:
        arr = []
        for category in data.get('categories'):
            arr.append(data['total_products'][store][category][0])

        print(f"{store} : {str(sum(arr))}")
    
    print("\n")

    print(f"Total products crawled:")
    for store in data.get('competitors'):
        arr = []
        for category in data.get('categories'):
            arr.append(data['total_products'][store][category][1])

        print(f"{store} : {str(sum(arr))}")
    
    print("\n")

    print("Total number of products crawled category wise.")
    for category in data.get('categories'):
        for competitor in data.get('competitors') + [data.get('client')]:
            if competitor == data.get('client'):
                print("\n")
                print(f"{competitor} / {category} : {data['total_products'][competitor][category][0]}")
                continue

            print(f"{competitor} / {category} : {data['total_products'][competitor][category][0]}")
        print("\n")
    print("\n")

    print("\n")

    print("Spar Price Lower that competitor price.")
    for category in data.get('categories'):
        for competitor in data.get('competitors'):
                print(f"{competitor}/{category} : {len(data['comparison_data']['spar_lower'][category][competitor])}")
        print("\n")
    print("\n")

    print("\n")

    print("Spar Price greater that competitor price.")
    for category in data.get('categories'):
        for competitor in data.get('competitors'):
                print(f"{competitor}/{category} : {len(data['comparison_data']['spar_greater'][category][competitor])}")
        print("\n")
    print("\n\n")

def regexFn(inputString):
    allowedQuantities = ['kg', 'g', 'l', 'ml', 'pcs', 'gm', 'pc']
    matchProductName = "^[aA-zZ ]*"
    matchQuantityAndNumbers = "[0-9]*[aA-zZ][\*||xX][0-9]"
    # multiPackRegex = ["\d+[xX||*]\d+ [aA-zZ]+", "\d+ [aA-zZ]+[xX||*]\d+" ] #2x100 g, 2*100 g, 2X100 g
    qty_regex = "\d+\s*[aA-zZ]+"
    uom_regex = "[aA-zZ]+"

    quantity_and_numbers = None
    product_name = None

    if re.findall(r"{}".format(matchProductName), inputString):
        uom = ""
        quantity_and_numbers = re.sub(r"{}".format(matchProductName), "", inputString)
        quantity = re.findall(r"{}".format(qty_regex), quantity_and_numbers)
        if quantity:
            index = 0
            while not uom in allowedQuantities:
                uom = re.sub(r"\d+", "", quantity[index])
                uom = uom.strip()
                index = index + 1
                if index > len(quantity)-1:
                    if uom.lower() in allowedQuantities:
                        break
                    else:
                        # print(f"not allowed: {uom}")
                        uom = ""
                        break
                
        # print(f"Quantity: {quantity_and_numbers},  {quantity}")
        # print(f"UOM : {uom}")
        if re.findall(r"{}".format(matchQuantityAndNumbers), quantity_and_numbers):
            product_name = re.sub(r"{}".format(matchQuantityAndNumbers), "", inputString)
        else:
            product_name = inputString
    
    return product_name, quantity_and_numbers, uom

def generateFrontendData(inputfile, location):
    consolidated_data = []
    consolidated_data_map = {}

    frontendData = {} #this is the final dict used to populate data on frontend.
    frontendData["client"] = client
    frontendData["competitors"] = competitors
    frontendData["categories"] = set()
    frontendData["total_products"] = {}
    frontendData["comparison_data"] = {}
    frontendData["comparison_data"]["spar_greater"] = {}
    frontendData["comparison_data"]["spar_equal"] = {}
    frontendData["comparison_data"]["spar_lower"] = {}
    frontendData["comparison_data"]["min_max_price"] = {}
    frontendData["no_price"] = {}

    for competitor in competitors:
        #initializing dict to store scraped data.
        frontendData["no_price"][competitor] = {}

    with open(inputfile, "r") as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter = ",")
        line_count = 0
        
        category_analysis = {}
        unavailable_links = {}
        client_price_unavailable = []
        data_with_price = {} # to calculate min max prices


        for row in csv_reader:
            if line_count == 0:
                line_count = line_count +1 
                # print(row.keys())
            else:
                # Getting unique categorires.
                if not row[category_column] in category_analysis:
                    category_analysis[row[category_column]] = []
                    
                category_analysis[row[category_column]].append(row)

                ## adding categories to set.
                frontendData["categories"].add(row[category_column])

                product_name, product_quantity, uom = regexFn(row['ITEM_DESC'])

                # print(product_quantity)
                row['quantity'] = product_quantity
                row['uom'] = uom

                if not product_name:
                    regexMissName.append(row)

                if not product_quantity:
                    row['quantity'] = 1
                    row['uom'] = 'U'
                    # print(f"Qty miss :{row['ITEM_DESC']}. Assuming {row['quantity']}{row['uom']}, actual UOM {row['UOM']}")
                    # print(row)
                    regexMissQuantity.append(row)                    
                
                # print(f"{row['ITEM_DESC']} - {row['quantity']} - {row['uom']}")
                # print(row)
                # if not isinstance(row["selling_price"], int) or not isinstance(row["selling_price"], float):
                if row["selling_price"] == "#N/A":
                    # print(f"Ignoring {row['ITEM_DESC']} as client selling price not float or int, it is {row['selling_price']}")
                    client_price_unavailable.append(row['item_code'])
                    continue

                # Updating the numbers if the product url is unavailable.
                for competitor in competitors:
                    # print(f"Doing analytics for {competitor}")
                    if not competitor in unavailable_links.keys():
                        #initializing 0 for unavailable links for all competitors.
                        unavailable_links[competitor] = {}
                    
                    if not row[category_column] in unavailable_links[competitor]:
                        unavailable_links[competitor][row[category_column]] = 0

                    if row[f"{competitor.title()} URL"] == "Unavailable":
                        unavailable_links[competitor][row[category_column]] = unavailable_links[competitor][row[category_column]] + 1
                
                    if not competitor in data_with_price:
                        data_with_price[competitor] = {}
                    
                    if not row[category_column] in data_with_price[competitor]:
                        data_with_price[competitor][row[category_column]] =[]

                    #Updating spar_greater - Spar Selling Price is greater than competitor's selling price.
                    #Updating spar_lower - Spar Selling Price is lower than competitor's selling price.
                    #Updating spar_equal - Spar Selling Price is equal to competitor's selling price.
                    if not row[category_column] in frontendData["comparison_data"]["spar_greater"]:
                        frontendData["comparison_data"]["spar_greater"][row[category_column]] = {}
                        frontendData["comparison_data"]["min_max_price"][row[category_column]] = {}
                    
                    if not competitor in frontendData["comparison_data"]["spar_greater"][row[category_column]]:
                        frontendData["comparison_data"]["spar_greater"][row[category_column]][competitor] = []
                        frontendData["comparison_data"]["spar_greater"][row[category_column]][competitor+'_json'] = []

                        frontendData["comparison_data"]["min_max_price"][row[category_column]][competitor] = {}


                    if not row[category_column] in frontendData["comparison_data"]["spar_equal"]:
                        frontendData["comparison_data"]["spar_equal"][row[category_column]] = {}
                        frontendData["comparison_data"]["min_max_price"][row[category_column]] = {}
                    
                    if not competitor in frontendData["comparison_data"]["spar_equal"][row[category_column]]:
                        frontendData["comparison_data"]["spar_equal"][row[category_column]][competitor] = []
                        frontendData["comparison_data"]["spar_equal"][row[category_column]][competitor+'_json'] = []

                        frontendData["comparison_data"]["min_max_price"][row[category_column]][competitor] = {}


                    if not row[category_column] in frontendData["comparison_data"]["spar_lower"]:
                        frontendData["comparison_data"]["spar_lower"][row[category_column]] = {}
                        frontendData["comparison_data"]["min_max_price"][row[category_column]] = {}
                    
                    if not competitor in frontendData["comparison_data"]["spar_lower"][row[category_column]]:
                        frontendData["comparison_data"]["spar_lower"][row[category_column]][competitor] = []
                        frontendData["comparison_data"]["spar_lower"][row[category_column]][competitor+'_json'] = []

                        frontendData["comparison_data"]["min_max_price"][row[category_column]][competitor] = []

                    
                    for data in scrapedData[location][competitor]:
                        # print(row["selling_price"], data["selling_price"])
                        # print(float(row["selling_price"]), float(data["selling_price"]))

                        # print(type(row["selling_price"]), type(data["selling_price"]))
                        if not row["Item Code"] in consolidated_data_map:
                            consolidated_data_map[row["Item Code"]] = dict(row)
                            
                        consolidated_data_map[row["Item Code"]][competitor] = data["selling_price"]
 

                        if row.get("selling_price") == "":
                            continue

                        if data.get("selling_price") == " " or not data.get("selling_price"):
                            # adding to no_price list if the scaped data doesn't have price.
                            if row[category_column] not in frontendData["no_price"][competitor]:
                                frontendData["no_price"][competitor][row[category_column]] = []
                            #frontendData["no_price"][competitor][row[category_column]].append(data)
                            continue

                        # print(data)
                        product_name, product_quantity, uom = regexFn(data['product_name'])
                        
                        
                        # data['quantity'] = product_quantity
                        data['uom'] = ""
                    
                        # if not product_name:
                        #     regexCompMissName.append(row)
                        # if not product_quantity:
                        #     print(product_name)
                        #     data['quantity'] = 1
                        #     data['uom'] = 'kg'
                        #     # print(f"Qty miss :{row['ITEM_DESC']}. Assuming {row['quantity']}{row['uom']}")
                        #     # print(row)
                        #     regexCompMissQuantity.append(row)
                        #     # sys.exit()
                        
                        competitor_brand = data["brand"]
                        competitor_product_name = data["product_name"]
                        competitor_product_quantity = f"{data['quantity']}" #{data['uom']}"
                        competitor_product_selling_price = data["selling_price"]
                        competitor_product_mrp = data["price"]
                        competitor_product_price = [competitor_product_selling_price, competitor_product_mrp]
                        competitor_normalized_quantity = data["quantity"]
                        competitor_normalized_price = data["selling_price"]

                        client_brand = "NA"
                        client_product_name = row["ITEM_DESC"]
                        client_product_quantity = row["UOM"] #f"{row['quantity']}{row['uom']}"
                        client_product_selling_price = row["selling_price"]
                        client_product_mrp = row["selling_price"]
                        client_product_price = [client_product_selling_price, client_product_mrp]
                        client_normalized_quantity = "NA"
                        client_product_id = row["Item Code"]
                        client_normalized_price = row["selling_price"]


                        # normalized_quantity = 
                        # if row[f"{competitor}_needs_normalization"].lower() == "yes":
                        #     pass
                        # elif row[f"{competitor}_needs_normalization"].lower() == "":
                        #     normalized_quantity = 1
                        
                        dl = [competitor_brand, competitor_product_name, competitor_product_quantity, competitor_product_price, competitor_normalized_quantity, competitor_normalized_price, client_brand, client_product_name, client_product_quantity, client_product_price, client_normalized_quantity, client_product_id, client_normalized_price]

                        dj = {
                            "spar_product_id" : client_product_id, 
                            "spar_product_name" : client_product_name, 
                            "spar_product_quantity" : client_product_quantity, 
                            "spar_product_price" : client_product_selling_price, 
                            "competitor_product_name" : competitor_product_name, 
                            "competitor_product_quantity" : competitor_product_quantity, 
                            "competitor_product_price" : competitor_product_selling_price
                            }
                        # dl = [data["brand"], data["product_name"], data["quantity"], [data["selling_price"],data["selling_price"]], data["quantity"], data["selling_price"], "NA", row["ITEM_DESC"], 'NA', [row["selling_price"],row["selling_price"]], "NA","NA", row["selling_price"]]
                        # print(data)
                        if row["ITEM_DESC"] == data["item_desc"]:

                          
                            ##print(dict(row))
                            ##print(data)

                            ##con_data =  {}
                            ##for key in consolidated_data_headers:
                            ##    if key in row:
                            ##        con_data[key] = row[key]
                            
                            #if not row["Item Code"] in consolidated_data_map:
                            #    consolidated_data_map[row["Item Code"]] = dict(row)

                            #consolidated_data_map[row["Item Code"]][competitor] = competitor_product_selling_price

                            ##con_data[competitor] = competitor_product_selling_price
                            
                            ##consolidated_data.append(con_data)
                                                       
                            if data.get("needs_normalization") == "True":
                                competitor_price = data.get("normalized_price")
                            else:
                                competitor_price = data.get("selling_price")
                            
                            if not competitor_price:
                                #to deal with combo products
                                continue

                            # print(client_product_selling_price, type(client_product_selling_price), competitor_price, type(competitor_price))
                            if row["Item Code"] == '136185198':
                                print("item POPPY")
                                sys.exit()

                            if float(client_product_selling_price) > float(competitor_price):
                                frontendData["comparison_data"]["spar_greater"][row[category_column]][competitor].append(dl)
                                frontendData["comparison_data"]["spar_greater"][row[category_column]][competitor+"_json"].append(dj)
                                data_with_price[competitor][row[category_column]].append(data)
                                break
                            elif float(client_product_selling_price) < float(competitor_price):
                                frontendData["comparison_data"]["spar_lower"][row[category_column]][competitor].append(dl)
                                frontendData["comparison_data"]["spar_lower"][row[category_column]][competitor+"_json"].append(dj)
                                data_with_price[competitor][row[category_column]].append(data)
                                break
                            elif float(client_product_selling_price) == float(competitor_price):
                                frontendData["comparison_data"]["spar_equal"][row[category_column]][competitor].append(dl)
                                frontendData["comparison_data"]["spar_equal"][row[category_column]][competitor+"_json"].append(dj)
                                data_with_price[competitor][row[category_column]].append(data)
                                break
        

        # # getting min max values for competitors.                            
        # for competitor in competitors:                                                                
        #converting set to list to avoid json serilization errors.
        frontendData["categories"] = list(frontendData["categories"])

        # getting actual number of products per category. All products - unavailable products.
        for key in category_analysis.keys():
            for site in competitors + [client]:
                if not site in frontendData["total_products"]:
                    frontendData["total_products"][site] = {}
                
                # no_of_products = total_rows - link_unavailable_rows
                # no_of_crawled_products = greater + lower + equal
                no_of_products = len(category_analysis[key]) if site == client else len(category_analysis[key]) - unavailable_links[site][key]
                frontendData["total_products"][site][key] = []
                frontendData["total_products"][site][key].append(no_of_products)

                if site != client:
                    no_of_crawled_products = len(frontendData["comparison_data"]["spar_greater"][key][site]) + len(frontendData["comparison_data"]["spar_equal"][key][site]) + len(frontendData["comparison_data"]["spar_lower"][key][site])

                    frontendData["total_products"][site][key].append(no_of_crawled_products)


                if site == client:
                    if not client in frontendData["comparison_data"]["min_max_price"][key]:
                        frontendData["comparison_data"]["min_max_price"][key][client] = []
                    ascArray = sorted(category_analysis[key], key=operator.itemgetter('selling_price'))
                    decArray = sorted(category_analysis[key], key=operator.itemgetter('selling_price'), reverse=True)
                    minVal = []
                    maxVal = []
                    if len(ascArray):
                        minVal.append(ascArray[0]["selling_price"])
                        minVal.append(ascArray[0]["selling_price"]) #substituting selling price instead of mrp becase mrp data not 

                    if len(decArray):
                        maxVal.append(decArray[0]["selling_price"])
                        maxVal.append(decArray[0]["selling_price"]) #substituting selling price instead of mrp becase mrp data not available.

                    # print(f"Minval : {minVal}")
                    # print(f"Maxval : {maxVal}")
                    frontendData["comparison_data"]["min_max_price"][key][client].append(minVal)
                    frontendData["comparison_data"]["min_max_price"][key][client].append(maxVal)
                else:
                    if len(frontendData["comparison_data"]["min_max_price"][key][site]) == 0:
                        ascArray = sorted(data_with_price[site][key], key=operator.itemgetter('selling_price'))
                        decArray = sorted(data_with_price[site][key], key=operator.itemgetter('selling_price'), reverse=True)
                        minVal = []
                        maxVal = []
                        if len(ascArray):
                            minVal.append(ascArray[0]["selling_price"])
                            minVal.append(ascArray[0]["price"])

                        if len(decArray):
                            maxVal.append(decArray[0]["selling_price"])
                            maxVal.append(decArray[0]["price"])

                        # print(f"Minval : {minVal}")
                        # print(f"Maxval : {maxVal}")
                        frontendData["comparison_data"]["min_max_price"][key][site].append(minVal)
                        frontendData["comparison_data"]["min_max_price"][key][site].append(maxVal)
   
    for item_code in consolidated_data_map.keys():
        con_data =  {}
        for key in consolidated_data_headers:
            if key == "spar":
                if "selling_price" in consolidated_data_map[item_code]:
                    con_data[key] = consolidated_data_map[item_code]["selling_price"]
                    continue
            if key in consolidated_data_map[item_code]:
                con_data[key] = consolidated_data_map[item_code][key]

        consolidated_data.append(con_data)

    #print(consolidated_data)
    # with open(f"{generated_files_dir}/consolidated_data_{location}.json", "w") as fp:
    #     fp.write(json.dumps(consolidated_data, indent = 4))
    
    writeJsonFile(consolidated_data, f"{generated_files_dir}/consolidated_data_{location}.json", False)

    
    if len(consolidated_data) > 0:
        with open(f"{generated_files_dir}/consolidated_data_{location}_{time_now.strftime('%d-%m-%y_%H:%M')}.csv", "w") as cp:
            dw = csv.DictWriter(cp, fieldnames=consolidated_data_headers )
            dw.writeheader()
            dw.writerows(consolidated_data)

    return frontendData

if __name__ == "__main__":
    time_now = datetime.datetime.now()
    timestamp = time_now.strftime("%d/%m/%y, %H:%M")
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--GEN_FILES_DIR', help = "Generated files dir.")
    parser.add_argument('--SCRAPED_DATA_DIR', help = "Directory where scraped CSVs are stored.", default = "./scrapedData/")
    parser.add_argument('--TIMESTAMP', help="Timestamp that appears on frontend.", default=timestamp)

    args = parser.parse_args()
    scraped_data_dir = args.SCRAPED_DATA_DIR
    generated_files_dir = args.GEN_FILES_DIR
    timestamp = args.TIMESTAMP

    print(f"Generated files dir is taken as {generated_files_dir}")
    print(f"Scraped data dir is taken as {scraped_data_dir}")


    if not os.path.isdir(generated_files_dir):
        print("Generated files dir doesn't exists.")
        sys.exit()
    
    if not os.path.isdir(scraped_data_dir):
        print("Scraped data dir doesn't exists.")
        sys.exit()

    for location in locations:
        print(location)
        scrapedData[location] = {}
        location_shorthand = location_shorthand_map[location]
        for competitor in competitors:
            scrapedData[location][competitor] = []
            if not os.path.isfile(f"{scraped_data_dir}{competitor}_{location}.csv"):
                print(f"File {scraped_data_dir}{competitor}_{location}.csv not found.")
                continue
    
            with open(f"{scraped_data_dir}{competitor}_{location}.csv", "r") as csv_file:
                csv_reader = csv.DictReader(csv_file, delimiter = ",")
                line_count = 0
    
                for row in csv_reader:
                    print(row)
                    scrapedData[location][competitor].append(dict(row))
                    print(f"{location} - {competitor} - {row['item_desc']}")
                #csv_file.seek(0)
                #writeJsonFile(csv_reader, f"{competitor}_{location}", False)
        
    # print(json.dumps(scrapedData, indent=4))
    writeJsonFile(scrapedData, f"{generated_files_dir}/scrapedData", False)

    allCategories = []
    for location in locations:
        inputfile = f"{generated_files_dir}/valid_products_with_CSP_{location_shorthand_map[location]}.csv"

        if not os.path.isfile(inputfile):
            print(f"Input file {inputfile} doesn't exists.")
            continue

        allFrontendData["data"][location] = generateFrontendData(inputfile, location)
        allCategories = allCategories + allFrontendData["data"][location]["categories"]

    # print(list(set(allCategories)))
    allFrontendData["categories"] = list(set(allCategories))
    allFrontendData["timestamp"] = timestamp
    writeJsonFile(allFrontendData, f"{generated_files_dir}/allFrontendData", False)

    with open("./frontend/data_temp.js", "w+") as fp:
        jsond = f"var pdata = {json.dumps(allFrontendData)};"
        fp.write(jsond)
    sys.exit()

    # print(json.dumps(unavailable_links, indent =4))
    # print(frontendData)
    with open("frontendData.json", "w+") as fp:
        fp.write(json.dumps(frontendData, indent = 4))
    
    with open("./frontend/data_temp.js", "w+") as fp:
        jsond = f"var pdata = {json.dumps(frontendData)};"
        fp.write(jsond)
    
    csv_file.seek(0)
    writeJsonFile(csv_reader, 'spar', True )



    # check_data(frontendData)
    # print(f"Name miss: {len(regexCompMissName)}, Quantity miss: {len(regexCompMissQuantity)}")

