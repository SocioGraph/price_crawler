let lower_percentage = undefined;
let higher_percentage = undefined;
let equal_percentage = undefined;
var stat_type = undefined;

function showSnackbar(message) {
	var x = document.getElementById("snackbar");
	x.innerHTML = message;
	x.className = "show";
	setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
};

function showStatsType(type = "percentage") {
	let number_button = document.getElementById("number_button");
	let percentage_button = document.getElementById("percentage_button");
	stat_type = type;
	if(type == "percentage") {
		number_button.style.backgroundColor = "#d9ff66";
		percentage_button.style.backgroundColor = "rgb(0, 179, 0)";
	} else if (type == "number") {
		percentage_button.style.backgroundColor = "#d9ff66";
		number_button.style.backgroundColor = "rgb(0, 179, 0)";		
	}
}