var avaliableCategories = undefined;
var store_list = ["spar", "spencers", "grofers", "dmart", "big_basket"];

function animateValue(id, end, duration = 1000) {
	var range = end - (end*0.5);
	var start = end - range;
	var current = start;
	var increment = 100*range / duration;
	// console.log(id, start, end, duration, range, increment);
	var obj = document.getElementById(id);
	var timer = setInterval(function() {
		current += increment;
		// console.log(current, increment);
		if (current >= end) {
			clearInterval(timer);
			current = end
		}
		// console.log(id);
		// console.log(obj);
		obj.innerHTML = Math.abs(Math.floor(current));
	}, 100.0);
}
var category = undefined;
function changeNumbers(element) {
	avaliableCategories =  data["categories"];
	let activeCategories = ["fruits_and_vegetables", "grocery"];
	category = element.getAttribute("data_set");
	selected_category = category;
	//category = "fruits_and_vegetables";
	currentActiveCategory = category;
	// console.log(category);
	for(let item of document.getElementById("select_categories_box").children) {
		if (currentActiveCategory == item.getAttribute("data_set")) {
			//item.style.backgroundColor = "#b3ff66";
			item.style.backgroundColor = "#00b300";
		}else {
			item.style.backgroundColor = null;
		}
	}
	activeCategories.forEach(function change(itm,index) {
		// console.log(selected_category, itm);
		if(selected_category == "fmcg"){
			showSnackbar("Currently Unavailable!")
		}
	});
	//if (category != "fruits_and_vegetables" && category != "all") {
	//	return;
	//}
	
	applyUI(category);
	updateGraphs(category);
	competitors.forEach(function(competitor, competitiorIndex) {
		// console.log(selected_category);
		if (selected_category != "fmcg") {
			fillTables(competitor, comparison_data_for_tables[client+"_"+selected_comparison][selected_category][competitor]);
			fillTablesNew(competitor, comparison_data_for_tables[client+"_"+selected_comparison][category][competitor+'_json']);
		}
	});
}