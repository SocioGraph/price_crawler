var randomScalingFactor = function() {
	return Math.round(Math.random() * 100);
};
var graphMap = {};
let ctxMap = {};
var total_higher_sum = undefined;
var total_lower_sum = undefined;
var total_equal_sum = undefined;

function createGraph(ctx,data) {
	var config = {
		type: 'pie',
		data: {
			datasets: [{
				data: data,
				backgroundColor: [
					"#00cc44",
					window.chartColors.red,
					window.chartColors.orange
				],
				label: 'Dataset 1'
			}],
			labels: [
				'LOWER',
				'HIGHER',
				'EQUAL'
			]
		},
		options: {
			responsive: false
		}
	};
	return new Chart(ctx, config);
}

function deleteGraph(pie) {
	pie.destroy();
}

function updatePercentages() {
	competitors.forEach(function(competitior, competitiorIndex) {

		// console.log(selected_comparison, currentActiveCategory, competitior);
		let _of_no = data["comparison_data"]["spar_"+selected_comparison][currentActiveCategory][competitior].length;
		let _total_no = data["total_products"][competitior][currentActiveCategory][0];
		// console.log("Percentage:");
		// console.log(_of_no, _total_no, (_of_no*100)/_total_no);

		// let of_no = parseInt(document.getElementById(competitior+"_low_or_high_number").innerHTML);
		// let total_no = parseInt(document.getElementById(competitior+"_comparison_matched_products").innerHTML);
		// console.log(of_no,total_no);

		let percentage_of = (_of_no*100)/_total_no;
		document.getElementById(competitior+"_percentage").innerHTML = Math.round(percentage_of)+" %";
	});
}

function updateGraphs(category, dispData = undefined) {
	category = selected_category;
	
	if (dispData) {
		comparison_data_for_graphs = dispData;
	} else {
		comparison_data_for_graphs = comparison_data;
	}
	
	if(categories.indexOf(category) != -1) {
		// console.log(category);
		competitors.forEach(function(competitior, competitiorIndex) {
			let data = [
				comparison_data_for_graphs[client+"_lower"][category][competitior].length,
				comparison_data_for_graphs[client+"_greater"][category][competitior].length,
				comparison_data_for_graphs[client+"_equal"][category][competitior].length
			];

			// console.log(competitior+"_pie");
			deleteGraph(graphMap[competitior+"_pie"]);
			let pie = createGraph(ctxMap[competitior+"_pie_context"],data);
			graphMap[competitior+"_pie"] = pie;
			
			// console.log(competitior, comparison_data[client+"_lower"][category][competitior].length);
			if (stat_type == "percentage") {
				document.getElementById(competitior+"_low_or_high_number").innerHTML;
				dispComparisonCount(competitior, 100/(total_products[competitior][category][0]/comparison_data[client+"_lower"][category][competitior].length), "percentage");
			} else {
				dispComparisonCount(competitior, comparison_data[client+"_lower"][category][competitior].length, "number");
			}
			
		});
	} else if (category == "all") {
		// console.log("hereeeeeeee");
		competitors.forEach(function(competitior, competitiorIndex) {
			let lower_list = [];
			let higher_list = [];
			let equal_list = [];
			
			let lower_pd_list = [];
			let higher_pd_list = [];
			let equal_pd_list = [];
			
			categories.forEach(function(category,catIndex) {
				lower_list.push(comparison_data[client+"_lower"][category][competitior].length);
				lower_pd_list = comparison_data[client+"_lower"][category][competitior];
				higher_list.push(comparison_data[client+"_greater"][category][competitior].length);
				higher_pd_list = comparison_data[client+"_greater"][category][competitior];
				equal_list.push(comparison_data[client+"_equal"][category][competitior].length);
				equal_pd_list = comparison_data[client+"_equal"][category][competitior];
			});
			
			total_lower_sum = lower_list.reduce((a,b) => a+b, 0);
			total_higher_sum = higher_list.reduce((a,b) => a+b, 0);
			total_equal_sum =  equal_list.reduce((a,b) => a+b, 0);
			categories.forEach(function(category,catIndex) {
				lower_percentage = 100 /(total_products[competitior][category][1]/total_lower_sum);
				higher_percentage = 100 /(total_products[competitior][category][1]/total_higher_sum);
				equal_percentage = 100 /(total_products[competitior][category][1]/total_equal_sum);
			});
			// console.log(lower_percentage, higher_percentage, equal_percentage);
			let data = [ total_lower_sum, total_higher_sum, total_equal_sum];
			deleteGraph(graphMap[competitior+"_pie"]);
			let pie = createGraph(ctxMap[competitior+"_pie_context"],data);
			graphMap[competitior+"_pie"] = pie;
			if (stat_type == "percentage") {
				dispComparisonCount(competitior, lower_list.reduce((a,b) => a+b, 0));
			} else {
				// dispComparisonCount(competitior, 100/(total_products[competitior][category][0]/comparison_data[client+"_lower"][category][competitior].length), "number_holder");
			}
		});
	}
	updatePercentages();
}

function dispComparisonCount(competitior, value, type) {
	// console.log(value);
	document.getElementById(competitior+"_low_or_high_number").innerHTML = value;
}

function fillTables(competitior, table_list, table_element_id = undefined) {
	console.log("FILL TABLELLLLLLLLL")
	// console.log(table_list,competitior);
	let comp_table = "";
	
	if (!table_element_id) {
		// console.log(table_element_id);
		comp_table = document.getElementById(competitior+"_table");
	} else {
		comp_table = document.getElementById(table_element_id);
	}
	
	comp_table.innerHTML = "";
	let Thead = comp_table.createTHead();
	let headerRow = Thead.insertRow(0);
	// headerRow.insertCell(0).innerHTML = "Sl.No.";
	//headerRow.insertCell(0).innerHTML = "QUANTITY";
	
	let clBrand = headerRow.insertCell(0);
	clBrand.innerHTML = client.toUpperCase()+" BRAND";
	clBrand.className = "clientHighlight";
	
	let clName = headerRow.insertCell(1);
	clName.innerHTML = client.replace("_"," ").toUpperCase()+" NAME";
	clName.className = "clientHighlight";
	
	let clAQuantity = headerRow.insertCell(2);
	clAQuantity.innerHTML = client.replace("_"," ").toUpperCase()+" ACTUAL QUANTITY";
	clAQuantity.className = "clientHighlight";
	
	let clSellPrice = headerRow.insertCell(3);
	clSellPrice.innerHTML = client.toUpperCase()+" SELLING PRICE";
	clSellPrice.className = "clientHighlight";
	
	let clPrice = headerRow.insertCell(4);
	clPrice.innerHTML = client.toUpperCase()+" PRICE (MRP)";
	clPrice.className = "clientHighlight";
	
	let clNQuantity = headerRow.insertCell(5);
	clNQuantity.innerHTML = client.toUpperCase()+" Norm. QUANTITY";
	clNQuantity.className = "clientHighlight";
	
	let clNPrice = headerRow.insertCell(6);
	clNPrice.innerHTML = client.toUpperCase()+" Per Norm. Qty. PRICE";
	clNPrice.className = "clientHighlight";
	
	headerRow.insertCell(7).innerHTML = competitior.replace("_"," ").toUpperCase()+" BRAND";
	headerRow.insertCell(8).innerHTML = competitior.replace("_"," ").toUpperCase()+" NAME";
	headerRow.insertCell(9).innerHTML = competitior.replace("_"," ").toUpperCase()+" QUANTITY";
	headerRow.insertCell(10).innerHTML = competitior.replace("_"," ").toUpperCase()+" SELLING PRICE";
	headerRow.insertCell(11).innerHTML = competitior.replace("_"," ").toUpperCase()+" PRICE (MRP)";
	headerRow.insertCell(12).innerHTML = competitior.replace("_"," ").toUpperCase()+" Norm. QUANTITY";
	headerRow.insertCell(13).innerHTML = competitior.replace("_"," ").toUpperCase()+" Per Norm. Qty. PRICE";
	
	// headerRow.insertCell(8).innerHTML = "ID";
	let Tbody = comp_table.createTBody();
	
	let rowss = 10;
	if (table_list.length < 10 ) {
		rowss = table_list.length;
	} else {
		rowss = 10;
	}
	for (let i = 0; i<rowss;i++) {
		let bodyRow = Tbody.insertRow(i);
		for(let j = 0; j<14;j++) {
			let bodyCell = bodyRow.insertCell(j);
			if (j ==7){//Client Brand
				try {
					bodyCell.innerHTML = table_list[i][0];
				} catch (e) {
					bodyCell.innerHTML = ""
				}
			} else if (j == 8) {//client pd name
				try {
					bodyCell.innerHTML = table_list[i][1];
				} catch (e) {
					bodyCell.innerHTML = "";
				}
			} else if (j == 9) {//client pd actual qty
				try {
					bodyCell.innerHTML = table_list[i][2];
				} catch (e) {
					bodyCell.innerHTML = "";
				}							
			} else if (j == 10) {//client pd selling price
				try {
					bodyCell.innerHTML = Math.round(table_list[i][3][0]);
				} catch (e) {
					bodyCell.innerHTML = "";
				}							
			} else if (j == 11) {//client mrp price
				try {
					bodyCell.innerHTML = Math.round(table_list[i][3][1]);
				} catch (e) {
					bodyCell.innerHTML = "";
				}							
			} else if (j == 12) {//client norm. qty
				try {
					bodyCell.innerHTML = table_list[i][4];
				} catch (e) {
					bodyCell.innerHTML = "";
				}				
			} else if (j == 13) {//client calc price
				try {
					bodyCell.innerHTML = table_list[i][5];
				} catch (e) {
					bodyCell.innerHTML = "";
				}							
			} else if (j ==0){//Client Brand
				try {
					bodyCell.innerHTML = table_list[i][6];
				} catch (e) {
					bodyCell.innerHTML = ""
				}
			} else if (j == 1) {//client pd name
				try {
					bodyCell.innerHTML = table_list[i][7];
				} catch (e) {
					bodyCell.innerHTML = "";
				}
			} else if (j == 2) {//client pd actual qty
				try {
					bodyCell.innerHTML = table_list[i][8];
				} catch (e) {
					bodyCell.innerHTML = "";
				}							
			} else if (j == 3) {//client pd selling price
				try {
					bodyCell.innerHTML = Math.round(table_list[i][9][0]);
				} catch (e) {
					bodyCell.innerHTML = "";
				}							
			} else if (j == 4) {//client mrp price
				try {
					bodyCell.innerHTML = Math.round(table_list[i][9][1]);
				} catch (e) {
					bodyCell.innerHTML = "";
				}							
			} else if (j == 5) {//client norm. qty
				try {
					bodyCell.innerHTML = table_list[i][11];
				} catch (e) {
					bodyCell.innerHTML = "";
				}				
			} else if (j == 6) {//client calc price
				try {
					bodyCell.innerHTML = parseInt(table_list[i][12]);
				} catch (e) {
					bodyCell.innerHTML = "";
				}							
			}
		}
		// console.log(table_list[i]);
		sortTable(7, competitior+"_table", "asc");
	}
}

function CreateTableFromJSON(data) {
	var myBooks = data;

	// EXTRACT VALUE FOR HTML HEADER. 
	// ('Book ID', 'Book Name', 'Category' and 'Price')
	var col = [];
	for (var i = 0; i < myBooks.length; i++) {
		for (var key in myBooks[i]) {
			if (col.indexOf(key) === -1) {
				col.push(key);
			}
		}
	}

	// CREATE DYNAMIC TABLE.
	var table = document.createElement("table");
	table.className = "custom_table";

	// CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.

	var tr = table.insertRow(-1);                   // TABLE ROW.

	for (var i = 0; i < col.length; i++) {
		let header = col[i];
		header = header.replaceAll('_', " ");
		header = header.toUpperCase()
		var th = document.createElement("th");      // TABLE HEADER.
		console.log(header);
		if (header.startsWith('SPAR')) {
			th.className = "clientHighlight";
		}
		th.innerHTML = header;

		tr.appendChild(th);
	}

	// ADD JSON DATA TO THE TABLE AS ROWS.
	for (var i = 0; i < myBooks.length; i++) {

		tr = table.insertRow(-1);

		for (var j = 0; j < col.length; j++) {
			var tabCell = tr.insertCell(-1);
			var value = myBooks[i][col[j]];
			
			tabCell.innerHTML = value;
		}
	}

	// FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
	// var divContainer = document.getElementById("showData");
	// divContainer.innerHTML = "";
	// divContainer.appendChild(table);
	return table;
}


function fillTablesNew(competitior, table_list, table_element_id = undefined) {
	console.log(competitior);
	let tbl = CreateTableFromJSON(table_list);
	tbl.id = competitior+"_table";
	tbl.className = "custom_table";
	console.log(tbl);
	let table_wrapper = document.getElementById(competitior+'_table_wrapper');
	table_wrapper.innerHTML = "";
	table_wrapper.appendChild(tbl);
}

function dispLowOrHigh(whichVal = "lower", useData=comparison_data_for_tables) {
	selected_comparison = whichVal;
	if (useData) {
		comparison_data_for_tables = useData;
	} else {
		comparison_data_for_tables = comparison_data;
	}
	// console.log(comparison_data_for_tables);
	generateCategoryLowHighData(comparison_data_for_tables);
	for(let selector of document.getElementById("select_comparison_box").children) {
		// console.log(selector.getAttribute("data_set"));
		if (whichVal == selector.getAttribute("data_set")) {
			//selector.style.backgroundColor = "#b3ff66";						
			selector.style.backgroundColor = "#00b300";
		} else {
			selector.style.backgroundColor = null;
		}
	}
	// console.log(whichVal, selected_category);
	category = selected_category;
	competitors.forEach(function(competitior, competitiorIndex){
		// console.log(competitior,whichVal);
		if (whichVal == "lower") {
			document.getElementById(competitior+"_comparison_label").innerHTML = "LOWER THAN";
		} else if (whichVal == "greater") {
			document.getElementById(competitior+"_comparison_label").innerHTML = "HIGHER THAN";
		} else if (whichVal == "equal"){
			document.getElementById(competitior+"_comparison_label").innerHTML = "EQUAL TO";
		}
		if (category == "all") {
			// console.log(competitior, allCatLowHigh[whichVal][competitior]);
			dispComparisonCount(competitior, allCatLowHigh[whichVal][competitior]);
			console.log("THHHHHHHH");
			fillTables(competitior, comparison_data_for_tables[client+"_"+whichVal]["FRUIT AND VEG"][competitior]);
			fillTablesNew(competitior, comparison_data_for_tables[client+"_"+whichVal]["FRUIT AND VEG"][competitior+'_json']);

		} else if(categories.indexOf(category) != -1){
			dispComparisonCount(competitior, comparison_data_for_tables[client+"_"+whichVal][category][competitior].length);
			fillTables(competitior, comparison_data_for_tables[client+"_"+whichVal][category][competitior]);
			fillTablesNew(competitior, comparison_data_for_tables[client+"_"+whichVal][category][competitior+'_json']);

		}
	});
	updatePercentages();
}

// sleep time expects milliseconds
function custSleep (time) {
	// console.log("Sleeping for "+time);
	return new Promise((resolve) => setTimeout(resolve, time));
}

// window.onload = function() {
// 	// console.log("load");
// 	dispLowOrHigh("lower");
// 	var bigBasketCtx = document.getElementById('sparVsBigBasketChart').getContext('2d');
// 	var spencersCtx = document.getElementById('sparVsSpencersChart').getContext('2d');
// 	var grofersCtx = document.getElementById('sparVsGrofersChart').getContext('2d');
// 	var dmartCtx = document.getElementById('sparVsDmartChart').getContext('2d');
// 	var amazonCtx = document.getElementById('sparVsAmazonChart').getContext('2d');
// 	ctxMap = {
// 		"big_basket_pie_context" : bigBasketCtx,
// 		"spencers_pie_context" : spencersCtx,
// 		"grofers_pie_context" : grofersCtx,
// 		"dmart_pie_context" : dmartCtx,
// 		"amazon_pie_context" : amazonCtx
// 	};
// 	competitors.forEach(function (item, index) {
// 		let pie = createGraph(ctxMap[item+"_pie_context"], [100, 40, 130]);
// 		graphMap[item+"_pie"] = pie;
// 	});
// 	updateGraphs("FRUIT AND VEG");
// 	document.getElementById("tmp_bt_id").click();
// };