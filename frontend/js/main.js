var client = undefined;
var competitors = undefined;
var categories = undefined;
var total_products = undefined;
var comparison_data = undefined;
var comparison_data_for_tables = undefined;
var client_total_products = undefined;
var categoryLowHighData = {};
var selected_category =  "all";
var selected_comparison = undefined;
var allCatLowHigh = undefined;
var max_price_input_element = undefined;
var min_range_input_element = undefined;
var currentActiveCategory = undefined;
var currentActiveLocation = undefined;
var locations = undefined;
var data = undefined;
var XhrCalls = undefined;

function generateCategoryLowHighData(data) {
	allCatLowHigh = {
		"greater":{},
		"lower" :{},
		"equal": {}
	}

	let price_levels =["greater", "lower", "equal"];
	// console.log(price_levels);
	price_levels.forEach(function(item, index) {
		competitors.forEach(function(competitior, competitiorIndex) {
			let tmp = [];
			categories.forEach(function(cat, catIndex) {
				// console.log(client,item,cat,competitior);
				// console.log(comparison_data[client+"_"+item][cat][competitior].length)
				tmp.push(comparison_data[client+"_"+item][cat][competitior].length);
			});
			// console.log(tmp);
			allCatLowHigh[item][competitior] = tmp.reduce((a,b) => a+b, 0);
		});
	});
}

function getTotalProducts(store, matched = false) {
	let temp = [];
	let pd_index = 0;
	if (matched) {
		pd_index = 1;
	}
	categories.forEach(function (item, index){
		// console.log(store, item, pd_index);
		temp.push(total_products[store][item][pd_index]);
	});
	return temp.reduce((a,b) => a+b, 0);
}

function applyUI(category = "all") {
	// console.log(category);
	if (category == "all") {
		animateValue("spar_products_number", getTotalProducts(client));
		competitors.forEach(function (item,index){
			// console.log("Populating values for: "+category );
			// console.log(item+"_products_number", getTotalProducts(item, true));
			animateValue(item+"_products_number", getTotalProducts(item, true));
			animateValue(item+"_total_products_number", getTotalProducts(item));
			// animateValue(item+"_comparison_matched_products", getTotalProducts(item, true));
			animateValue(item+"_comparison_matched_products", comparison_data["spar_greater"]["FRUIT AND VEG"][item].length+
				comparison_data["spar_lower"]["FRUIT AND VEG"][item].length+
				comparison_data["spar_equal"]["FRUIT AND VEG"][item].length);
		});
	} else if (categories.indexOf(category) != -1) {
		animateValue("spar_products_number", total_products[client][category][0]);
		competitors.forEach(function (item,index){
			animateValue(item+"_products_number", total_products[item][category][1]);
			animateValue(item+"_total_products_number", total_products[item][category][0]);
			// animateValue(item+"_comparison_matched_products", total_products[item][category][1]);
			animateValue(item+"_comparison_matched_products", comparison_data["spar_greater"][category][item].length+
				comparison_data["spar_lower"][category][item].length+
				comparison_data["spar_equal"][category][item].length);
		});
	} else {
		console.log("Unknown category.");
	}
}

function getProductsInPriceRange(min_price, max_price) {
	let allProductsMap = {
		"spar_greater" :{
			"fruits_and_vegetables":{
				"spencers" :[],
				"grofers":[],
				"big_basket" :[],
				"dmart" :[]
			}
		},
		"spar_lower" :{
			"fruits_and_vegetables":{
				"spencers" :[],
				"grofers":[],
				"big_basket" :[],
				"dmart" :[]
			}
		},
		"spar_equal" :{
			"fruits_and_vegetables":{
				"spencers" :[],
				"grofers":[],
				"big_basket" :[],
				"dmart" :[]
			}
		}
	}
	let levels = ["lower", "higher", "equal"];
	levels.forEach(function(level, levelIndex){
		let lv = level;
		if (lv == "higher") {
			lv = "greater";
		} 
		// console.log("spar_"+lv, currentActiveCategory);
		let pds = comparison_data["spar_"+lv][currentActiveCategory];
		competitors.forEach(function (competitor, competitiorIndex) {
			let tl = pds[competitor];
			// console.log(tl);
			tl.forEach(function(item, index) {
				if(parseFloat(item[5]) > min_price && parseFloat(item[5]) < max_price ) {
					allProductsMap["spar_"+lv][category][competitor].push(item);
				}
			});
		});
	});
	console.log(allProductsMap);	
	return allProductsMap;
}

			var allTHProducts = {};
function calcTolerance(val = undefined) {
	// console.log(val,"calculating tolerance");
	inflateModal();
	let lv =  ["lower","greater","equal"];
	// let cats = ["fruits_and_vegetables"];
	if (!val ) {
		val = document.getElementById("tolerance_input").value;
		console.log("given val", val);
	}
	if (isNaN(val) || val == ""){
		showSnackbar('Enter a valid number');
		return;
	}
	// console.log(val);
	document.getElementById("modal_tolerance_value").innerHTML = val;
	allTHProducts = {}
	var tmpComp = [];
	lv.forEach(function(level, levelIndex){
		allTHProducts[level] = {};
		data["categories"].forEach(function(category, categoryIndex){
			allTHProducts[level][category] = {};
			competitors.forEach(function(competitor, competitiorIndex){
				// console.log(level, category, competitor);
				allTHProducts[level][category][competitor] = [];
				comparison_data["spar_"+level][category][competitor].forEach(function(item, index){
					let spar_price = parseInt(item[12]);
					let comp_price = parseInt(item[5]);
					let p_no = spar_price * (val/100);
					let prange = spar_price+p_no;
					let nrange = spar_price-p_no;
					if (comp_price >= prange ||
						comp_price <= nrange) {
						// console.log(item);
						// console.log("hit",comp_price,prange,nrange);
						tmpComp.push(item);
					} else {
						// console.log("miss",comp_price,prange,nrange);
					}
				});
				if(tmpComp.length > 0){
					// console.log(level, category, competitor, tmpComp);
					allTHProducts[level][category][competitor] = tmpComp;
					tmpComp = []
				}
			});
		});	
	});
	// console.log(allTHProducts);
	competitors.forEach(function(competitor, competitiorIndex){
		// console.log(competitor);
		let ddata = [];
		let headingSpan = document.createElement("span");
		headingSpan.innerHTML = competitor.replace("_"," ").toUpperCase();
		headingSpan.className = "modal_heading_span";
		let table = document.createElement("table");
		table.setAttribute("id",competitor+"_tolerance_table");
		table.className = "custom_table";
		document.getElementById("toleranceTableModalContent").appendChild(headingSpan);
		document.getElementById("toleranceTableModalContent").appendChild(table);
		lv.forEach(function(level,levelIndex) {
			// console.log(allTHProducts);
			allTHProducts[level][currentActiveCategory][competitor].forEach(function(item,index){
			ddata.push(item);
			});
		})
		// console.log(ddata);
		// console.log("THHHHHHHHH");
		fillTables(competitor, ddata, competitor+"_tolerance_table");	
	});
}

function sendRangeReport() {
	showSnackbar('Reports will be sent to your email address. Please check your inbox in a moment. ');

	let min_price = document.getElementById("min_price_input").value;
	let max_price = document.getElementById("max_price_input").value;

	if (!min_price) {
		min_price = "default";
	}

	if (!max_price) {
		max_price = "default";
	}

	min_price = "min_price="+min_price;
	max_price = "max_price="+max_price;
	let comparison = "comparison="+selected_comparison;
	let category = "category="+currentActiveCategory;
	let email = "email="+getCookie("username");
	let location = "location="+currentActiveLocation;

	XhrCalls.getData("/filter", {} , [min_price, max_price, comparison, category, email, location], function(response) {
		console.log(response);
	})

}

function doAnalysisForRange(){
	let min_price = min_range_input_element.value;
	let max_price = max_price_input_element.value;
	let lv = ["lower", "greater", "equal"];
	// console.log(min_price, max_price);
	let newCompData= getProductsInPriceRange(min_price, max_price);
	updateGraphs("", newCompData );
	lv.forEach(function(competitior, competitiorIndex){
		dispLowOrHigh(competitior, newCompData);
	}); 
}

async function 	updatePercentagesOnStartup() {
	// console.log("updatePercentages");
	await custSleep(1000);
	updatePercentages();
};

async function selectCategoryOnStartup() {
	// console.log("clicking_tmp_bt");
	await custSleep(1000);
	document.getElementById("tmp_bt_id").click();
}

// custSleep(2000).then(() => {
    // Do something after the sleep!
// });
// console.log("Do something after the sleep!");
// custSleep(5000).then(() => {
    // Do something after the sleep!
// });

// (function () {
//     console.log("Do something after the sleep!");
// 	console.log(getCookie("username"));
	
//     if (getCookie("username") == "") {
// 		// window.location.href = "/";
// 	}
	
//     document.getElementById("user_account").addEventListener("click", function(e) {
//         console.log("Logging out.");
// 		setCookie("username", "", 10);
// 		location.reload(true);
// 	});

// 	client = data["client"];
// 	competitors = data["competitors"];
// 	categories = data["categories"];
// 	total_products = data["total_products"];
// 	comparison_data = data["comparison_data"];
// 	comparison_data_for_tables = comparison_data;
// 	min_range_input_element = document.getElementById("min_price_input");
// 	max_price_input_element = document.getElementById("max_price_input");
// 	generateCategoryLowHighData(comparison_data);
// 	client_total_products = getTotalProducts(client);
// 	// console.log(client_total_products);
// 	let spar_total_products = client_total_products;
// 	applyUI();
// 	let min_max_price_value = {};
	
//     categories.forEach(function (category, categoryIndex) {
//         min_max_price_value[category] = [];
// 		console.log(category)
// 		try {
// 			let min_price_value = Math.min(...[comparison_data["min_max_price"][category]["spar"][0][0][0], comparison_data["min_max_price"][category]["big_basket"][0][0][0], comparison_data["min_max_price"][category]["spencers"][0][0][0],comparison_data["min_max_price"][category]["grofers"][0][0][0],comparison_data["min_max_price"][category]["dmart"][0][0][0]]);
// 			let max_price_value = Math.max(...[comparison_data["min_max_price"][category]["spar"][1][0][0], comparison_data["min_max_price"][category]["big_basket"][1][0][0], comparison_data["min_max_price"][category]["spencers"][1][0][0],comparison_data["min_max_price"][category]["grofers"][0][0][0],comparison_data["min_max_price"][category]["dmart"][1][0][0]]);
			
// 			min_max_price_value[category].push(min_price_value);
// 			min_max_price_value[category].push(max_price_value);
// 		} catch(e) {
// 			console.log(e);
// 			min_price_value = 0;
// 			max_price_value = 0;
// 		}
// 	});
		
//     console.log(min_max_price_value);
// 	console.log(selected_category);	
// 		try {
// 		document.getElementById("min_price_input").value = min_max_price_value[selected_category][0]; //min_price_value;
// 		document.getElementById("max_price_input").value = min_max_price_value[selected_category][1];
// 	} catch (err) {
// 		console.log(err);
// 	}
	
// 	// document.getElementById("tmp_bt_id").click();
// 	updatePercentagesOnStartup();
// 	selectCategoryOnStartup();
// })();

function changelocation(location) {
	currentActiveLocation = location;
	console.log("Changing location to "+location);
	let currentLocationElement = document.getElementById("currentLocation");
	currentLocationElement.innerHTML = location.toUpperCase();
	if (!pdata["data"].hasOwnProperty(location)) {
		showSnackbar("Location being updated. Please try in sometime.");
		return;
	}
	let ddata = pdata["data"][location];
	data = ddata;
	total_products = ddata["total_products"];
	comparison_data = ddata["comparison_data"];
	comparison_data_for_tables = comparison_data;
	min_range_input_element = document.getElementById("min_price_input");
	max_price_input_element = document.getElementById("max_price_input");


	generateCategoryLowHighData(comparison_data);
	// console.log(allCatLowHigh);

	client_total_products = getTotalProducts(client);
	// console.log("Total Products: "+client_total_products);

	let spar_total_products = client_total_products;
	
	applyUI();


	// Code has to be optimized - this is a tiny feature to automatically load min and max values on input filds of RANGE, not a show stopper.
	let min_max_price_value = {};
	
    categories.forEach(function (category, categoryIndex) {
        min_max_price_value[category] = [];
		// console.log(category)
		try {
			let min_price_value = Math.min(...[comparison_data["min_max_price"][category]["spar"][0][0][0], comparison_data["min_max_price"][category]["big_basket"][0][0][0], comparison_data["min_max_price"][category]["spencers"][0][0][0],comparison_data["min_max_price"][category]["grofers"][0][0][0],comparison_data["min_max_price"][category]["dmart"][0][0][0]]);
			let max_price_value = Math.max(...[comparison_data["min_max_price"][category]["spar"][1][0][0], comparison_data["min_max_price"][category]["big_basket"][1][0][0], comparison_data["min_max_price"][category]["spencers"][1][0][0],comparison_data["min_max_price"][category]["grofers"][0][0][0],comparison_data["min_max_price"][category]["dmart"][1][0][0]]);
			
			min_max_price_value[category].push(min_price_value);
			min_max_price_value[category].push(max_price_value);
		} catch(e) {
			// console.log(e);
			min_price_value = 0;
			max_price_value = 0;
		}
	});

	
    // console.log(min_max_price_value);
	// console.log(selected_category);	
	try {
		document.getElementById("min_price_input").value = min_max_price_value[selected_category][0]; //min_price_value;
		document.getElementById("max_price_input").value = min_max_price_value[selected_category][1];
	} catch (err) {
		// console.log(err);
	}

	//RANGE min/max auto fill ends..
	
	// document.getElementById("tmp_bt_id").click();

	
	
	////////////////////////////////////////////////////////////
	currentActiveCategory = "FRUIT AND VEG";

	dispLowOrHigh("lower");
	// console.log(allCatLowHigh);

	updatePercentagesOnStartup();
	selectCategoryOnStartup();
}

function showLocations() {
	console.log(pdata["locations"]);
	let list_wrapper = document.getElementById("location_list");
	list_wrapper.innerHTML = "";
	locations.forEach(location => {
		let li = document.createElement("p");
		li.innerHTML = location.toUpperCase();
		li.addEventListener('click', function (event) {
			changelocation(location)
		}
		);
		list_wrapper.appendChild(li);
	});

}

(function() {
	// console.log("load");

	// console.log("Do something after the sleep!");
	// console.log(getCookie("username"));

	XhrCalls = new ApiCalls('http://164.52.216.56');
	
    if (getCookie("username") == "") {
		window.location.href = "/";
	}
	
    document.getElementById("user_account").addEventListener("click", function(e) {
        console.log("Logging out.");
		deleteCookie("username");
		window.location.href = "/";
	});

	client = pdata["client"];
	locations = pdata["locations"];
	competitors = pdata["competitors"];
	categories = pdata["categories"];
	changelocation("bangalore");

})();

window.onload = (event) => {
		// await custSleep(5000);
		var bigBasketCtx = document.getElementById('sparVsBigBasketChart').getContext('2d');
		var spencersCtx = document.getElementById('sparVsSpencersChart').getContext('2d');
		var grofersCtx = document.getElementById('sparVsGrofersChart').getContext('2d');
		var dmartCtx = document.getElementById('sparVsDmartChart').getContext('2d');
		var amazonCtx = document.getElementById('sparVsAmazonChart').getContext('2d');
		ctxMap = {
			"bigbasket_pie_context" : bigBasketCtx,
			"spencers_pie_context" : spencersCtx,
			"grofers_pie_context" : grofersCtx,
			"dmart_pie_context" : dmartCtx,
			"amazon_pie_context" : amazonCtx
		};

		competitors.forEach(function (item, index) {
			let pie = createGraph(ctxMap[item+"_pie_context"], [100, 40, 130]);
			graphMap[item+"_pie"] = pie;
		});
	
		// updateGraphs("FRUIT AND VEG");

		// document.getElementById("tmp_bt_id").click();
		return;
}
