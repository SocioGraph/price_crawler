function inflateModal() {
    // console.log("Inflating modal");
    // Get the modal
    var modal = document.getElementById("toleranceTableModal");
    
    // Get the <span> element that closes the modal
    var closeBtn = document.getElementById("modalCloseButton");
    modal.style.display = "block";
    
    // When the user clicks on <span> (x), close the modal
    closeBtn.onclick = function() {
        modal.style.display = "none";
        document.getElementById("toleranceTableModalContent").innerHTML = "";
    }
    
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
            document.getElementById("toleranceTableModalContent").innerHTML = "";
        }
    }
}