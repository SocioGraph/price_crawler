var data = {
    "client": "spar",
    "competitors": [
      "dmart",
      "spencers",
      "grofers",
      "big_basket"
    ],
    "categories": [
      "fruits_and_vegetables",
      "grocery"
    ],
    "total_products": {
      "spar": {
        "fruits_and_vegetables": [
          189
        ],
        "grocery": [
          2258
        ]
      },
      "dmart": {
        "fruits_and_vegetables": [
          61,
          21
        ],
        "grocery": [
          557,
          0
        ]
      },
      "spencers": {
        "fruits_and_vegetables": [
          162,
          27
        ],
        "grocery": [
          717,
          0
        ]
      },
      "grofers": {
        "fruits_and_vegetables": [
          34,
          16
        ],
        "grocery": [
          [],
          0
        ]
      },
      "big_basket": {
        "fruits_and_vegetables": [
          497,
          69
        ],
        "grocery": [
          2380,
          0
        ]
      }
    },
    "comparison_data": {
      "min_max_price": {
        "fruits_and_vegetables": {
          "spar": [
            [
              [
                "100.0",
                "100.0",
                127456769,
                500.0,
                "1 kg"
              ]
            ],
            [
              [
                "99.0",
                "99.0",
                100024699,
                16.5,
                "1 piece"
              ]
            ]
          ],
          "dmart": [
            [
              [
                8.0,
                20.0
              ]
            ],
            [
              [
                185.0,
                200.0
              ]
            ]
          ],
          "spencers": [
            [
              [
                3.5,
                3.5
              ]
            ],
            [
              [
                1000.0,
                1000.0
              ]
            ]
          ],
          "grofers": [
            [
              [
                7.0,
                10.0
              ]
            ],
            [
              [
                193.0,
                251.0
              ]
            ]
          ],
          "big_basket": [
            [
              [
                4.0,
                20.0
              ]
            ],
            [
              [
                399.0,
                1995.0
              ]
            ]
          ]
        },
        "grocery": {
          "spar": [
            [
              [
                "10.0",
                "10.0",
                100002445,
                370.3703703703704,
                "1 kg"
              ]
            ],
            [
              [
                "99.0",
                "99.0",
                136106331,
                1375.0,
                "1 kg"
              ]
            ]
          ],
          "dmart": [
            [
              [
                7.0,
                14.0
              ]
            ],
            [
              [
                2110.0,
                2600.0
              ]
            ]
          ],
          "spencers": [
            [
              [
                6.0,
                6.0
              ]
            ],
            [
              [
                3865.0,
                4750.0
              ]
            ]
          ],
          "grofers": [
            [],
            []
          ],
          "big_basket": [
            [
              [
                8.0,
                26.666666666666668
              ]
            ],
            [
              [
                3195.73,
                5706.660714285715
              ]
            ]
          ]
        }
      },
      "spar_greater": {
        "fruits_and_vegetables": {
          "spencers": [
            [
              "Fresh Value",
              "Green Long Brinjal",
              "250g",
              [
                18.75,
                18.75
              ],
              "1 kg",
              75.0,
              "NA",
              "BRINJAL GREEN LONG",
              "1 KG",
              [
                "59.0",
                "79.0"
              ],
              100026981,
              "1 kg",
              59.0
            ],
            [
              "Fresh Value",
              "Grape Fruit",
              "300g",
              [
                59.7,
                59.7
              ],
              "1 kg",
              199.00000000000003,
              "NA",
              "GRAPE FRUIT",
              "1 KG",
              [
                "249.0",
                "329.0"
              ],
              111121500,
              "1 kg",
              249.0
            ],
            [
              "Fresh Value",
              "Imported Plum",
              "300g",
              [
                134.7,
                134.7
              ],
              "1 kg",
              449.0,
              "NA",
              "PLUMS - IMPORTED RED",
              "1 KG",
              [
                "359.0",
                "399.0"
              ],
              100026231,
              "1 kg",
              359.0
            ],
            [
              "Fresh Value",
              "Garlic",
              "200g",
              [
                57.8,
                57.8
              ],
              "1 kg",
              288.99999999999994,
              "NA",
              "GARLIC",
              "1 KG",
              [
                "269.0",
                "269.0"
              ],
              100023637,
              "1 kg",
              269.0
            ],
            [
              "Fresh Value",
              "Royal Gala Apple",
              "500g - 700g",
              [
                101.4,
                132.0
              ],
              "1 kg",
              202.8,
              "NA",
              "APPLE ROYALGALA",
              "1 KG",
              [
                "199.0",
                "369.0"
              ],
              100025659,
              "1 kg",
              199.0
            ],
            [
              "Fresh Value",
              "Onion",
              "1kg",
              [
                35.0,
                35.0
              ],
              "1 kg",
              35.0,
              "NA",
              "ONION LOCAL",
              "1 KG",
              [
                "39.0",
                "165.0"
              ],
              111594446,
              "1 kg",
              39.0
            ],
            [
              "Fresh Value",
              "Green Zuccini",
              "200g",
              [
                49.8,
                49.8
              ],
              "1 kg",
              248.99999999999997,
              "NA",
              "ZUCCHINI GREEN",
              "1 KG",
              [
                "79.0",
                "199.0"
              ],
              100027263,
              "1 kg",
              79.0
            ],
            [
              "Fresh Value",
              "Snake Gourd",
              "250g",
              [
                23.75,
                23.75
              ],
              "1 kg",
              95.0,
              "NA",
              "SNAKE GOURD SHORT",
              "1 KG",
              [
                "29.0",
                "75.9"
              ],
              100023589,
              "1 kg",
              29.0
            ],
            [
              "Fresh Value",
              "Avacoda Butter Fruit",
              "250g",
              [
                124.75,
                124.75
              ],
              "1 kg",
              499.0,
              "NA",
              "AVOCADO",
              "1 KG",
              [
                "399.0",
                "399.0"
              ],
              100026707,
              "1 kg",
              399.0
            ],
            [
              "Fresh Value",
              "Green Chilli",
              "100g",
              [
                9.0,
                9.0
              ],
              "1 kg",
              90.0,
              "NA",
              "CHILLY GREEN LONG",
              "1 KG",
              [
                "39.0",
                "129.0"
              ],
              100023631,
              "1 kg",
              39.0
            ],
            [
              "Fresh Value",
              "Bitter Gourd",
              "250g",
              [
                24.75,
                24.75
              ],
              "1 kg",
              99.0,
              "NA",
              "BITTER GOURD GREEN",
              "1 KG",
              [
                "28.0",
                "99.0"
              ],
              100023391,
              "1 kg",
              28.0
            ],
            [
              "Fresh Value",
              "Beans",
              "250g",
              [
                7.5,
                7.5
              ],
              "1 kg",
              30.0,
              "NA",
              "BEANS BUSH",
              "1 KG",
              [
                "39.0",
                "169.0"
              ],
              100776471,
              "1 kg",
              39.0
            ],
            [
              "Fresh Value",
              "Knool Khol",
              "500g",
              [
                14.5,
                14.5
              ],
              "1 kg",
              29.0,
              "NA",
              "KNOLKHOL",
              "1 KG",
              [
                "29.0",
                "89.0"
              ],
              100023861,
              "1 kg",
              29.0
            ],
            [
              "NA",
              "French Cucumber",
              "500g",
              [
                6.5,
                9.5
              ],
              "1 kg",
              13.0,
              "NA",
              "CUCUMBER GREEN",
              "1 KG",
              [
                "16.0",
                "49.0"
              ],
              100023371,
              "1 kg",
              16.0
            ],
            [
              "Fresh Value",
              "Beetroot",
              "500g",
              [
                19.5,
                19.5
              ],
              "1 kg",
              39.0,
              "NA",
              "BEET ROOT",
              "1 KG",
              [
                "29.0",
                "59.0"
              ],
              100023605,
              "1 kg",
              29.0
            ],
            [
              "Fresh Value",
              "Baby Potato",
              "500g",
              [
                7.5,
                7.5
              ],
              "1 kg",
              15.0,
              "NA",
              "POTATO BABY",
              "500 G",
              [
                "29.0",
                "29.0"
              ],
              100026747,
              "1 kg",
              58.0
            ],
            [
              "Fresh Value",
              "Tomato",
              "1kg",
              [
                12.0,
                15.0
              ],
              "1 kg",
              12.0,
              "NA",
              "TOMATO HYBRID",
              "1 KG",
              [
                "22.0",
                "59.0"
              ],
              100026735,
              "1 kg",
              22.0
            ],
            [
              "Fresh Value",
              "Peas",
              "500g",
              [
                12.5,
                20.0
              ],
              "1 kg",
              25.0,
              "NA",
              "PEAS DELHI",
              "1 KG",
              [
                "59.0",
                "199.0"
              ],
              100023887,
              "1 kg",
              59.0
            ],
            [
              "Fresh Value",
              "Banana Flower",
              "1 Piece",
              [
                27.0,
                27.0
              ],
              "1 piece",
              27.0,
              "NA",
              "BANANA FLOWER",
              "1 PC",
              [
                "29.0",
                "29.0"
              ],
              100023865,
              "1 piece",
              29.0
            ],
            [
              "Fresh Value",
              "Carrot",
              "500g",
              [
                11.0,
                14.0
              ],
              "1 kg",
              22.0,
              "NA",
              "CARROT KOLAR",
              "1 KG",
              [
                "49.0",
                "89.0"
              ],
              100026965,
              "1 kg",
              49.0
            ]
          ],
          "grofers": [
            [
              "NA",
              "Onion",
              "1 kg",
              [
                37.0,
                49.0
              ],
              "1 kg",
              37.0,
              "NA",
              "ONION LOCAL",
              "1 KG",
              [
                "39.0",
                "165.0"
              ],
              111594446,
              "1 kg",
              39.0
            ],
            [
              "NA",
              "Potato",
              "1 kg",
              [
                29.0,
                38.0
              ],
              "1 kg",
              29.0,
              "NA",
              "POTATO",
              "1 KG",
              [
                "34.0",
                "52.0"
              ],
              100026745,
              "1 kg",
              34.0
            ],
            [
              "NA",
              "Tomato - Hybrid",
              "1 kg",
              [
                18.0,
                24.0
              ],
              "1 kg",
              18.0,
              "NA",
              "TOMATO HYBRID",
              "1 KG",
              [
                "22.0",
                "59.0"
              ],
              100026735,
              "1 kg",
              22.0
            ],
            [
              "NA",
              "Cucumber",
              "500 g",
              [
                14.0,
                19.0
              ],
              "1 kg",
              28.0,
              "NA",
              "CUCUMBER GREEN",
              "1 KG",
              [
                "16.0",
                "49.0"
              ],
              100023371,
              "1 kg",
              16.0
            ],
            [
              "NA",
              "Local Carrot",
              "500 g",
              [
                13.0,
                17.0
              ],
              "1 kg",
              26.0,
              "NA",
              "CARROT KOLAR",
              "1 KG",
              [
                "49.0",
                "89.0"
              ],
              100026965,
              "1 kg",
              49.0
            ],
            [
              "NA",
              "Ginger",
              "200 g",
              [
                22.0,
                29.0
              ],
              "1 kg",
              110.0,
              "NA",
              "GINGER FRESH",
              "1 KG",
              [
                "99.0",
                "249.0"
              ],
              100023633,
              "1 kg",
              99.0
            ],
            [
              "NA",
              "Desi Tomato",
              "1kg",
              [
                16.0,
                21.0
              ],
              "1 kg",
              16.0,
              "NA",
              "TOMATO COUNTRY",
              "1 KG",
              [
                "19.0",
                "65.0"
              ],
              100026731,
              "1 kg",
              19.0
            ],
            [
              "NA",
              "Garlic",
              "200 g",
              [
                49.0,
                64.0
              ],
              "1 kg",
              245.0,
              "NA",
              "GARLIC",
              "1 KG",
              [
                "269.0",
                "269.0"
              ],
              100023637,
              "1 kg",
              269.0
            ],
            [
              "NA",
              "Beetroot",
              "500 g",
              [
                14.0,
                19.0
              ],
              "1 kg",
              28.0,
              "NA",
              "BEET ROOT",
              "1 KG",
              [
                "29.0",
                "59.0"
              ],
              100023605,
              "1 kg",
              29.0
            ],
            [
              "NA",
              "Cauliflower",
              "1 unit ",
              [
                21.0,
                28.0
              ],
              "1 piece",
              21.0,
              "NA",
              "CAULIFLOWER",
              "1 PC",
              [
                "28.0",
                "45.0"
              ],
              100026995,
              "1 piece",
              28.0
            ],
            [
              "NA",
              "Nagpur Orange",
              "1 kg",
              [
                57.0,
                75.0
              ],
              "1 kg",
              57.0,
              "NA",
              "ORANGE NAGPUR",
              "1 KG",
              [
                "59.0",
                "109.0"
              ],
              100026461,
              "1 kg",
              59.0
            ],
            [
              "NA",
              "Sweet Potato",
              "500 g",
              [
                36.0,
                47.0
              ],
              "1 kg",
              72.0,
              "NA",
              "SWEET POTATO",
              "1 KG",
              [
                "45.0",
                "99.0"
              ],
              100023615,
              "1 kg",
              45.0
            ],
            [
              "NA",
              "Green Grapes",
              "500 g",
              [
                69.0,
                90.0
              ],
              "1 kg",
              138.0,
              "NA",
              "GRAPES SONAKA",
              "1 KG",
              [
                "89.0",
                "199.0"
              ],
              100026479,
              "1 kg",
              89.0
            ],
            [
              "NA",
              "Sweet Tamarind  - Box",
              "250 g",
              [
                96.0,
                125.0
              ],
              "1 kg",
              384.0,
              "NA",
              "SWEET TAMARIND",
              "250GM",
              [
                "99.0",
                "149.0"
              ],
              100535105,
              "1 kg",
              396.0
            ]
          ],
          "dmart": [
            [
              "Fresh",
              "Grapes Black",
              "500 gms",
              [
                69.0,
                150.0
              ],
              "1 kg",
              138.0,
              "NA",
              "GRAPES SHARAD",
              "1 KG",
              [
                "169.0",
                "303.0"
              ],
              100026477,
              "1 kg",
              169.0
            ],
            [
              "Fresh",
              "Onion",
              "1 kg",
              [
                32.0,
                60.0
              ],
              "1 kg",
              32.0,
              "NA",
              "ONION LOCAL",
              "1 KG",
              [
                "39.0",
                "165.0"
              ],
              111594446,
              "1 kg",
              39.0
            ],
            [
              "Fresh",
              "Potato",
              "1 kg",
              [
                29.0,
                45.0
              ],
              "1 kg",
              29.0,
              "NA",
              "POTATO",
              "1 KG",
              [
                "34.0",
                "52.0"
              ],
              100026745,
              "1 kg",
              34.0
            ],
            [
              "NA",
              "Garlic",
              "200 gms",
              [
                62.0,
                80.0
              ],
              "1 kg",
              310.0,
              "NA",
              "GARLIC",
              "1 KG",
              [
                "269.0",
                "269.0"
              ],
              100023637,
              "1 kg",
              269.0
            ],
            [
              "Fresh",
              "Tomato",
              "500 gms",
              [
                14.0,
                30.0
              ],
              "1 kg",
              28.0,
              "NA",
              "TOMATO HYBRID",
              "1 KG",
              [
                "22.0",
                "59.0"
              ],
              100026735,
              "1 kg",
              22.0
            ],
            [
              "NA",
              "Ginger",
              "200 gms",
              [
                25.0,
                40.0
              ],
              "1 kg",
              125.0,
              "NA",
              "GINGER FRESH",
              "1 KG",
              [
                "99.0",
                "249.0"
              ],
              100023633,
              "1 kg",
              99.0
            ],
            [
              "Fresh",
              "Carrot",
              "500 gms",
              [
                25.0,
                60.0
              ],
              "1 kg",
              50.0,
              "NA",
              "CARROT KOLAR",
              "1 KG",
              [
                "49.0",
                "89.0"
              ],
              100026965,
              "1 kg",
              49.0
            ],
            [
              "Fresh",
              "Beetroot",
              "500 gms",
              [
                19.0,
                40.0
              ],
              "1 kg",
              38.0,
              "NA",
              "BEET ROOT",
              "1 KG",
              [
                "29.0",
                "59.0"
              ],
              100023605,
              "1 kg",
              29.0
            ],
            [
              "Fresh",
              "Baby Potato",
              "500 gms",
              [
                23.0,
                35.0
              ],
              "1 kg",
              46.0,
              "NA",
              "POTATO BABY",
              "500 G",
              [
                "29.0",
                "29.0"
              ],
              100026747,
              "1 kg",
              58.0
            ],
            [
              "Fresh",
              "Green Chilli",
              "100 gms",
              [
                8.0,
                20.0
              ],
              "1 kg",
              80.0,
              "NA",
              "CHILLY GREEN LONG",
              "1 KG",
              [
                "39.0",
                "129.0"
              ],
              100023631,
              "1 kg",
              39.0
            ],
            [
              "NA",
              "Bitter Gourd",
              "300 gms",
              [
                19.0,
                35.0
              ],
              "1 kg",
              63.333333333333336,
              "NA",
              "BITTER GOURD GREEN",
              "1 KG",
              [
                "28.0",
                "99.0"
              ],
              100023391,
              "1 kg",
              28.0
            ],
            [
              "NA",
              "French Beans",
              "250 gms",
              [
                15.0,
                30.0
              ],
              "1 kg",
              60.0,
              "NA",
              "BEANS FRENCH RING",
              "1 KG",
              [
                "45.0",
                "149.0"
              ],
              100023349,
              "1 kg",
              45.0
            ],
            [
              "NA",
              "Zucchini Green",
              "250 gms",
              [
                25.0,
                40.0
              ],
              "1 kg",
              100.0,
              "NA",
              "ZUCCHINI GREEN",
              "1 KG",
              [
                "79.0",
                "199.0"
              ],
              100027263,
              "1 kg",
              79.0
            ],
            [
              "NA",
              "Zucchini Yellow",
              "250 gms",
              [
                35.0,
                40.0
              ],
              "1 kg",
              140.0,
              "NA",
              "ZUCCHINI YELLOW",
              "1 KG",
              [
                "85.0",
                "219.0"
              ],
              100027261,
              "1 kg",
              85.0
            ],
            [
              "Fresh",
              "Sprouts Mixed Gram",
              "200 gms",
              [
                29.0,
                40.0
              ],
              "1 kg",
              145.0,
              "NA",
              "SPROUTAMINS MIXED SPROUTS",
              "200G",
              [
                "39.0",
                "39.0"
              ],
              100023853,
              "1 kg",
              195.0
            ],
            [
              "Fresh",
              "Sprouts Moong Green",
              "200 gms",
              [
                29.0,
                40.0
              ],
              "1 kg",
              145.0,
              "NA",
              "SPROUTAMINS GREEN GRAM SPROUTS",
              "200G",
              [
                "39.0",
                "39.0"
              ],
              100023657,
              "1 kg",
              195.0
            ],
            [
              "Fresh",
              "Cabbage - Red",
              "400 gms",
              [
                38.0,
                60.0
              ],
              "1 kg",
              95.0,
              "NA",
              "CABBAGE RED",
              "1 KG",
              [
                "89.0",
                "149.0"
              ],
              100027005,
              "1 kg",
              89.0
            ]
          ],
          "big_basket": [
            [
              "Fresho",
              "Onion",
              "1 kg",
              [
                35.0,
                175.0
              ],
              "1 kg",
              35.0,
              "NA",
              "ONION LOCAL",
              "1 KG",
              [
                "39.0",
                "165.0"
              ],
              111594446,
              "1 kg",
              39.0
            ],
            [
              "Fresho",
              "Potato",
              "1 kg",
              [
                27.0,
                135.0
              ],
              "1 kg",
              27.0,
              "NA",
              "POTATO",
              "1 KG",
              [
                "34.0",
                "52.0"
              ],
              100026745,
              "1 kg",
              34.0
            ],
            [
              "Fresho",
              "Tomato - Hybrid",
              "1 kg",
              [
                15.0,
                75.0
              ],
              "1 kg",
              15.0,
              "NA",
              "TOMATO HYBRID",
              "1 KG",
              [
                "22.0",
                "59.0"
              ],
              100026735,
              "1 kg",
              22.0
            ],
            [
              "Fresho",
              "Cauliflower",
              "1 pc",
              [
                12.0,
                60.0
              ],
              "1 piece",
              12.0,
              "NA",
              "CAULIFLOWER",
              "1 PC",
              [
                "28.0",
                "45.0"
              ],
              100026995,
              "1 piece",
              28.0
            ],
            [
              "Fresho",
              "Carrot - Local",
              "500 g",
              [
                18.0,
                90.0
              ],
              "1 kg",
              36.0,
              "NA",
              "CARROT KOLAR",
              "1 KG",
              [
                "49.0",
                "89.0"
              ],
              100026965,
              "1 kg",
              49.0
            ],
            [
              "Fresho",
              "Banana - Yelakki",
              "500 g",
              [
                36.0,
                180.0
              ],
              "1 kg",
              72.0,
              "NA",
              "BANANA YELAKKI",
              "1 KG",
              [
                "69.0",
                "115.0"
              ],
              100025893,
              "1 kg",
              69.0
            ],
            [
              "Fresho",
              "Tomato - Local",
              "1 kg",
              [
                12.0,
                60.0
              ],
              "1 kg",
              12.0,
              "NA",
              "TOMATO COUNTRY",
              "1 KG",
              [
                "19.0",
                "65.0"
              ],
              100026731,
              "1 kg",
              19.0
            ],
            [
              "Fresho",
              "Tomato - Local, Organically Grown",
              "500 g",
              [
                9.0,
                45.0
              ],
              "1 kg",
              18.0,
              "NA",
              "ORGANIC TOMATO",
              "1KG",
              [
                "39.0",
                "79.0"
              ],
              100024401,
              "1 kg",
              39.0
            ],
            [
              "Fresho",
              "Broccoli",
              "500 g",
              [
                49.0,
                245.0
              ],
              "1 kg",
              98.0,
              "NA",
              "BROCCOLI",
              "1 KG",
              [
                "89.0",
                "299.0"
              ],
              100027265,
              "1 kg",
              89.0
            ],
            [
              "Fresho",
              "Beans - Haricot",
              "1 kg",
              [
                29.0,
                145.0
              ],
              "1 kg",
              29.0,
              "NA",
              "BEANS BUSH",
              "1 KG",
              [
                "39.0",
                "169.0"
              ],
              100776471,
              "1 kg",
              39.0
            ],
            [
              "Fresho",
              "Carrot - Red",
              "1 kg",
              [
                79.0,
                395.0
              ],
              "1 kg",
              79.0,
              "NA",
              "CARROT OOTY",
              "1 KG",
              [
                "105.0",
                "129.0"
              ],
              100026967,
              "1 kg",
              105.0
            ],
            [
              "Fresho",
              "Bottle Gourd",
              "1 kg",
              [
                18.0,
                90.0
              ],
              "1 kg",
              18.0,
              "NA",
              "BOTTLE GOURD LONG",
              "1 KG",
              [
                "19.0",
                "369.0"
              ],
              100023395,
              "1 kg",
              19.0
            ],
            [
              "Fresho",
              "Grapes - Sonaka Seedless",
              "500 g",
              [
                55.0,
                275.0
              ],
              "1 kg",
              110.0,
              "NA",
              "GRAPES SONAKA",
              "1 KG",
              [
                "89.0",
                "199.0"
              ],
              100026479,
              "1 kg",
              89.0
            ],
            [
              "Fresho",
              "Beetroot",
              "1 kg",
              [
                22.0,
                110.0
              ],
              "1 kg",
              22.0,
              "NA",
              "BEET ROOT",
              "1 KG",
              [
                "29.0",
                "59.0"
              ],
              100023605,
              "1 kg",
              29.0
            ],
            [
              "Fresho",
              "Garlic",
              "100 g",
              [
                15.0,
                75.0
              ],
              "1 kg",
              150.0,
              "NA",
              "GARLIC",
              "1 KG",
              [
                "269.0",
                "269.0"
              ],
              100023637,
              "1 kg",
              269.0
            ],
            [
              "Fresho",
              "Radish - White",
              "1 kg",
              [
                18.0,
                90.0
              ],
              "1 kg",
              18.0,
              "NA",
              "RADISH WHITE",
              "1 KG",
              [
                "29.0",
                "69.0"
              ],
              100023607,
              "1 kg",
              29.0
            ],
            [
              "Fresho",
              "Brinjal - Bottle Shape",
              "1 kg",
              [
                39.0,
                195.0
              ],
              "1 kg",
              39.0,
              "NA",
              "BRINJAL SUPHAL",
              "1 KG",
              [
                "58.0",
                "89.0"
              ],
              100026987,
              "1 kg",
              58.0
            ],
            [
              "Fresho",
              "Ridge Gourd",
              "500 g",
              [
                14.0,
                70.0
              ],
              "1 kg",
              28.0,
              "NA",
              "RIDGE GOURD",
              "1 KG",
              [
                "26.0",
                "79.0"
              ],
              100023399,
              "1 kg",
              26.0
            ],
            [
              "Fresho",
              "Chilli - Green Long, Medium",
              "100 g",
              [
                7.0,
                35.0
              ],
              "1 kg",
              70.0,
              "NA",
              "CHILLY GREEN LONG",
              "1 KG",
              [
                "39.0",
                "129.0"
              ],
              100023631,
              "1 kg",
              39.0
            ],
            [
              "Fresho",
              "Bitter Gourd",
              "1 kg",
              [
                25.0,
                125.0
              ],
              "1 kg",
              25.0,
              "NA",
              "BITTER GOURD GREEN",
              "1 KG",
              [
                "28.0",
                "99.0"
              ],
              100023391,
              "1 kg",
              28.0
            ],
            [
              "Fresho",
              "Coccinia",
              "1 kg",
              [
                24.0,
                120.0
              ],
              "1 kg",
              24.0,
              "NA",
              "COCCINIA",
              "1 KG",
              [
                "28.0",
                "59.0"
              ],
              100023873,
              "1 kg",
              28.0
            ],
            [
              "Fresho",
              "Apple - Royal Gala, Regular",
              "1 kg",
              [
                139.0,
                695.0
              ],
              "1 kg",
              139.0,
              "NA",
              "APPLE ROYALGALA",
              "1 KG",
              [
                "199.0",
                "369.0"
              ],
              100025659,
              "1 kg",
              199.0
            ],
            [
              "Fresho",
              "Grapes - Sharad Seedless",
              "500 g",
              [
                89.0,
                445.0
              ],
              "1 kg",
              178.0,
              "NA",
              "GRAPES SHARAD",
              "1 KG",
              [
                "169.0",
                "303.0"
              ],
              100026477,
              "1 kg",
              169.0
            ],
            [
              "Fresho",
              "Beans - French, Round",
              "500 g",
              [
                15.0,
                75.0
              ],
              "1 kg",
              30.0,
              "NA",
              "BEANS FRENCH RING",
              "1 KG",
              [
                "45.0",
                "149.0"
              ],
              100023349,
              "1 kg",
              45.0
            ],
            [
              "Fresho",
              "Brinjal - Varikatri",
              "1 kg",
              [
                29.0,
                145.0
              ],
              "1 kg",
              29.0,
              "NA",
              "BRINJAL PURPLE STRIPES",
              "1 KG",
              [
                "39.0",
                "49.0"
              ],
              100026985,
              "1 kg",
              39.0
            ],
            [
              "Fresho",
              "Baby Potato",
              "500 g",
              [
                18.0,
                90.0
              ],
              "1 kg",
              36.0,
              "NA",
              "POTATO BABY",
              "500 G",
              [
                "29.0",
                "29.0"
              ],
              100026747,
              "1 kg",
              58.0
            ],
            [
              "Fresho",
              "Beans - Cowpea",
              "250 g",
              [
                9.0,
                45.0
              ],
              "1 kg",
              36.0,
              "NA",
              "YARD LONG BEAN",
              "1 KG",
              [
                "59.0",
                "99.0"
              ],
              100023897,
              "1 kg",
              59.0
            ],
            [
              "Fresho",
              "Capsicum - Red",
              "500 g",
              [
                51.0,
                255.0
              ],
              "1 kg",
              102.0,
              "NA",
              "CAPSICUM RED",
              "1 KG",
              [
                "109.0",
                "249.0"
              ],
              100023365,
              "1 kg",
              109.0
            ],
            [
              "Fresho",
              "Sapota",
              "1 kg",
              [
                59.0,
                295.0
              ],
              "1 kg",
              59.0,
              "NA",
              "SAPOTA CRICKET BALL",
              "1 KG",
              [
                "99.0",
                "149.0"
              ],
              100026215,
              "1 kg",
              99.0
            ],
            [
              "Fresho",
              "Sprouts - Mixed Gram",
              "200 g",
              [
                31.0,
                155.0
              ],
              "1 kg",
              155.0,
              "NA",
              "SPROUTAMINS MIXED SPROUTS",
              "200G",
              [
                "39.0",
                "39.0"
              ],
              100023853,
              "1 kg",
              195.0
            ],
            [
              "Fresho",
              "Fresho Capsicum - Yellow",
              "500 g",
              [
                51.0,
                255.0
              ],
              "1 kg",
              102.0,
              "NA",
              "CAPSICUM YELLOW",
              "1 KG",
              [
                "109.0",
                "249.0"
              ],
              100023367,
              "1 kg",
              109.0
            ],
            [
              "Fresho",
              "Chow Chow",
              "250 g",
              [
                8.0,
                40.0
              ],
              "1 kg",
              32.0,
              "NA",
              "CHOW CHOW",
              "1 KG",
              [
                "26.0",
                "85.0"
              ],
              100023871,
              "1 kg",
              26.0
            ],
            [
              "Fresho",
              "Lettuce - Iceberg",
              "1 kg",
              [
                49.0,
                245.0
              ],
              "1 kg",
              49.0,
              "NA",
              "LETTUCE ICEBERG",
              "1 KG",
              [
                "79.0",
                "199.0"
              ],
              100027281,
              "1 kg",
              79.0
            ],
            [
              "Fresho",
              "Guava - Thai",
              "250 g",
              [
                69.0,
                345.0
              ],
              "1 kg",
              276.0,
              "NA",
              "GUAVA PREPACK",
              "1 KG",
              [
                "179.0",
                "299.0"
              ],
              106202532,
              "1 kg",
              179.0
            ],
            [
              "Fresho",
              "Pumpkin - Disco",
              "500 g",
              [
                10.0,
                50.0
              ],
              "1 kg",
              20.0,
              "NA",
              "PUMPKIN DISCO",
              "1 KG",
              [
                "35.0",
                "89.0"
              ],
              100023387,
              "1 kg",
              35.0
            ],
            [
              "Fresho",
              "Cucumber - Mangalore",
              "1 kg",
              [
                14.0,
                70.0
              ],
              "1 kg",
              14.0,
              "NA",
              "CUCUMBER MALABAR",
              "1 KG",
              [
                "39.0",
                "59.0"
              ],
              100023377,
              "1 kg",
              39.0
            ],
            [
              "Fresho",
              "Brinjal - Green, Long",
              "500 g",
              [
                20.0,
                100.0
              ],
              "1 kg",
              40.0,
              "NA",
              "BRINJAL GREEN LONG",
              "1 KG",
              [
                "59.0",
                "79.0"
              ],
              100026981,
              "1 kg",
              59.0
            ],
            [
              "Fresho",
              "Zucchini - Green",
              "250 g",
              [
                15.0,
                75.0
              ],
              "1 kg",
              60.0,
              "NA",
              "ZUCCHINI GREEN",
              "1 KG",
              [
                "79.0",
                "199.0"
              ],
              100027263,
              "1 kg",
              79.0
            ],
            [
              "Fresho",
              "Banana - Red",
              "500 g",
              [
                38.0,
                190.0
              ],
              "1 kg",
              76.0,
              "NA",
              "BANANA RED",
              "1 KG",
              [
                "95.0",
                "109.0"
              ],
              100025903,
              "1 kg",
              95.0
            ],
            [
              "Fresho",
              "Plum - Imported",
              "500 g",
              [
                194.0,
                970.0
              ],
              "1 kg",
              388.0,
              "NA",
              "PLUMS - IMPORTED RED",
              "1 KG",
              [
                "359.0",
                "399.0"
              ],
              100026231,
              "1 kg",
              359.0
            ],
            [
              "Fresho",
              "Banana - Raw Green",
              "500 g",
              [
                25.0,
                125.0
              ],
              "1 kg",
              50.0,
              "NA",
              "BANANA RAW",
              "1 KG",
              [
                "69.0",
                "85.0"
              ],
              100023867,
              "1 kg",
              69.0
            ],
            [
              "Fresho",
              "Sprouts-Moong Green",
              "200 g",
              [
                30.0,
                150.0
              ],
              "1 kg",
              150.0,
              "NA",
              "SPROUTAMINS GREEN GRAM SPROUTS",
              "200G",
              [
                "39.0",
                "39.0"
              ],
              100023657,
              "1 kg",
              195.0
            ],
            [
              "Fresho",
              "Knol Khol",
              "250 g",
              [
                7.0,
                35.0
              ],
              "1 kg",
              28.0,
              "NA",
              "KNOLKHOL",
              "1 KG",
              [
                "29.0",
                "89.0"
              ],
              100023861,
              "1 kg",
              29.0
            ],
            [
              "Fresho",
              "Zucchini - Yellow",
              "1 kg",
              [
                49.0,
                245.0
              ],
              "1 kg",
              49.0,
              "NA",
              "ZUCCHINI YELLOW",
              "1 KG",
              [
                "85.0",
                "219.0"
              ],
              100027261,
              "1 kg",
              85.0
            ],
            [
              "Fresho",
              "Banganapalli Mango",
              "1 kg",
              [
                159.0,
                795.0
              ],
              "1 kg",
              159.0,
              "NA",
              "MANGO BANGANAPALLY",
              "1 KG",
              [
                "199.0",
                "199.0"
              ],
              100026173,
              "1 kg",
              199.0
            ],
            [
              "Fresho",
              "Totapuri Mango",
              "500 gm",
              [
                49.0,
                245.0
              ],
              "1 kg",
              98.0,
              "NA",
              "MANGO TOTAPURI",
              "1 KG",
              [
                "169.0",
                "199.0"
              ],
              100026175,
              "1 kg",
              169.0
            ],
            [
              "Fresho",
              "Onion - White",
              "1 kg",
              [
                77.0,
                385.0
              ],
              "1 kg",
              77.0,
              "NA",
              "ONION WHITE",
              "1 KG",
              [
                "129.0",
                "129.0"
              ],
              100026759,
              "1 kg",
              129.0
            ],
            [
              "Fresho",
              "Banana Stem",
              "1 pc",
              [
                14.0,
                70.0
              ],
              "1 piece",
              14.0,
              "NA",
              "BANANA STEM",
              "1 PC",
              [
                "19.0",
                "30.0"
              ],
              100023869,
              "1 piece",
              19.0
            ],
            [
              "Fresho",
              "Chinese Cabbage",
              "1 kg",
              [
                55.0,
                275.0
              ],
              "1 kg",
              55.0,
              "NA",
              "CHINESE CABBAGE",
              "1 KG",
              [
                "69.0",
                "129.0"
              ],
              100027267,
              "1 kg",
              69.0
            ],
            [
              "Fresho",
              "Banana Flower",
              "1 pc",
              [
                20.0,
                100.0
              ],
              "1 piece",
              20.0,
              "NA",
              "BANANA FLOWER",
              "1 PC",
              [
                "29.0",
                "29.0"
              ],
              100023865,
              "1 piece",
              29.0
            ],
            [
              "Fresho",
              "Rosemary",
              "10 g",
              [
                20.0,
                100.0
              ],
              "1 kg",
              2000.0,
              "NA",
              "ROSEMARY",
              "20G",
              [
                "29.0",
                "29.0"
              ],
              100501621,
              "1 kg",
              1450.0
            ]
          ]
        },
        "grocery": {
          "spencers": [],
          "grofers": [],
          "dmart": [],
          "big_basket": []
        }
      },
      "spar_equal": {
        "fruits_and_vegetables": {
          "spencers": [
            [
              "Natures Best",
              "Moong Sprouts",
              "200g",
              [
                39.0,
                39.0
              ],
              "1 kg",
              195.0,
              "NA",
              "SPROUTAMINS GREEN GRAM SPROUTS",
              "200G",
              [
                "39.0",
                "39.0"
              ],
              100023657,
              "1 kg",
              195.0
            ]
          ],
          "grofers": [],
          "dmart": [],
          "big_basket": [
            [
              "Fresho",
              "Green Peas",
              "1 kg",
              [
                59.0,
                295.0
              ],
              "1 kg",
              59.0,
              "NA",
              "PEAS DELHI",
              "1 KG",
              [
                "59.0",
                "199.0"
              ],
              100023887,
              "1 kg",
              59.0
            ],
            [
              "Fresho",
              "Orange - Nagpur, Regular",
              "1 kg",
              [
                59.0,
                295.0
              ],
              "1 kg",
              59.0,
              "NA",
              "ORANGE NAGPUR",
              "1 KG",
              [
                "59.0",
                "109.0"
              ],
              100026461,
              "1 kg",
              59.0
            ],
            [
              "Fresho",
              "Coconut - Medium",
              "1 pc",
              [
                29.0,
                145.0
              ],
              "1 piece",
              29.0,
              "NA",
              "COCONUT",
              "1 PC",
              [
                "29.0",
                "35.0"
              ],
              100023881,
              "1 piece",
              29.0
            ],
            [
              "Fresho",
              "Ginger",
              "1 kg",
              [
                99.0,
                495.0
              ],
              "1 kg",
              99.0,
              "NA",
              "GINGER FRESH",
              "1 KG",
              [
                "99.0",
                "249.0"
              ],
              100023633,
              "1 kg",
              99.0
            ],
            [
              "Fresho",
              "Cucumber - English",
              "1 kg",
              [
                25.0,
                125.0
              ],
              "1 kg",
              25.0,
              "NA",
              "CUCUMBER EUROPEAN",
              "1 KG",
              [
                "25.0",
                "69.0"
              ],
              100023381,
              "1 kg",
              25.0
            ],
            [
              "Fresho",
              "Avocado",
              "1 kg",
              [
                399.0,
                1995.0
              ],
              "1 kg",
              399.0,
              "NA",
              "AVOCADO",
              "1 KG",
              [
                "399.0",
                "399.0"
              ],
              100026707,
              "1 kg",
              399.0
            ],
            [
              "Fresho",
              "Sweet Potato",
              "1 kg",
              [
                45.0,
                225.0
              ],
              "1 kg",
              45.0,
              "NA",
              "SWEET POTATO",
              "1 KG",
              [
                "45.0",
                "99.0"
              ],
              100023615,
              "1 kg",
              45.0
            ],
            [
              "Fresho",
              "Tamarind - Sweet",
              "225 g",
              [
                99.0,
                495.0
              ],
              "1 kg",
              440.0,
              "NA",
              "SWEET TAMARIND",
              "250GM",
              [
                "99.0",
                "149.0"
              ],
              100535105,
              "1 kg",
              396.0
            ]
          ]
        },
        "grocery": {
          "spencers": [],
          "grofers": [],
          "dmart": [],
          "big_basket": []
        }
      },
      "spar_lower": {
        "fruits_and_vegetables": {
          "spencers": [
            [
              "NA",
              "Pineapple",
              "1pc",
              [
                55.0,
                55.0
              ],
              "1 piece",
              55.0,
              "SPAR FRESH ",
              "PINEAPPLE",
              "1PC",
              [
                "49.0",
                "99.0"
              ],
              114406826,
              "1 piece",
              49.0
            ],
            [
              "NA",
              "Green Grapes",
              "1Kg",
              [
                99.0,
                129.0
              ],
              "1 kg",
              99.0,
              "NA",
              "GRAPES SONAKA",
              "1 KG",
              [
                "89.0",
                "199.0"
              ],
              100026479,
              "1 kg",
              89.0
            ],
            [
              "Fresh Value",
              "Kabul Pomegranate",
              "1kg",
              [
                169.0,
                169.0
              ],
              "1 kg",
              169.0,
              "NA",
              "POMEGRANATE ARAKTA",
              "1 KG",
              [
                "115.0",
                "199.0"
              ],
              100025917,
              "1 kg",
              115.0
            ],
            [
              "Fresh Value",
              "Lettuce Iceberg",
              "500g",
              [
                149.5,
                149.5
              ],
              "1 kg",
              299.0,
              "NA",
              "LETTUCE ICEBERG",
              "1 KG",
              [
                "79.0",
                "199.0"
              ],
              100027281,
              "1 kg",
              79.0
            ],
            [
              "Fresh Value",
              "Red Cabbage",
              "500g",
              [
                89.5,
                89.5
              ],
              "1 kg",
              179.0,
              "NA",
              "CABBAGE RED",
              "1 KG",
              [
                "89.0",
                "149.0"
              ],
              100027005,
              "1 kg",
              89.0
            ],
            [
              "Fresh Value",
              "Chandramukhi Potato",
              "2kg",
              [
                46.0,
                46.0
              ],
              "1 kg",
              23.0,
              "NA",
              "POTATO",
              "1 KG",
              [
                "34.0",
                "52.0"
              ],
              100026745,
              "1 kg",
              34.0
            ]
          ],
          "grofers": [
            [
              "NA",
              "Baby Potato",
              "1 kg",
              [
                48.0,
                63.0
              ],
              "1 kg",
              48.0,
              "NA",
              "POTATO BABY",
              "500 G",
              [
                "29.0",
                "29.0"
              ],
              100026747,
              "1 kg",
              58.0
            ],
            [
              "NA",
              "Valencia Orange - Imported",
              "1 kg",
              [
                171.0,
                223.0
              ],
              "1 kg",
              171.0,
              "NA",
              "ORANGE NAVEL IMPORTED",
              "1 KG",
              [
                "149.0",
                "199.0"
              ],
              100026465,
              "1 kg",
              149.0
            ]
          ],
          "dmart": [
            [
              "NA",
              "Coconut",
              "1 Unit",
              [
                35.0,
                40.0
              ],
              "1 piece",
              35.0,
              "NA",
              "COCONUT",
              "1 PC",
              [
                "29.0",
                "35.0"
              ],
              100023881,
              "1 piece",
              29.0
            ],
            [
              "Fresh",
              "Cucumber",
              "500 gms",
              [
                23.0,
                40.0
              ],
              "1 kg",
              46.0,
              "NA",
              "CUCUMBER GREEN",
              "1 KG",
              [
                "16.0",
                "49.0"
              ],
              100023371,
              "1 kg",
              16.0
            ],
            [
              "NA",
              "Bottle Gourd",
              "500 gms",
              [
                23.0,
                40.0
              ],
              "1 kg",
              46.0,
              "NA",
              "BOTTLE GOURD LONG",
              "1 KG",
              [
                "19.0",
                "369.0"
              ],
              100023395,
              "1 kg",
              19.0
            ],
            [
              "Safal",
              "Green Peas",
              "1 kg",
              [
                165.0,
                190.0
              ],
              "1 kg",
              165.0,
              "NA",
              "PEAS DELHI",
              "1 KG",
              [
                "59.0",
                "199.0"
              ],
              100023887,
              "1 kg",
              59.0
            ]
          ],
          "big_basket": [
            [
              "Fresho",
              "Tender Coconut - Medium",
              "1 pc",
              [
                34.0,
                170.0
              ],
              "1 piece",
              34.0,
              "NA",
              "COCONUT TENDER",
              "1 PC",
              [
                "25.0",
                "35.0"
              ],
              100023889,
              "1 piece",
              25.0
            ],
            [
              "Fresho",
              "Banana - Robusta",
              "1 kg",
              [
                30.0,
                150.0
              ],
              "1 kg",
              30.0,
              "NA",
              "BANANA ROBUSTA",
              "1 KG",
              [
                "24.0",
                "45.9"
              ],
              100025895,
              "1 kg",
              24.0
            ],
            [
              "Fresho",
              "Mushrooms - Button",
              "200 g",
              [
                49.0,
                245.0
              ],
              "1 kg",
              245.0,
              "NA",
              "MUSHROOM BUTTON",
              "200G",
              [
                "45.0",
                "75.0"
              ],
              100024125,
              "1 kg",
              225.0
            ],
            [
              "Fresho",
              "Cucumber",
              "1 kg",
              [
                21.0,
                105.0
              ],
              "1 kg",
              21.0,
              "NA",
              "CUCUMBER GREEN",
              "1 KG",
              [
                "16.0",
                "49.0"
              ],
              100023371,
              "1 kg",
              16.0
            ],
            [
              "Fresho",
              "Garlic - Peeled",
              "200 g",
              [
                86.0,
                430.0
              ],
              "1 kg",
              430.0,
              "NA",
              "GARLIC PEELED",
              "180G",
              [
                "69.0",
                "69.0"
              ],
              100023639,
              "1 kg",
              383.33333333333337
            ],
            [
              "Fresho",
              "Lemon - Organically Grown",
              "4 pcs",
              [
                19.0,
                95.0
              ],
              "1 piece",
              4.75,
              "NA",
              "LEMON PC",
              "1 PC",
              [
                "3.0",
                "20.0"
              ],
              110215627,
              "1 piece",
              3.0
            ],
            [
              "Fresho",
              "Snake Gourd",
              "1 kg",
              [
                33.0,
                165.0
              ],
              "1 kg",
              33.0,
              "NA",
              "SNAKE GOURD SHORT",
              "1 KG",
              [
                "29.0",
                "75.9"
              ],
              100023589,
              "1 kg",
              29.0
            ],
            [
              "Fresho",
              "Tapioca",
              "1 kg",
              [
                55.0,
                275.0
              ],
              "1 kg",
              55.0,
              "NA",
              "TAPIOCA",
              "1 KG",
              [
                "39.0",
                "75.0"
              ],
              100023617,
              "1 kg",
              39.0
            ]
          ]
        },
        "grocery": {
          "spencers": [],
          "grofers": [],
          "dmart": [],
          "big_basket": []
        }
      }
    }
  }