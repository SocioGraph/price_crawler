import argparse, glob, os


def read_csvs(directory_path):
    csv_files = glob.glob(os.path.join(directory_path, "*.csv"))
    f_names = []
    if csv_files:
        for file in csv_files:
            if os.path.getsize(file) > 0:   # File size > 0
                filename_with_extension = os.path.basename(file)
                f_names.append(filename_with_extension)
    f_names.sort()
    return f_names

def craft_script_input(f_names, script_path, split_files_path, output_path, headless):
    script_call = {}
    competitors = set()
    for split_f_name in f_names:
        filename_with_extension = split_f_name
        filename = os.path.splitext(filename_with_extension)[0] # Removing extension from file name
        filename_sliced_list = filename.split('_')
        if len(filename_sliced_list) >= 6:
            website = filename_sliced_list[5]
            pincode = filename_sliced_list[6]
            out = f'python "{script_path}//{website}.py" -i "{split_files_path}//{filename_with_extension}" -o "{output_path}" -w {website} --headless {headless} -l {pincode}'
            competitors.add(website)
            if script_call.get(website) == None:
                script_call[website] = []
                script_call[website].append(out + '\n')
            else:
                script_call[website].append(out + '\n')
    return script_call

def write_to_file(script_call, output_dir, file_extension, is_split):
    if is_split:
        total_commands = 0
        print ('')
        for competitor, command in script_call.items():
            if file_extension == 'bat':
                fileame_with_dir = f'{output_dir}//run_crawlers_{competitor}.bat'
            else:
                fileame_with_dir = f'{output_dir}//run_crawlers_{competitor}.sh'
            with open(fileame_with_dir, "w") as file:
                file.writelines(command)
                if file_extension == 'bat':
                    file.write("cmd /k")
                total_commands += len(command)
                print (f'Data saved to {fileame_with_dir} - {len(command)} commands')
        print (f'Total commands - {total_commands}')
        
    else:
        if file_extension == 'bat':
            fileame_with_dir = f'{output_dir}//run_crawlers.bat'
        else:
            fileame_with_dir = f'{output_dir}//run_crawlers.sh'
        with open(fileame_with_dir, "w") as file:
            file.writelines(sum(script_call.values(), []))
            if file_extension == 'bat':
                    file.write("cmd /k")

            print (f'\nData saved to {fileame_with_dir}')
            print (f'Total commands - {len(sum(script_call.values(), []))}')

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--directory',    type=str, required=True, help="Directory path containing split CSV files")
    parser.add_argument('-s', '--script',       type=str, required=True, help="Locations of crawing scripts")
    parser.add_argument('-o', '--output',       type=str, required=True, help="Output Directory to save crawled results")
    parser.add_argument('-S', '--save',         type=str, required=True, help="Directory to save bat/sh file")
    parser.add_argument('-t', '--type',         type=str, help="Output file type (bat/sh)", default='bat')
    parser.add_argument('-H', '--headless',     help="Browser headless or not (True/False). Default is True", default=True)
    parser.add_argument('-c', '--cut',     action='store_true', help="Split/cut bat/sh file by competitor websites")
    
    args = parser.parse_args()
    
    if not args.directory:
        parser.print_help(sys.stderr)
        print (f'\nError: Please provide the directory path containing split CSV files')
        sys.exit()
    if not args.script:
        parser.print_help(sys.stderr)
        print (f'\nError: Please provide the directory path containing crawing scripts (.py files)')
        sys.exit()    
    if not args.output:
        parser.print_help(sys.stderr)
        print (f'\nError: Please provide the directory path to save results after crawling')
        sys.exit() 
    if not args.save:
        parser.print_help(sys.stderr)
        print (f'\nError: Please provide the directory path to save bat/sh file')
        sys.exit() 
    if not args.type:
        parser.print_help(sys.stderr)
        print (f'\nError: Please provide the Output file type (bat/sh)')
        sys.exit() 
    else:
        if args.type.lower() not in ['bat', 'sh']:
            print (f'\nError: POutput file type should be bat or sh')
            sys.exit()
    
    f_names = read_csvs(args.directory)
    if f_names:
        script_call = craft_script_input(f_names, args.script, args.directory, args.output, args.headless)
        write_to_file(script_call, args.save, args.type, args.cut)

    else:
        print (f'No CSV files found in {args.directory}')

if __name__ == "__main__":
    main()