# -*- coding: utf-8 -*-
import argparse, sys, csv, time, re, json, random, datetime, os
from seleniumwire import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
import codecs
import logging
logging.getLogger('seleniumwire.proxy').setLevel(logging.FATAL)

import traceback

chromedriver_path = None

def highlight(element):
    driver = element._parent
    def apply_style(s):
        driver.execute_script("arguments[0].setAttribute('style', arguments[1]);",
                              element, s)
    original_style = element.get_attribute('style')
    apply_style("background: yellow; border: 2px solid red;")
    time.sleep(.2)
    apply_style(original_style)

def set_location(driver, inputlocation): 

    DELAY = 20
    location_placeholder = "//input[@placeholder='Type your city Society/Colony/Area']"
    #location_placeholder = '//*[@id="app"]/div/div[2]/header/div[2]/div[2]/div/div/div/div/div/div[2]/div[2]/div/div[2]/div/div/div/input'
    location_selected = '//*[@id="app"]/div/div[2]/div[2]/header/div[1]/a/div/div/div[2]/div[2]'
    
    driver.get("https://blinkit.com/")    
    print("Setting location...")
    
    #placeholder = WebDriverWait(driver, DELAY).until(EC.element_to_be_clickable((By.CLASS_NAME, 'Select-placeholder')))
    placeholder = WebDriverWait(driver, DELAY).until(EC.element_to_be_clickable((By.XPATH, location_placeholder)))
    highlight(placeholder)
    action = ActionChains(driver).move_to_element(placeholder)
    action.click()
    action.send_keys(inputlocation)
    time.sleep(1)
    action.send_keys(' ').perform()
    time.sleep(5)
    
    all_locations = driver.find_elements(By.CLASS_NAME, "pac-matched")
    if not all_locations:
        all_locations = driver.find_elements(By.CLASS_NAME, "Select-option")
    if all_locations:
        first_location = all_locations[0]
        highlight(first_location)
        first_location.click()
    else:
        print ('Location NOT found')
        return None

    #time.sleep(1500)
    DELAY = 20
    try:
        try:
            #user_location = driver.find_element(By.CLASS_NAME, "eta-location-address")
            user_location = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.CLASS_NAME, "eta-location-address")))
            selected_location = user_location.text.replace('\nSorry, we are temporarily unavailable', '').split('delivery')[0].strip()
            print (f'\nSelected Location: {selected_location}\n')
            return selected_location
        except Exception as err:
            #check_servicecable = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.CLASS_NAME, "non-serviceable-step")))
            check_servicecable = driver.find_elements(By.CLASS_NAME, "non-serviceable-step")
            if check_servicecable:
                if 'We are not yet in' in check_servicecable[0].get_attribute("innerHTML"):
                    print ('\nNo Delivery in specified Location, Exiting..')
                    return None
          
    except Exception as err:
        traceback.print_exc()
        print (f'Error: {err}, Exiting..')
        return None
    
def read_csv(input_csv, path, website_name, inputlocation, headless, crawl_time):
    is_headless = ''
    is_location_set = False
    is_remove_file = False
    first_url = True
    with input_csv:
        csv_reader = csv.reader(input_csv, delimiter=',')
        with open(path+"//"+website_name+"_"+inputlocation+".csv", 'w', encoding='utf-8-sig', newline='') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(['ITEM_DESC','PRODUCT_NAME','QUANTITY','PRICE','SELLING_PRICE', 'BRAND','WEBSITE_LINK', 'Comments'])
            links_count = 0
            is_headless = 'No'
            for line_count, row in enumerate(csv_reader):
                if line_count == 0:
                    website_name_flag = False
                    item_desc_flag = False
                    url_index = 0
                    item_desc_index = 0
                    for column_index, column_name in enumerate(row):
                        if column_name.lower() == f"{website_name.lower()} url" or column_name.lower() == "grofers url":
                            url_index = column_index
                            website_name_flag = True
                        
                        elif column_name.lower() == 'item_desc':
                            item_desc_index = column_index
                            item_desc_flag = True
                        
                        if website_name_flag and item_desc_flag:
                            break
                        
                    if not website_name_flag or not item_desc_flag:
                        print (f'\nColumn Header "item_desc" or "{website_name} url" NOT found in {input_csv.name} file, Exiting..')
                        is_remove_file = True
                        break

                else:
                    if not is_location_set:
                        chrome_options = webdriver.ChromeOptions()
                        chrome_options.add_argument("--window-size=1920,1080")
                        chrome_options.add_argument("--start-maximized")
                        chrome_options.add_argument('--ignore-certificate-errors')
                        chrome_options.add_argument('--ignore-ssl-errors')
                        chrome_options.add_argument('log-level=3')
                        options_wire = {'disable_encoding': True}
                        if headless:
                            is_headless = 'Yes'
                            chrome_options.add_argument('--headless')
                            chrome_options.add_argument('--no-sandbox')
                            driver = webdriver.Chrome(executable_path = chromedriver_path, options=chrome_options, seleniumwire_options=options_wire)
                        else:
                            is_headless = 'No'
                            driver = webdriver.Chrome(executable_path = chromedriver_path, options=chrome_options, seleniumwire_options=options_wire)
                            
                        user_location = set_location(driver, inputlocation)
                        is_location_set = True
                        time.sleep(10)
                        if not user_location:
                            is_remove_file = True
                            break
                        
                    prod_desc = row[item_desc_index]
                    url = row[url_index]
                    if (url.startswith('https://grofers.com/') or url.startswith('https://blinkit.com/')) and '/prid/' in url:
                        units = re.findall(r"[-+]?\d*\.\d+|\d+", prod_desc)
                        units.sort()
                        product_name, orig_quan, mrp, selling_price, brand, comments = read_blinkit(prod_desc, url, units, driver, inputlocation, crawl_time)
                        csvwriter.writerow([prod_desc, product_name, orig_quan, mrp, selling_price, brand, url, comments])
                        links_count += 1
                        
                        sleep_time = random.uniform(1, 3)
                        print (f'\t      Sleeping {"{:.2f}".format(sleep_time)} sec')
                        time.sleep(sleep_time)
                    else:
                        csvwriter.writerow([prod_desc, '', '', '', '', '', url, ''])

                csvfile.flush()

            if links_count > 0:
                minutes , seconds = divmod((time.time() - start_time), 60)
                print (f'\nInput      : {input_csv.name}')
                print   (f'Output     : {path}\{website_name}_{inputlocation}.csv')
                print   (f'Website    : Blinkit')
                print   (f'Location   : {user_location}')
                print   (f'Links      : {links_count}')
                print   (f'Headless   : {is_headless}')
                if int(minutes) > 0:
                    print   (f'Time Taken : {int(minutes)}.{int(seconds)} minutes')
                else:
                    print   (f'Time Taken : {int(seconds)} seconds')

    if is_remove_file:
        if os.path.isfile(path+"//"+website_name+"_"+inputlocation+".csv"):
            os.remove(path+"//"+website_name+"_"+inputlocation+".csv")
        sys.exit()

    if is_location_set:
        driver.quit()

def read_blinkit(prod_desc, url, units, driver, inputlocation, crawl_time):
    product_name = orig_quan = mrp = selling_price = brand = comments = ''  
    product_id = int(re.search(r'\d+', url.split('/prid/')[1]).group())
    print (prod_desc)
    url = url.split('?', 1)[0]
    print ('\t' + url)
    
    #screenshots_dir = f"./screenshots/{inputlocation}/{crawl_time}"
    #if not os.path.isdir(screenshots_dir):
    #    os.makedirs(screenshots_dir, exist_ok=True)

    i = 0
    while i < 3:
        json_response = visit_url(driver, url, product_id)
        if json_response is not None:
            break
        else:
            print ('Retrying', json_response)
            time.sleep(10)
            i += 1
    
    if json_response is not None:
        if 'product_details' in json_response:
            if 'success' in json_response['product_details']:
                if json_response['product_details']['success'] == False:
                    comments = 'Product not found'
        else:
            if 'data' in json_response:
                if 'product' in json_response['data']:

                    if 'name' in json_response['data']['product']:
                        product_name = json_response['data']['product']['name']
                        
                    if 'unit' in json_response['data']['product']:
                        orig_quan = json_response['data']['product']['unit']
                        orig_quan_num = re.findall(r"[-+]?\d*\.\d+|\d+", orig_quan)
                        orig_quan_num.sort()
                    
                    check_variants_present = False
                    search_unit_flag = False
                    if 'variants_info' in json_response['data']:
                        if json_response['data']['variants_info']:
                            check_variants_present = True
                        else:
                            search_unit_flag = True
                    
                    is_same = False
                    if check_variants_present:
                        if units == orig_quan_num:
                            search_unit_flag = True
                            is_same = True
                    
                    if units and check_variants_present and not is_same:
                        changing_unit = False
                        if not any(item in orig_quan_num for item in units):
                            for variants in json_response['data']['variants_info']:
                                if 'unit' in variants:
                                    cc = re.findall(r"[-+]?\d*\.\d+|\d+", variants['unit'])
                                    if any(item in cc for item in units):
                                        orig_quan = variants['unit']
                                        if 'mrp' in variants:
                                            mrp = variants['mrp']
                                        if 'price' in variants:
                                            selling_price = variants['price']
                                        changing_unit = True
                                        search_unit_flag = False
                                        break
                        if not changing_unit:
                            search_unit_flag = True

                    if not units:
                        search_unit_flag = True
                    
                    if search_unit_flag:
                        if 'mrp' in json_response['data']['product']:
                            mrp = json_response['data']['product']['mrp']
                        if 'price' in json_response['data']['product']:
                            selling_price = json_response['data']['product']['price']

                    if 'brand' in json_response['data']['product']:
                        brand = json_response['data']['product']['brand']
                    comments = ''
        
        if comments == '':
            if brand:
                print (f'\t   {product_name} | {orig_quan} | {mrp} | {selling_price} | {brand}')
            else:
                print (f'\t   {product_name} | {orig_quan} | {mrp} | {selling_price}')
        else:
            print (f'\t   {comments}')
    
    else:
        print (f'\t   Json Response is None!')

    return product_name, orig_quan, mrp, selling_price, brand, comments

def take_screenshot(driver, save_as):
    print(f"Taking Screenshot.. -> {save_as}")
    driver.get_screenshot_as_file(save_as)

def visit_url(driver, url, product_id):
    del driver.requests
    driver.get(url)

    #print("Taking Screenshot..")
    #take_screenshot(driver, f"{screenshots_dir}/grofers_{product_id}.png")
    
    product_id = str(product_id)
    url_regex = '^https:\/\/blinkit.com\/v6\/merchant\/.*('+product_id+'\/|current_screen=pdp)$'
    try:
        request = driver.wait_for_request(url_regex, timeout=20)
        url_response = json.loads(request.response.body)
        if 'You are being rate limited' in url_response:
            print ('Too many simultaneous requests, sleeping for 5 minutes.')
            time.sleep(300)
            return None
        else:
            return url_response
    except Exception as err:
        traceback.print_exc()
        print (f'Error: {err}')
        return None
    
def main():
    crawl_time = datetime.datetime.now().strftime('%d-%m-%y-%H:%M:%S')
    global chromedriver_path
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', type=argparse.FileType("r"), help="Input CSV file")
    parser.add_argument('-o', '--output', type=str, help="Output Directory")
    parser.add_argument('-w', '--website', type=str, help="CSV Column Name")
    parser.add_argument('-l', '--location', type=str, help="Blinkit Location")
    parser.add_argument('-H', '--headless', help="Browser headless or not (True/False). Default is True", default=True)

    if len(sys.argv)==1:
        parser.print_help(sys.stderr)
        sys.exit()
    
    args = parser.parse_args()

    if sys.platform == 'win32':
        chromedriver_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "chromedriver.exe")
        if not os.path.exists(os.path.join(os.path.dirname(os.path.abspath(__file__)), "chromedriver.exe")):
            print (f'\nChrome Driver Not Found, Exiting..')
            sys.exit()
    elif 'linux' in sys.platform:
        chromedriver_path = "/usr/bin/chromedriver"
        if not os.path.exists("/usr/bin/chromedriver"):
            print (f'\nChrome Driver Not Found, Exiting..')
            sys.exit()
    else:
        print(f'Unknown OS, Exitting..')
        sys.exit()

    headless = False if args.headless == "False" else True
    
    if not args.input:
        parser.print_help(sys.stderr)
        print (f'\nError: Provide Input CSV file')
        sys.exit()
    if not args.output:
        parser.print_help(sys.stderr)
        print (f'\nError: Provide output Directory')
        sys.exit()
    if not args.website:
        parser.print_help(sys.stderr)
        print (f'\nError: Provide Blinkit column name from CSV')
        sys.exit()
    if not args.location:
        parser.print_help(sys.stderr)
        print (f'\nError: Provide Blinkit Location')
        sys.exit()
        
    read_csv(args.input, args.output, args.website, args.location, headless, crawl_time)

if __name__ == "__main__":
    start_time = time.time()
    main()
