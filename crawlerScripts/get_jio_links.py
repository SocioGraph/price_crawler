import csv, time
import pandas as pd
import argparse, sys, csv
from PIL import Image
import openpyxl
from openpyxl.drawing.image import Image
from jiomart_otr import *

set_location('110018')

input_csv_file = sys.argv[1]
modified_rows = []
fieldnames = None
try:
    with open(input_csv_file, mode='r') as input_file:
        csv_reader = csv.DictReader(input_file)

        fieldnames = csv_reader.fieldnames
        if 'Screenshot' not in fieldnames:
            fieldnames.append('Screenshot')
        if 'Remarks' not in fieldnames:
            fieldnames.append('Remarks')

        modified_rows = []
        
        for num, row in enumerate(csv_reader, 1): 
            product_title = row.get("SPAR Product Title", None).split("*")[0].replace("&", "%26").replace("%", "~pct~")
            print (f"{num}. {product_title}")
            if product_title:
                results = {}
                try:
                    results = search_products(product_title)
                except:
                    print (f"\tUnable to get data")
                url = results.get('url', None)
                title = results.get('title', None)
                screenshot = results.get('screenshot', None)
                row['Jio Link'] = url
                row['Screenshot'] = screenshot
                if url:
                    if url.startswith('https'):
                        if product_title.split()[0].lower() not in title.lower():
                            row['Remarks'] = 'Product might be different'
                modified_rows.append(row)
                #if num>10:  break

except KeyboardInterrupt:
    sys.exit()
    print("KeyboardInterrupt: Ctrl+C was pressed")       
except Exception as e:
    print(f"An exception occurred: {type(e).__name__} - {e}")    

output_excel_file = "JioMart_output.xlsx"
df = pd.DataFrame(modified_rows, columns=fieldnames)

with pd.ExcelWriter(output_excel_file, engine='openpyxl') as writer:
    df.to_excel(writer, sheet_name='JioMart products', index=False)

    workbook = writer.book
    worksheet = writer.sheets['JioMart products']

    num_rows = df.shape[0]

    for i in range(num_rows):
        image_path = df.at[i, 'Screenshot']
        if image_path:
            img = Image(image_path)
            img.width = 100  # Adjust the image width as needed
            img.height = 100  # Adjust the image height as needed

            cell = worksheet.cell(row=i + 2, column=fieldnames.index('Screenshot') + 1)
            worksheet.add_image(img, cell.coordinate)
            
            cell.value = ""

    workbook.save(output_excel_file)

