import requests
import json
import re
import pandas as pd

headers = {
  'authority': 'www.bigbasket.com',
  'accept': '*/*',
  'accept-language': 'en-US,en;q=0.9',
  'content-type': 'application/json',
  'cookie': '_client_version=2558; _bb_hid=895; _bb_cid=18; x-entry-context-id=100; x-entry-context=bb-b2c; x-channel=web; _bb_loid=j:null; _bb_bhid=; _bb_nhid=1723; _bb_vid=NTQ4OTY1MzQyMA==; _bb_dsevid=; _bb_dsid=; _bb_aid=MzA4NTgxODk5Nw==; csrftoken=Xp5dolxulh27U7znoVGTFgeqaJqz3ILUKtWB5gpAJSBxR518UIa9qTDdOf6QuEqX; _bb_home_cache=70ecc5a0.1.visitor; _bb_bb2.0=1; is_global=1; _bb_addressinfo=; _bb_pin_code=; _bb_sa_ids=10654; _is_bb1.0_supported=0; bb2_enabled=true; csurftoken=c72CkA.NTQ4OTY1MzQyMA==.1700207668358.avU5wb9Qmc6HgmoPbpbdj27AELgC4xQuBeyrptpv7ec=; _gcl_au=1.1.1420669312.1700207669; jarvis-id=9468047b-baa7-4426-b8cf-9bfd5894c6f2; bigbasket.com=765cecc3-2741-4359-8c43-0dbcf0341fb0; adb=0; _gid=GA1.2.141446687.1700207670; _fbp=fb.1.1700207669702.1277866774; ufi=1; ts=2023-11-17%2013:27:08.322; _ga=GA1.2.1167820943.1700207670; _gat_UA-27455376-1=1; _ga_FRRYG5VKHX=GS1.1.1700207669.1.1.1700207871.13.0.0; _bb_cid=1; csurftoken=c72CkA.NTQ4OTY1MzQyMA==.1700207668358.avU5wb9Qmc6HgmoPbpbdj27AELgC4xQuBeyrptpv7ec=',
  'newrelic': 'eyJ2IjpbMCwxXSwiZCI6eyJ0eSI6IkJyb3dzZXIiLCJhYyI6IjgzNzMwNCIsImFwIjoiMTgzNDk4NzAwMiIsImlkIjoiOTdjNDMzNmNjNDY5Njc3MSIsInRyIjoiNzY0NzZkMTIwMDFhNDQ0YjRmMmM5NzVmNzI4MGUxMDAiLCJ0aSI6MTcwMDIwNzg3MTcyMH19',
  'referer': 'https://www.bigbasket.com/cl/eggs-meat-fish/?page=2',
  'sec-ch-ua': '"Google Chrome";v="119", "Chromium";v="119", "Not?A_Brand";v="24"',
  'sec-ch-ua-mobile': '?0',
  'sec-ch-ua-platform': '"Windows"',
  'sec-fetch-dest': 'empty',
  'sec-fetch-mode': 'cors',
  'sec-fetch-site': 'same-origin',
  'traceparent': '00-76476d12001a444b4f2c975f7280e100-97c4336cc4696771-01',
  'tracestate': '837304@nr=0-1-837304-1834987002-97c4336cc4696771----1700207871720',
  'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36',
  'x-channel': 'BB-WEB',
  'x-tracker': '275efe50-5d37-4372-b441-4e48a0a3d5f7'
}

category_url = "https://www.bigbasket.com/cl/eggs-meat-fish/?nc=nb"
match = re.search(r'/([^/]+)/?(\?nc=nb)?$', category_url)
if match:
    category = match.group(1)
    print (f"Category: {category}")
    search_url = f"https://www.bigbasket.com/listing-svc/v2/products?type=pc&slug={category}"
    product_list = []
    page_num = 1
    while True:
        print (f"Scanning page {page_num}", end = '\r')
        response = requests.request("GET", f"{search_url}&page={page_num}", headers=headers)
        if response.status_code == 200:
            #print (response.text)
            response = response.json()
            
            if response.get('tabs'):
                tabs =  response.get('tabs')[0]
                if tabs.get('product_info'):
                    product_info = tabs.get('product_info')
                    if product_info.get('products'):
                        for each_product in product_info.get('products'):
                            absolute_url = each_product.get('absolute_url')
                            url = f"https://www.bigbasket.com{absolute_url}"
                            title = each_product.get('desc')
                            product = {
                                        'title': title,
                                        'url': url
                                        }
                            if product not in product_list:
                                product_list.append(product)
                            else:
                                print (product)
                        
                        total_count = product_info.get('total_count')
                        #print (total_count)
                        #break
                        num_of_pages = product_info.get('number_of_pages')
                        page = product_info.get('page')
                        if page == num_of_pages:
                            break
                        
            page_num += 1
        
        else:
            print ("Status Code is {response.status_code}")
            break

    print (f"\n\nFound {len(product_list)} products")
    #print(response.text)

    output_file = f"bigbasket_products_{category}.csv"

    if product_list:
        fieldnames = ["title","url"]
        df = pd.DataFrame(product_list, columns=fieldnames)
        output_csv_file = output_file
        df.to_csv(output_csv_file, index=False)
        print(f"\nSaved to {output_csv_file}")
else:
    print("BigBasket Category not found in the URL")





