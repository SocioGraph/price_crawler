import sys, time, csv, re, json, requests
import pandas as pd
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains

from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

DELAY = 20

chrome_options = webdriver.ChromeOptions()
#chrome_options.add_argument("--window-size=1920,1080")
#chrome_options.add_argument('--headless')
chrome_options.add_argument("--start-maximized")
chrome_options.add_argument('--ignore-certificate-errors')
chrome_options.add_argument('--ignore-ssl-errors')
chrome_options.add_argument('log-level=3')

#driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)

headers = {
  'authority': 'www.bigbasket.com',
  'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
  'accept-language': 'en-US,en;q=0.9',
  'cache-control': 'max-age=0',
  'cookie': '_client_version=2558; _bb_hid=895; _bb_cid=18; x-entry-context-id=100; x-entry-context=bb-b2c; x-channel=web; _bb_loid=j:null; _bb_bhid=; _bb_nhid=1723; _bb_vid=NTQ4OTY1MzQyMA==; _bb_dsevid=; _bb_dsid=; csrftoken=Xp5dolxulh27U7znoVGTFgeqaJqz3ILUKtWB5gpAJSBxR518UIa9qTDdOf6QuEqX; _bb_home_cache=70ecc5a0.1.visitor; _bb_bb2.0=1; bb2_enabled=true; _gcl_au=1.1.1420669312.1700207669; jarvis-id=9468047b-baa7-4426-b8cf-9bfd5894c6f2; bigbasket.com=765cecc3-2741-4359-8c43-0dbcf0341fb0; adb=0; _fbp=fb.1.1700207669702.1277866774; ufi=1; _bb_lat_long="MjguNjQzNjA4Mnw3Ny4wODY5ODM2OTk5OTk5OQ=="; _bb_aid="Mjk5MTE2MjgzNg=="; is_global=0; _bb_addressinfo=MjguNjQzNjA4Mnw3Ny4wODY5ODM2OTk5OTk5OXxUaWxhayBOYWdhcnwxMTAwMTh8RGVsaGl8MXxmYWxzZXx0cnVlfHRydWV8QmlnYmFza2V0ZWVy; _bb_pin_code=110018; _bb_sa_ids=10260; _is_bb1.0_supported=1; _ga_FRRYG5VKHX=deleted; csurftoken=iuYJdw.NTQ4OTY1MzQyMA==.1700968837264.n+/JtFLQD3UNsJCm2BTmQ8pemFMBNki3f7YuRMsg92Y=; ts=2023-11-26%2008:50:43.904; _ga_FRRYG5VKHX=GS1.1.1700968846.11.0.1700968846.60.0.0; _ga=GA1.2.1167820943.1700207670; _gid=GA1.2.1710394213.1700968849; _gat_UA-27455376-1=1; _bb_addressinfo=MjguNjQzNjA4Mnw3Ny4wODY5ODM2OTk5OTk5OXxUaWxhayBOYWdhcnwxMTAwMTh8RGVsaGl8MXxmYWxzZXx0cnVlfHRydWV8QmlnYmFza2V0ZWVy; _bb_bb2.0=1; _bb_cid=1; _bb_pin_code=110018; _bb_sa_ids=10260; _is_bb1.0_supported=1; bb2_enabled=true; csurftoken=iuYJdw.NTQ4OTY1MzQyMA==.1700968837264.n+/JtFLQD3UNsJCm2BTmQ8pemFMBNki3f7YuRMsg92Y=; is_global=0; x-channel=web',
  'sec-ch-ua': '"Google Chrome";v="119", "Chromium";v="119", "Not?A_Brand";v="24"',
  'sec-ch-ua-mobile': '?0',
  'sec-ch-ua-platform': '"Windows"',
  'sec-fetch-dest': 'document',
  'sec-fetch-mode': 'navigate',
  'sec-fetch-site': 'none',
  'sec-fetch-user': '?1',
  'upgrade-insecure-requests': '1',
  'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36'
}


def highlight(element):
    driver = element._parent
    def apply_style(s):
        driver.execute_script("arguments[0].setAttribute('style', arguments[1]);",
                              element, s)
    original_style = element.get_attribute('style')
    apply_style("background: yellow; border: 2px solid red;")
    time.sleep(.01)
    apply_style(original_style)

def take_screenshot(save_as):
    #print(f"Taking Screenshot.. -> {save_as}")
    driver.get_screenshot_as_file(save_as)

def save_output(crawled_data, fieldnames, output_file):
    df = pd.DataFrame(crawled_data, columns=fieldnames)
    output_excel_file = output_file

    with pd.ExcelWriter(output_excel_file, engine='openpyxl') as writer:
        df.to_excel(writer, sheet_name=output_file, index=False)

        workbook = writer.book
        worksheet = writer.sheets[output_file]
        workbook.save(output_excel_file)
        print (f"\nSaved to {output_excel_file}")
   
def read_bigbasket(url):
    '''
    driver.get(url)
    page_source = driver.page_source
    json_xpath = '//*[@id="__NEXT_DATA__"]'
    jsonSource = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, json_xpath)))
    pattern = re.compile(r'<script id="__NEXT_DATA__" type="application/json">(.*?)</script>')
    match = pattern.search(jsonSource.get_attribute("outerHTML"))
    '''
    response = requests.request("GET", url, headers=headers)
    soup = BeautifulSoup(response.text, 'html.parser')
    script_tag = soup.find('script', {'id': '__NEXT_DATA__'})
    script_content = script_tag.contents[0]
    data = json.loads(script_content)

    if data:
        #json_data = match.group(1)
        #data = json.loads(json_data)
    
        image_urls = []
        if data.get('props'):
            props = data.get('props')
            if props.get('pageProps'):
                pageProps = props.get('pageProps')
                if pageProps.get('productDetails'):
                    productDetails = pageProps.get('productDetails')
                    if productDetails.get('children'):
                        children = productDetails.get('children')
                        for each_child in children:
                            if each_child.get('images'):
                                images = each_child.get('images')
                                for each_image in images:
                                    if each_image.get('l'):
                                        img_url = each_image.get('l')
                                        image_urls.append(img_url)

        return {
        'BigBasket Image URLs': image_urls
        }
    else:
        print (f"\tNo data found!")
        return None

def read_input_csv(input_csv_file):
    with open(input_csv_file, mode='r') as input_file:
        csv_reader = csv.DictReader(input_file)

        fieldnames = csv_reader.fieldnames
        
        url_list = ['pic' + str(i) for i in range(1, 21)]
        fieldnames.extend(url_list)
        
        modified_rows = []

        for num, row in enumerate(csv_reader):
                bigbasket_url = row.get("url")
                if bigbasket_url:
                    if bigbasket_url.startswith('bigbasket.com'):
                        bigbasket_url = 'https://www.' + bigbasket_url
                    if bigbasket_url.startswith('https://www.bigbasket.com/pd/'):
                        #bigbasket_url = 'https://www.bigbasket.com/pd/40204537/meisterwurst-pork-peppery-meatball-300-g/'     # For testing purposes
                        print (f"{num+1}. {bigbasket_url}")
                        result = read_bigbasket(bigbasket_url)
                        #time.sleep(300)     # For testing purposes
                        if result:
                            for key in row:
                                if key in [f'pic{i}' for i in range(1, 21)]:
                                    row[key] = result['BigBasket Image URLs'].pop(0) if result['BigBasket Image URLs'] else None
                            
                            #if num > 1:  break     # For testing purposes
                        modified_rows.append(row)
    
    return modified_rows, fieldnames

def main():
    input_csv_file = sys.argv[1]
    output_file = f"{input_csv_file.rsplit('/')[-1].split('.')[0]}_output.xlsx"
    crawled_data, fieldnames = read_input_csv(input_csv_file)
    save_output(crawled_data, fieldnames, output_file)

if __name__ == "__main__":
    main()                       