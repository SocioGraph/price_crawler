import argparse, sys, csv, time, os, json
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

from selenium.common.exceptions import NoSuchElementException

import traceback

def highlight(element):
    try:
        driver = element._parent
        def apply_style(s):
            driver.execute_script("arguments[0].setAttribute('style', arguments[1]);",
                                  element, s)
        original_style = element.get_attribute('style')
        apply_style("background: yellow; border: 2px solid red;")
        time.sleep(.1)
        apply_style(original_style)
    except Exception as err:
        pass

def set_location(driver, input_location): 
    DELAY = 20
    
    while True:
        try:
            driver.get('https://www.starquik.com/')
            break
        except Exception as err:
            pass
            #print (err)
    print("Setting location...")
    
    proceed_flag = False
    location_suggestions = check_servicable_data = selected_location = ''
    
    select_location_xpath           = '//*[@id="bg-Body-search"]/div[4]/div[2]/div[2]/div/div/div[1]/button/span'
    input_location_textbox_xpath    = '//*[@id="pac-input"]'
    selected_location_xpath         = '//*[@id="navbarSupportedContentHead"]/div/div/div[1]/button'
    not_servicable_xpath            = '//*[@id="navbarSupportedContentHead"]/div/div/div[1]/div[2]/app-location-drop-down/div[1]/div[1]/div[3]/p'
        
    try:       
        newline = '\n'
        count = 0
        retry_flag = sarath_city_mall_flag = False
        while True:
            
            if count == 0 or retry_flag:
                select_location = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, select_location_xpath)))
                highlight(select_location)
                select_location.click()
                
                input_location_textbox = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, input_location_textbox_xpath)))
        
            highlight(input_location_textbox)
            input_location_textbox.clear()
            
            for each_char in input_location:
                input_location_textbox.send_keys(each_char)
                time.sleep(.01)
            input_location_textbox.send_keys(' ')
            time.sleep(2)
            
            location_suggestions = driver.find_elements(By.CLASS_NAME, "location-suggestions-item")
            highlight(location_suggestions)
            if location_suggestions:
                print (f"Trying the Location: {location_suggestions[count].text.split(newline)[0]}")
                highlight(location_suggestions[count])
                location_suggestions[count].click()
                count += 1
                time.sleep(1)
                check_servicable = driver.find_element(By.CLASS_NAME, "service-not-applicable")
                if not "We are currently operational in & around" in check_servicable.text:
                    element = driver.find_element(By.XPATH, select_location_xpath)
                    WebDriverWait(driver, 30).until(EC.staleness_of(element))
                    select_location = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, select_location_xpath)))
                    highlight(select_location)
                    if not select_location.text.startswith('undefined'):
                        print ((f'\nSelected Location: {select_location.text}\n'))
                        return select_location.text
                    else:
                        print (f"\tNot servicable")
                        retry_flag = True
                else:
                    print (f"\tNot servicable")
                if count+1 > len(location_suggestions):
                    if input_location == '500084' and not sarath_city_mall_flag: # Since pincode 500084 does not work
                        input_location = 'Sarath City Capital Mall'
                        sarath_city_mall_flag = True
                        count = 0
                    else:
                        print (f'\nSelected Locations NOT found or NOT servicable, Exiting..')
                        break

    except Exception as err:
        print (f'Error: {err}\nExiting..')
        traceback.print_exc()
        driver.quit()
        sys.exit()

    driver.quit()
    sys.exit()

def read_csv(input_csv, path, website_name, location_name, headless, filename):
    website_name = website_name.lower().replace('url', '').strip()
    line_count  = 0
    links_count = 0
    is_headless = ''
    is_location_set = False
    is_remove_file = False
    fileame_with_dir = path+"//"+filename+"_out.csv"
    with input_csv:
        csv_reader = csv.reader(input_csv, delimiter=',')
        with open(fileame_with_dir, 'w', newline='') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(['ITEM_DESC','PRODUCT_NAME','QUANTITY','PRICE','SELLING_PRICE', 'BRAND','WEBSITE_LINK', 'Comments', 'REGION', 'Category', 'Sub_Category', 'ITEM CODE', 'Standard_UOM', 'UOM', 'SKU TYPE', 'Type', 'Normalization', 'Store Name', 'Store Code', 'Pincode', 'Competitor', 'Spar Price', 'Margin'])
            for row in csv_reader:
                if line_count == 0:
                    website_name_flag   = False
                    item_desc_flag      = False
                    previous_url        = ''
                    
                    column_name         = [_.lower() for _ in row]
                    
                    region_index        = column_name.index("region")
                    category_index      = column_name.index("category")
                    sub_category_index  = column_name.index("sub_category")
                    item_code_index     = column_name.index("item code")
                    std_uom_index       = column_name.index("standard_uom")
                    sku_type_index      = column_name.index("sku type")
                    url_norm_index      = column_name.index(f"{website_name.lower()} needs normalization")
                    store_name_index    = column_name.index("store name")
                    store_code_index    = column_name.index("store code")
                    pincode_index       = column_name.index("pincode")
                    competitor_index    = column_name.index("competitor")
                    type_index          = column_name.index("type")
                    spar_price_index    = column_name.index("spar price")
                    margin_index        = column_name.index("margin")
                    
                    url_index           = column_name.index(f"{website_name.lower()} url")
                    item_desc_index     = column_name.index("item_desc")
                    uom_index           = column_name.index("uom")
                    
                else:
                    if not is_location_set:
                        chrome_options = webdriver.ChromeOptions()
                        chrome_options.add_argument("--window-size=1920,1080")
                        chrome_options.add_argument("--start-maximized")
                        chrome_options.add_argument('--ignore-certificate-errors')
                        chrome_options.add_argument('--ignore-ssl-errors')
                        chrome_options.add_argument('log-level=3')
                        chrome_options.add_argument('--blink-settings=imagesEnabled=false') # Disbale loading images
                        
                        if headless:
                            is_headless = 'Yes'
                            chrome_options.add_argument('--headless')
                            chrome_options.add_argument('--no-sandbox')
                            #driver = webdriver.Chrome(executable_path = chromedriver_path, options=chrome_options)
                            driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)
                        else:
                            is_headless = 'No'
                            #driver = webdriver.Chrome(executable_path = chromedriver_path, options=chrome_options)
                            driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)
                
                        user_location = set_location(driver, location_name)
                        is_location_set = True

                    prod_desc = row[item_desc_index]
                    url = row[url_index]
                    if url.startswith('https://www.starquik.com/product'):# and line_count >28:
                        product_name, orig_quan, mrp, selling_price, brand, comments = read_starquik(prod_desc, url, driver, previous_url)
                        csvwriter.writerow([prod_desc, product_name, orig_quan, mrp, selling_price, brand, url, comments, row[region_index], row[category_index], row[sub_category_index], row[item_code_index], row[std_uom_index], row[uom_index], row[sku_type_index], row[type_index], row[url_norm_index], row[store_name_index], row[store_code_index], row[pincode_index], row[competitor_index], row[spar_price_index], row[margin_index]])
                        links_count += 1
                    else:
                        csvwriter.writerow([prod_desc, '', '', '', '', '', url, '', row[region_index], row[category_index], row[sub_category_index], row[item_code_index], row[std_uom_index], row[uom_index], row[sku_type_index], row[type_index], row[url_norm_index], row[store_name_index], row[store_code_index], row[pincode_index], row[competitor_index], row[spar_price_index], row[margin_index]])
                        
                line_count += 1
                csvfile.flush()
                
            if links_count > 0:
                minutes , seconds = divmod((time.time() - start_time), 60)
                print (f'\nInput      : {input_csv.name}')
                print   (f'Output     : {fileame_with_dir}')
                print   (f'Website    : Star Quik')
                print   (f'Location   : {user_location}')
                print   (f'Links      : {links_count}')
                print   (f'Headless   : {is_headless}')
                if int(minutes) > 0:
                    print   (f'Time Taken : {int(minutes)}.{int(seconds)} minutes')
                else:
                    print   (f'Time Taken : {int(seconds)} seconds')
            else:
                is_remove_file = True
                print (f'\nNo StarQuik URLs found in the input CSV file')
    
    if is_remove_file:
        if os.path.isfile(fileame_with_dir):
            os.remove(fileame_with_dir)
        sys.exit()

    if is_location_set:
        driver.quit()
        
def read_starquik(prod_desc, url, driver, previous_url):
    print (f'{prod_desc}')
    print (f'\t{url}')   
    product_name = orig_quan = mrp = selling_price = brand = comments = ''
    try:
        driver.get(url)

        full_prod_html_data = driver.page_source
        prod_html_data = full_prod_html_data.split("var productDetails =")[1].split("};")[0] + "}" 
        prod_json_data = json.loads(prod_html_data)
        product_name = prod_json_data.get('Name')
        brand = prod_json_data.get('p_brand')
        mrp = prod_json_data.get('Price')
        selling_price = prod_json_data.get('sale_price')
        orig_quan = prod_json_data.get('p_pack')
            
    except Exception as err:
        final_url = driver.current_url
        if final_url == 'https://www.starquik.com/404':
            comments = 'Page Not Found'
        elif final_url == 'https://www.starquik.com/':
            comments = 'Page Not Found'
        else:
            print (f'Error: {err}')
            traceback.print_exc()

    if comments:
        print (f'\t\t{comments}\n')
        product_name = orig_quan = mrp = selling_price = brand = ''
    else:
        print (f'\t\t{product_name} | {brand} | {selling_price} | {mrp} | {orig_quan}\n')

    return product_name, orig_quan, mrp, selling_price, brand, comments

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', type=argparse.FileType("r"), help="Input CSV file")
    parser.add_argument('-o', '--output', type=str, help="Output Directory")
    parser.add_argument('-w', '--website', type=str, help="CSV Column Name")
    parser.add_argument('-l', '--location', type=str, help="StarQuik Location")
    parser.add_argument('-H', '--headless', help="Run Chrome headless or not. Default is True", default=True)
    args = parser.parse_args()

    if len(sys.argv)==1:
        parser.print_help(sys.stderr)
        sys.exit()

    if not args.input:
        parser.print_help(sys.stderr)
        print (f'\nError: Provide Input CSV file')
        sys.exit()
    if not args.output:
        parser.print_help(sys.stderr)
        print (f'\nError: Provide output Directory')
        sys.exit()
    if not args.website:
        parser.print_help(sys.stderr)
        print (f'\nError: Provide StarQuik column name from CSV')
        sys.exit()
    if not args.location:
        parser.print_help(sys.stderr)
        print (f'\nError: Provide StarQuik Location')
        sys.exit()
    headless = False if args.headless == "False" else True
    
    filename_with_extension = os.path.basename(args.input.name)
    filename = os.path.splitext(filename_with_extension)[0]
        
    read_csv(args.input, args.output, args.website, args.location.lower().strip(), headless, filename)

if __name__ == "__main__":
    start_time = time.time()
    main()