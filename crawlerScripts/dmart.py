# -*- coding: utf-8 -*-
import argparse, sys, csv, time, re, os, json, math
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

import traceback

DELAY = 20

def highlight(element):
    driver = element._parent
    def apply_style(s):
        driver.execute_script("arguments[0].setAttribute('style', arguments[1]);",
                              element, s)
    original_style = element.get_attribute('style')
    apply_style("background: yellow; border: 2px solid red;")
    time.sleep(.1)
    apply_style(original_style)
    
def set_location(driver, inputlocation): 
    location_textbox_xpath          = '//*[@id="pincodeInput"]'
    first_suggestion_class_name     = 'pincode-widget_pincode-item__qsZwZ'
    location_select_button_xpath    = '/html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div[2]/div/div[2]'
    selected_location_name_xpath    = '//*[@id="root"]/div[1]/header/div/div[1]/div[1]/div/div[2]/a/span/span/span[1]'
    not_delivering_location_xpath   = '/html/body/div[2]/div[3]/div/div[2]/div/div/div[3]/p'
    
    try:
        driver.get('https://www.dmart.in/')
        print("Setting location...")
        
        location_textbox = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, location_textbox_xpath)))
        highlight(location_textbox)
        location_textbox.clear()
        location_textbox.send_keys(inputlocation)
        
        time.sleep(2)
        
        try:
            select_first_suggestion = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.CLASS_NAME, first_suggestion_class_name)))
            highlight(select_first_suggestion)
            select_first_suggestion.click()
            
            time.sleep(2)
            
            location_select = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, location_select_button_xpath)))
            highlight(location_select)
            location_select.click()
            
            time.sleep(2)
            return inputlocation
            location_name = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, selected_location_name_xpath)))
            print (f'\nSelected Location: {location_name.text}\n')
            return location_name.text
        except Exception as err:
            try:
                check_for_delivery = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, not_delivering_location_xpath)))
                print (f'\nNot operating at the selected location.\n  Message: {check_for_delivery.text}\n\tExiting..')
                return None
            except Exception as err:
                traceback.print_exc()
                print (f'Error: {err}, Exiting..')
                return None

    except Exception as err:
        traceback.print_exc()
        print (f'Error: {err}, Exiting..')
        return None

def read_csv(input_csv, path, website_name, inputlocation, headless, filename):
    is_headless     = ''
    is_location_set = False
    is_remove_file  = False
    fileame_with_dir = path+"//"+filename+"_out.csv"
    with input_csv:
        csv_reader = csv.reader(input_csv, delimiter=',')
        with open(fileame_with_dir, 'w', encoding='utf-8-sig', newline='') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(['ITEM_DESC','PRODUCT_NAME','QUANTITY','PRICE','SELLING_PRICE', 'BRAND','WEBSITE_LINK', 'Comments', 'REGION', 'Category', 'Sub_Category', 'ITEM CODE', 'Standard_UOM', 'UOM', 'SKU TYPE', 'Type', 'Normalization', 'Store Name', 'Store Code', 'Pincode', 'Competitor', 'Spar Price', 'Margin'])
            links_count = 0
            for line_count, row in enumerate(csv_reader):
                if line_count == 0:
                    website_name_flag   = False
                    item_desc_flag      = False
                    quantity_flag       = False
                    
                    column_name         = [_.lower() for _ in row]
                    
                    region_index        = column_name.index("region")
                    category_index      = column_name.index("category")
                    sub_category_index  = column_name.index("sub_category")
                    item_code_index     = column_name.index("item code")
                    std_uom_index       = column_name.index("standard_uom")
                    sku_type_index      = column_name.index("sku type")
                    url_norm_index      = column_name.index(f"{website_name.lower()} needs normalization")
                    store_name_index    = column_name.index("store name")
                    store_code_index    = column_name.index("store code")
                    pincode_index       = column_name.index("pincode")
                    competitor_index    = column_name.index("competitor")
                    type_index          = column_name.index("type")
                    spar_price_index    = column_name.index("spar price")
                    margin_index        = column_name.index("margin")
                    
                    url_index           = column_name.index(f"{website_name.lower()} url")
                    item_desc_index     = column_name.index("item_desc")
                    uom_index           = column_name.index("uom")
                    
                    
                else:
                    if not is_location_set:
                        chrome_options = webdriver.ChromeOptions()
                        chrome_options.add_argument("--window-size=1920,1080")
                        chrome_options.add_argument("--start-maximized")
                        chrome_options.add_argument('--ignore-certificate-errors')
                        chrome_options.add_argument('--ignore-ssl-errors')
                        chrome_options.add_argument('log-level=3')
                        options_wire = {'disable_encoding': True}
                        if headless:
                            is_headless = 'Yes'
                            chrome_options.add_argument('--headless')
                            chrome_options.add_argument('--no-sandbox')
                            #driver = webdriver.Chrome(executable_path = chromedriver_path, options=chrome_options, seleniumwire_options=options_wire)
                            driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)
                        else:
                            is_headless = 'No'
                            #driver = webdriver.Chrome(executable_path = chromedriver_path, options=chrome_options, seleniumwire_options=options_wire)
                            driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)
                            
                        user_location = set_location(driver, inputlocation)
                        is_location_set = True
                        if not user_location:
                            is_remove_file = True
                            break
                        
                    prod_desc = row[item_desc_index]
                    url = row[url_index]
                    if url.startswith('https://www.dmart.in/product/'):# and line_count in [25,26]:
                        units = []
                        if uom_index != 0:
                            units = re.findall(r"[-+]?\d*\.\d+|\d+", row[uom_index])
                        if not units:
                            units = re.findall(r"[-+]?\d*\.\d+|\d+", prod_desc)
                        units = list(map(float, units))
                        units.sort()
                        product_name, orig_quan, mrp, selling_price, brand, comments = read_dmart(prod_desc, url, units, driver)
                        
                        csvwriter.writerow([prod_desc, product_name, orig_quan, mrp, selling_price, brand, url, comments, row[region_index], row[category_index], row[sub_category_index], row[item_code_index], row[std_uom_index], row[uom_index], row[sku_type_index], row[type_index], row[url_norm_index], row[store_name_index], row[store_code_index], row[pincode_index], row[competitor_index], row[spar_price_index], row[margin_index]])
                        links_count += 1
                        
                        #sleep_time = random.randint(1, 5)
                        #print (f'\t      Sleeping {sleep_time} sec')
                        #time.sleep(sleep_time)
                    else:
                        csvwriter.writerow([prod_desc, '', '', '', '', '', url, '', row[region_index], row[category_index], row[sub_category_index], row[item_code_index], row[std_uom_index], row[uom_index], row[sku_type_index], row[type_index], row[url_norm_index], row[store_name_index], row[store_code_index], row[pincode_index], row[competitor_index], row[spar_price_index], row[margin_index]])
                
                csvfile.flush()

            if links_count > 0:
                minutes , seconds = divmod((time.time() - start_time), 60)
                print (f'\nInput      : {input_csv.name}')
                print   (f'Output     : {fileame_with_dir}')
                print   (f'Website    : DMart')
                print   (f'Location   : {user_location}')
                print   (f'Links      : {links_count}')
                print   (f'Headless   : {is_headless}')
                if int(minutes) > 0:
                    print   (f'Time Taken : {int(minutes)}.{int(seconds)} minutes')
                else:
                    print   (f'Time Taken : {int(seconds)} seconds')
            else:
                is_remove_file = True
                print (f'\nNo DMart URLs found in the input CSV file')

    if is_remove_file:
        if os.path.isfile(fileame_with_dir):
            os.remove(fileame_with_dir)
        driver.quit()
        sys.exit()

    if is_location_set:
        driver.quit()
        
def read_dmart(prod_desc, url, prod_quan_in_input, driver):
    print (f'{prod_desc}')
    print (f'\t{url}')
    
    orig_quan = price = selling_price = brand = product_name = comments = ''
    
    driver.get(url)
    
    get_json = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.ID, '__NEXT_DATA__'))).get_attribute("innerHTML")

    product_found = False
    if get_json:
        product_data = json.loads(get_json)
        if 'props' in product_data:
            if 'pageProps' in product_data['props']:
                if 'pdpData' in product_data['props']['pageProps']:
                    if 'dynamicPDP' in product_data['props']['pageProps']['pdpData']:
                        if 'data' in product_data['props']['pageProps']['pdpData']['dynamicPDP']:
                            if 'productData' in product_data['props']['pageProps']['pdpData']['dynamicPDP']['data']:
                                if product_data['props']['pageProps']['pdpData']['dynamicPDP']['data']['productData']:
                                    if 'name' in product_data['props']['pageProps']['pdpData']['dynamicPDP']['data']['productData']:
                                        product_name = (product_data['props']['pageProps']['pdpData']['dynamicPDP']['data']['productData']['name'])
                                    if 'manufacturer' in product_data['props']['pageProps']['pdpData']['dynamicPDP']['data']['productData']:
                                        brand = product_data['props']['pageProps']['pdpData']['dynamicPDP']['data']['productData']['manufacturer']
                                    if 'sKUs' in product_data['props']['pageProps']['pdpData']['dynamicPDP']['data']['productData']:
                                        for quantities in (product_data['props']['pageProps']['pdpData']['dynamicPDP']['data']['productData']['sKUs']):
                                            prod_original_quan = re.findall(r"[-+]?\d*\.\d+|\d+", quantities['variantTextValue'])
                                            prod_original_quan = list(map(float, prod_original_quan))
                                            prod_original_quan.sort()
                                            orig_quan = quantities['variantTextValue']
                                            price = quantities['priceMRP']
                                            selling_price = quantities['priceSALE']
                                            if prod_quan_in_input == prod_original_quan:
                                                break
                                            else:
                                                input_quan_sum = sum(prod_quan_in_input)
                                                input_quan_mult = math.prod(prod_quan_in_input)
                                                prod_orig_quan_sum = sum(prod_original_quan)
                                                prod_orig_quan_mult = math.prod(prod_original_quan)
                                                if len(prod_original_quan) == 1:
                                                    if input_quan_sum == prod_original_quan[0]:
                                                        break
                                                    elif input_quan_mult == prod_original_quan[0]:
                                                        break
                                                if len(prod_quan_in_input) == 1:
                                                    if prod_orig_quan_sum == prod_quan_in_input[0]:
                                                        break
                                                    elif prod_orig_quan_mult == prod_quan_in_input[0]:
                                                        break  
                                        product_found = True
    
        if not product_found:
            comments = 'Product NOT Found'

        if comments:
            print (f'\t\t{comments}\n')
        elif brand:
            print (f'\t\t{product_name} | {orig_quan} | {selling_price} | {price} | {brand} \n')
        else:
            print (f'\t\t{product_name} | {orig_quan} | {selling_price} | {price}\n')
    
    else:
        comments = 'Error getting response from product URL'
        print (f'\t\t{comments}')
    
    return product_name, orig_quan, price, selling_price, brand, comments

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input',    type=argparse.FileType("r"), help="Input CSV file")
    parser.add_argument('-o', '--output',   type=str, help="Output Directory")
    parser.add_argument('-w', '--website',  type=str, help="CSV Column Name")
    parser.add_argument('-l', '--location', type=str, help="DMart Location")
    parser.add_argument('-H', '--headless', help="Browser headless or not (True/False). Default is True", default=True)
    
    if len(sys.argv)==1:
        parser.print_help(sys.stderr)
        sys.exit()
    
    args = parser.parse_args()
    
    if not args.input:
        parser.print_help(sys.stderr)
        print (f'\nError: Provide Input CSV file')
        sys.exit()
    if not args.output:
        parser.print_help(sys.stderr)
        print (f'\nError: Provide output Directory')
        sys.exit()
    if not args.website:
        parser.print_help(sys.stderr)
        print (f'\nError: Provide DMart column name from CSV')
        sys.exit()
    if not args.location:
        parser.print_help(sys.stderr)
        print (f'\nError: Provide DMart Location')
        sys.exit()
    headless = False if args.headless == "False" else True
    
    filename_with_extension = os.path.basename(args.input.name)
    filename = os.path.splitext(filename_with_extension)[0]

    read_csv(args.input, args.output, args.website, args.location, headless, filename)

if __name__ == "__main__":
    start_time = time.time()
    main()       