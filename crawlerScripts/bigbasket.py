import argparse, os, sys, csv, time, re, traceback, json, math
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from bs4 import BeautifulSoup


def take_screenshot(driver, save_as):
    print(f'Taking Screenshot.. -> {save_as}')
    driver.get_screenshot_as_file(save_as)

def set_location(driver, inputlocation):
    """Function to set BigBasket Location"""
    from selenium.webdriver.support.ui import WebDriverWait
    from selenium.webdriver.support import expected_conditions as EC
    from selenium.webdriver.common.by import By
    
    def highlight(element):
        """Highlights (blinks) a Selenium Webdriver element"""
        driver = element._parent
        def apply_style(s):
            driver.execute_script("arguments[0].setAttribute('style', arguments[1]);",
                                  element, s)
        original_style = element.get_attribute('style')
        apply_style("background: yellow; border: 2px solid red;")
        time.sleep(.2)
        apply_style(original_style)
    
    location_xpath = '//*[@id="headerControllerId"]/header/div/div/div/div/ul/li[2]/div/a'
    location_dropdown = '//*[@id="headerControllerId"]/header/div/div/div/div/ul/li[2]/div/div/div[2]/form/div[1]/div'
    location_list = '//*[@id="headerControllerId"]/header/div/div/div/div/ul/li[2]/div/div/div[2]/form/div[1]/div/ul[1]'
    location_input = '//*[@id="headerControllerId"]/header/div/div/div/div/ul/li[2]/div/div/div[2]/form/div[1]/div/input[1]'
    location_select = '//*[@id="ui-select-choices-row-1-0"]/a'
    location_continue = '//*[@id="headerControllerId"]/header/div/div/div/div/ul/li[2]/div/div/div[2]/form/div[3]/button'
    selected_location_xpath_1 = '//*[@id="headerControllerId"]/header/div/div/div/div/ul/li[2]/div/a/span'
    selected_location_xpath_2 = '/html/body/div[1]/div/header[2]/div[1]/div[2]/div[1]/div/div/button/span[2]/span[2]'

    DELAY = 20
    url = 'https://www.bigbasket.com'
    print (f'Setting Location: {inputlocation.title()}')
    driver.get(url)
    
    start = time.time()
    while True:
        try:
            location = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, location_xpath)))
            highlight(location)
            location.click()
            break
        except Exception as err:
            pass
        if (time.time() - start) > 20:
            break

    start = time.time()
    while True:
        try:    
            dropdown = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, location_dropdown)))
            highlight(dropdown)
            dropdown.click()
            break
        except Exception as err:
            pass
        if (time.time() - start) > 20:
            break
            
    start = time.time()
    while True:
        try:
            locations_list = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, location_list)))
            locations_html = locations_list.get_attribute('innerHTML')
            locations_soup = BeautifulSoup(str(locations_html), "html.parser")
            all_locations = locations_soup.text.split()
            is_location_in_list = inputlocation.lower().strip() in (string.lower() for string in all_locations)
            break
        except Exception as err:
            pass
        if (time.time() - start) > 20:
            break

    if is_location_in_list:
            try:
                location_user_input = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, location_input)))
                highlight(location_user_input)
                location_user_input.clear()
                location_user_input.send_keys(inputlocation)

                location_input_select = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, location_select)))
                highlight(location_input_select)
                location_input_select.click()

                continue_button = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, location_continue)))
                highlight(continue_button)
                continue_button.click()

                try:
                    selected_location = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, selected_location_xpath_1)))
                except Exception:
                    selected_location = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, selected_location_xpath_2)))
                print (f'\nSelected Location: {selected_location.text}\n')
                return (selected_location.text)
            except Exception as err:
               print (err)
         
    else:
        print (f'BigBasket is NOT serviceable in {inputlocation.title()}')
        driver.quit()

def read_csv(input_csv, path, website_name, inputlocation, headless, chromedriver_path):
    is_headless = ''
    is_location_set = False
    is_remove_file = False
    with input_csv:
        csv_reader = csv.reader(input_csv, delimiter=',')
        with open(path+"//"+website_name+"_"+inputlocation+".csv", 'w', encoding='utf-8-sig', newline='') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(['ITEM_DESC','PRODUCT_NAME','QUANTITY','PRICE','SELLING_PRICE', 'BRAND','WEBSITE_LINK', 'Comments'])
            links_count = 0
            for line_count, row in enumerate(csv_reader):
                if line_count == 0:
                    website_name_flag = False
                    item_desc_flag = False
                    quantity_flag = False
                    url_index = 0
                    item_desc_index = 0
                    quantity_index = 0
                    for column_index, column_name in enumerate(row):
                        if column_name.lower() == f"{website_name.lower()} url":
                            url_index = column_index
                            website_name_flag = True
                        
                        elif column_name.lower() == 'item_desc':
                            item_desc_index = column_index
                            item_desc_flag = True
                            
                        elif column_name.lower() == 'uom':
                            quantity_index = column_index
                            quantity_flag = True
                        
                        if website_name_flag and item_desc_flag and quantity_flag:
                            break
                        
                    if not website_name_flag or not item_desc_flag:
                        print (f'\nColumn Header "item_desc" or "{website_name} url" NOT found in {input_csv.name} file, Exiting..')
                        is_remove_file = True
                        break

                else:
                    if not is_location_set:
                        chrome_options = webdriver.ChromeOptions()
                        chrome_options.add_argument("--window-size=1920,1080")
                        chrome_options.add_argument("--start-maximized")
                        chrome_options.add_argument('--ignore-certificate-errors')
                        chrome_options.add_argument('--ignore-ssl-errors')
                        chrome_options.add_argument('log-level=3')
                        if headless:
                            is_headless = 'Yes'
                            chrome_options.add_argument('--headless')
                            chrome_options.add_argument('--no-sandbox')
                            driver = webdriver.Chrome(executable_path = chromedriver_path, options=chrome_options)
                        else:
                            is_headless = 'No'
                            driver = webdriver.Chrome(executable_path = chromedriver_path, options=chrome_options)
                            
                        user_location = set_location(driver, inputlocation)
                        is_location_set = True
                        if not user_location:
                            is_remove_file = True
                            break

                    prod_desc = row[item_desc_index]
                    url = row[url_index]
                    if url.startswith('https://www.bigbasket.com/pd/'):
                        units = []
                        if quantity_index != 0:
                            units = re.findall(r"[-+]?\d*\.\d+|\d+", row[quantity_index])
                        if not units:
                            units = re.findall(r"[-+]?\d*\.\d+|\d+", prod_desc)
                        units = list(map(float, units))
                        units.sort()
                        product_name, orig_quan, mrp, selling_price, brand, comments = read_bigbasket(prod_desc, url, units, driver)
                        csvwriter.writerow([prod_desc, product_name, orig_quan, mrp, selling_price, brand, url, comments])
                        links_count += 1
                    else:
                        csvwriter.writerow([prod_desc, '', '', '', '', '', url, ''])
                
                csvfile.flush()

            if links_count > 0:
                minutes , seconds = divmod((time.time() - start_time), 60)
                print (f'\nInput      : {input_csv.name}')
                print   (f'Output     : {path}\{website_name}_{inputlocation}.csv')
                print   (f'Website    : BigBasket')
                print   (f'Location   : {user_location}')
                print   (f'Links      : {links_count}')
                print   (f'Headless   : {is_headless}')
                if int(minutes) > 0:
                    print   (f'Time Taken : {int(minutes)}.{int(seconds)} minutes')
                else:
                    print   (f'Time Taken : {int(seconds)} seconds')

    if is_remove_file:
        if os.path.isfile(path+"//"+website_name+"_"+inputlocation+".csv"):
            os.remove(path+"//"+website_name+"_"+inputlocation+".csv")
        driver.quit()
        sys.exit()

    if is_location_set:
        driver.quit()

def read_bigbasket(prod_desc, url, units, driver):
    print (f'{prod_desc}')
    print (f'\t{url.split("/?")[0]}')

    orig_quan = mrp = selling_price = brand = product_name = comments = ''
    prod_quan_in_input = units

    driver.get(url)

    try:       
        if "window.__PRELOADED_STATE__ = " in driver.page_source:
            jsonSource = driver.find_element(By.XPATH, "//script[contains(text(),'window.productId')]").get_attribute('text').split("window.__PRELOADED_STATE__ = ",1)[1]
            jsonSource = json.loads(jsonSource)
            jsonSource = json.loads(jsonSource)
            #print(json.dumps(jsonSource, indent = 3))
            
            if 'product' in jsonSource:
                if 'variants' in jsonSource['product']:
                    for each_variant in jsonSource['product']['variants']:
                        if 'w' in each_variant:
                            orig_quan = each_variant['w']
                            prod_quan = re.findall(r"[-+]?\d*\.\d+|\d+", orig_quan)
                            prod_quan = list(map(float, prod_quan))
                            prod_quan.sort()
                            
                            pack_desc_quan = []
                            if 'packDesc' in each_variant:
                                pack_description = each_variant['packDesc']
                                if pack_description:
                                    pack_desc_quan = re.findall(r"[-+]?\d*\.\d+|\d+", pack_description)
                                    pack_desc_quan = list(map(float, pack_desc_quan))
                                    pack_desc_quan.sort()
                            
                            product_name = each_variant['desc'].strip()
                            selling_price = each_variant['sp']
                            mrp = each_variant['mrp']
                            
                            if 'brand' in each_variant:
                                if 'name' in each_variant['brand']:
                                    brand = each_variant['brand']['name']

                            if prod_quan_in_input == prod_quan:
                                break
                            else:
                                prod_quan_with_pack_desc = prod_quan + pack_desc_quan
                                prod_quan_with_pack_desc.sort()
                                if prod_quan_in_input == prod_quan_with_pack_desc:
                                    break
                                elif sum(prod_quan_in_input) == sum(prod_quan_with_pack_desc):
                                    break
                                elif sum(prod_quan_in_input) == math.prod(prod_quan_with_pack_desc):
                                    break
                                
                        else:
                            comments = 'Product NOT Found'
                else:
                    comments = 'Product NOT Found'
            else:
                comments = 'Product NOT Found'
        
        elif "__NEXT_DATA__" in driver.page_source:
            jsonSource = driver.find_element(By.ID, "__NEXT_DATA__").get_attribute('text')
            jsonSource = json.loads(jsonSource)
            #print(json.dumps(jsonSource, indent = 3))
            
            if 'props' in jsonSource:
                if 'pageProps' in jsonSource['props']:
                    if 'productDetails' in jsonSource['props']['pageProps']:
                        if 'children' in jsonSource['props']['pageProps']['productDetails']:
                            if jsonSource['props']['pageProps']['productDetails']['children']:
                                for each_child in reversed(jsonSource['props']['pageProps']['productDetails']['children']):

                                    product_name = each_child['description']

                                    if 'brand' in each_child:
                                        if 'name' in each_child['brand']:
                                            brand = each_child['brand']['name']
                                            
                                    if each_child['availability']['avail_status'] == '000':
                                        comments = 'Out Of Stock'
                                    elif each_child['availability']['avail_status'] == '010':
                                        comments = 'Currently Unavailable'
                                    
                                    if 'pricing' in each_child:
                                        if 'discount' in each_child['pricing']:
                                            mrp = each_child['pricing']['discount']['mrp']
                                            selling_price = each_child['pricing']['discount']['prim_price']['sp']

                                    orig_quan = each_child['weight']
                                    prod_quan = re.findall(r"[-+]?\d*\.\d+|\d+", orig_quan)
                                    prod_quan = list(map(float, prod_quan))
                                    prod_quan.sort()
                                    
                                    pack_desc_quan = []
                                    if 'packDesc' in each_child:
                                        pack_description = each_child['packDesc']
                                        if pack_description:
                                            pack_desc_quan = re.findall(r"[-+]?\d*\.\d+|\d+", pack_description)
                                            pack_desc_quan = list(map(float, pack_desc_quan))
                                            pack_desc_quan.sort()
                                    
                                    if prod_quan_in_input == prod_quan:
                                        break
                                    else:
                                        prod_quan_with_pack_desc = prod_quan + pack_desc_quan
                                        prod_quan_with_pack_desc.sort()
                                        if prod_quan_in_input == prod_quan_with_pack_desc:
                                            break
                                        elif sum(prod_quan_in_input) == sum(prod_quan_with_pack_desc):
                                            break
                                        elif sum(prod_quan_in_input) == math.prod(prod_quan_with_pack_desc):
                                            break
                            else:
                                comments = 'Product NOT Found'
                        else:
                            comments = 'Product NOT Found'
                    else:
                        comments = 'Product NOT Found'
                else:
                    comments = 'Product NOT Found'
            else:
                comments = 'Product NOT Found'
        
        if comments and comments == 'Product NOT Found':
            print (f'\t\t{comments}')
        else:
            if comments and comments != 'Product NOT Found':
                print (f'\t\t{product_name} | {orig_quan} | {selling_price} | {mrp} | {brand} | {comments}')
            else:
                print (f'\t\t{product_name} | {orig_quan} | {selling_price} | {mrp} | {brand}')

    except Exception as err:
        traceback.print_exc()
    #time.sleep(300)
    return product_name, orig_quan, mrp, selling_price, brand, comments


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', type=argparse.FileType("r"), help="Input CSV file")
    parser.add_argument('-o', '--output', type=str, help="Output Directory")
    parser.add_argument('-w', '--website', type=str, help="CSV Column Name")
    parser.add_argument('-l', '--location', type=str, help="BigBasket Location")
    parser.add_argument('-H', '--headless', help="Browser headless or not (True/False). Default is True", default=True)

    if len(sys.argv)==1:
        parser.print_help(sys.stderr)
        sys.exit()
    
    args = parser.parse_args()

    if sys.platform == 'win32':
        chromedriver_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "chromedriver.exe")
        if not os.path.exists(os.path.join(os.path.dirname(os.path.abspath(__file__)), "chromedriver.exe")):
            print (f'\nChrome Driver Not Found, Exiting..')
            sys.exit()
    elif 'linux' in sys.platform:
        chromedriver_path = "/usr/bin/chromedriver"
        if not os.path.exists("/usr/bin/chromedriver"):
            print (f'\nChrome Driver Not Found, Exiting..')
            sys.exit()
    else:
        print(f'Unknown OS, Exitting..')
        sys.exit()

    headless = False if args.headless == "False" else True
    
    if not args.input:
        parser.print_help(sys.stderr)
        print (f'\nError: Provide Input CSV file')
        sys.exit()
    if not args.output:
        parser.print_help(sys.stderr)
        print (f'\nError: Provide output Directory')
        sys.exit()
    if not args.website:
        parser.print_help(sys.stderr)
        print (f'\nError: Provide BigBasket column name from CSV')
        sys.exit()
    if not args.location:
        parser.print_help(sys.stderr)
        print (f'\nError: Provide BigBasket Location')
        sys.exit()
        
    read_csv(args.input, args.output, args.website, args.location, headless, chromedriver_path)

if __name__ == "__main__":
    start_time = time.time()
    main()
