import argparse, csv, sys, time, datetime, glob, os, chardet
import pandas as pd

competitor_websites = ['jiomart', 'dmart', 'starquik', 'spencers']  # 'spencers', 
categories          = ['GROCERY', 'FMCG FOODS', 'FMCG N FOODS', 'FRUIT AND VEG', 'DAIRY AND FROZEN', 'FISH AND MEAT']
split_csv_header    = ['REGION', 'Category', 'Sub_Category', 'ITEM CODE', 'Item_Desc', 'Standard_UOM', 'UOM', 'SKU TYPE']
margin              = {"kvi": {"GROCERY": "3%", "FMCG FOODS": "7%", "FRUIT AND VEG": "3%", "DAIRY AND FROZEN": "3%", "FISH AND MEAT": "5%", "FMCG N FOODS": "7%"}, "pss": {"GROCERY": "5%", "FMCG FOODS": "7%", "FRUIT AND VEG": "3%", "DAIRY AND FROZEN": "5%", "FISH AND MEAT": "5%", "FMCG N FOODS": "7%"}}

def read_location_pincode_csv(location_csv):
    with open(location_csv, 'r') as file:
        csv_file = csv.DictReader(file)
        location_pincode_csv = list(csv_file)
        return location_pincode_csv
 
def split_new(input_csv_df, spar_price_df, location_pincode_csv, sku_type, out_dir):
    input_csv_df.columns = input_csv_df.columns.str.lower()
    spar_price_df.columns = spar_price_df.columns.str.lower()
    valid_data = invalid_data = total_urls = 0
    for each_store in location_pincode_csv:
        each_store = {key.lower(): value for key, value in each_store.items()}
        loc_region          = each_store.get('region')
        loc_store_name      = each_store.get('store name')
        loc_store_code      = each_store.get('store code')
        loc_pincode         = each_store.get('pincode')
        if loc_region and loc_pincode:
            for category in categories:
                competitor_name = each_store.get(category.lower())
                if competitor_name:
                    if competitor_name.lower() in competitor_websites:
                        selected_input_df = input_csv_df.loc[
                        (input_csv_df['category'] == category) &
                        input_csv_df[f'{competitor_name.lower()} url'].str.contains('http', case=False)
                        ]
                        
                        selected_spar_df = spar_price_df.loc[
                        (spar_price_df['region'] == loc_region) &
                        (spar_price_df['category'] == category) &
                        (spar_price_df['type'] == sku_type.upper()) &
                        (spar_price_df['store_code'] == int(loc_store_code)) &
                        (spar_price_df['item_code'].isin(input_csv_df['item code']))
                        ]

                        selected_spar_df = selected_spar_df.drop(['category', 'sub_category', 'item_name', 'uom'], axis=1)
                        
                        merged_df = pd.merge(selected_input_df, selected_spar_df, left_on='item code', right_on='item_code', how='inner')
                        merged_df = merged_df.drop(['item_code'], axis=1)
                        merged_df['competitor'] = competitor_name
                        urls = merged_df.shape[0]
                        total_urls += urls

                        if urls:
                            store_name = merged_df['store_name'].unique()[0]
                            merged_df['margin'] = margin[sku_type.lower()][category]
                            if f'{competitor_name.lower()} needs normalization' not in merged_df.columns.tolist():
                                merged_df[f'{competitor_name.lower()} needs normalization'] = ''
                            
                            merged_df.rename(columns = {'sub-category':'sub_category', 'item-desc': 'item_desc', 'store_name':'store name', 'spar_price':'spar price', 'sku_type':'sku type', 'store_code':'store code'}, inplace = True) # Renaming Column Names
                            
                            filename = f'{loc_region}_{store_name}_{loc_store_code}_{category}_{sku_type}_{competitor_name}_{loc_pincode}_{datetime.datetime.now().strftime("%d%b%y")}.csv'.replace(" ", "")
                            merged_df.to_csv(f'{out_dir}//{filename}', index=None)
                            valid_data += 1
                            print (f'[+] {loc_region:4s} {loc_pincode:8s} {category:18s} {competitor_name:8s} - {urls} URLs')
                        else:
                            invalid_data += 1
                            print (f'[-] {loc_region:4s} {loc_pincode:8s} {category:18s} {competitor_name:8s} - {urls} URLs')

    if valid_data:
        print (f'\n{valid_data} files created')
    if invalid_data:
        print (f'{invalid_data} locations data are unavailable')
    print (f'Total URLs: {total_urls}')
 
def delete_csvs(directory_path):
    csv_files = glob.glob(os.path.join(directory_path, "*.csv"))
    removed_files_count = 0
    if csv_files:
        for file in csv_files:
            filename_with_extension = os.path.basename(file)
            filename = os.path.splitext(filename_with_extension)[0]
            generated_file = filename.split('_')
            if len(generated_file) == 8:
                os.remove(file)
                removed_files_count += 1
    if removed_files_count:
        print (f'\nDeleted {removed_files_count} csv files from {directory_path}')

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input',    type=str, required=True, help="Input CSV file")
    parser.add_argument('-s', '--spar',     type=str, required=True, help="Spar price CSV")
    parser.add_argument('-o', '--output',   type=str, required=True, help="Output Directory")
    parser.add_argument('-t', '--type',     type=str, required=True, help="SKU Type (KVI/PSS)")
    parser.add_argument('-l', '--location', type=str, required=True, help="Location-Pincode mapping file")
    args = parser.parse_args()
    
    if not args.input:
        parser.print_help(sys.stderr)
        print (f'\nError: Please provide Input CSV file')
        sys.exit()
    if not args.spar:
        parser.print_help(sys.stderr)
        print (f'\nError: Please Provide Spar price CSV file')
        sys.exit()
    if not args.output:
        parser.print_help(sys.stderr)
        print (f'\nError: Please Provide output Directory')
        sys.exit()
    if not args.type:
        parser.print_help(sys.stderr)
        print (f'\nError: Please Provide SKU Type (KVI/PSS)')
        sys.exit()
    else:
        if args.type.lower() not in ['kvi', 'pss']:
            print (f'\nError: SKU Type should be KVI or PSS')
            sys.exit()
    if not args.location:
        parser.print_help(sys.stderr)
        print (f'\nError: Please Provide Spar Store-Pincode mapping CSV file')
        sys.exit()   

    delete_csvs(args.output)
    
    with open(args.input, 'rb') as rawdata:
        data_encoding = chardet.detect(rawdata.read(100000))
    input_csv_df         = pd.read_csv(args.input, encoding =data_encoding['encoding'])
    spar_price_df         = pd.read_csv(args.spar, delimiter=';')
    location_pincode_csv    = read_location_pincode_csv(args.location)
    
    split_new(input_csv_df, spar_price_df, location_pincode_csv, args.type, args.output)

    minutes , seconds = divmod((time.time() - start_time), 60)
    if int(minutes) > 0:
        print   (f'Time Taken: {int(minutes)}.{int(seconds)} minutes')
    else:
        print   (f'Time Taken: {int(seconds)} seconds')

if __name__ == "__main__":
    start_time = time.time()
    main()