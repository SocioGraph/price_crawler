import argparse, os, sys, csv, time, re, locale, datetime
from selenium import webdriver  # selenium==4.5.0, webdriver-manager==3.8.4
from bs4 import BeautifulSoup

from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

decimal_point_char = locale.localeconv()['decimal_point']

def take_screenshot(driver, save_as):
    print(f"Taking Screenshot.. -> {save_as}")
    driver.get_screenshot_as_file(save_as)

def selectLocation(driver, inputLocation):

    driver.execute_script(f"console.log('Setting location to load.'); window.location_to_load = {inputLocation}; console.log(window.location_to_load);")

    script = """
        var script_out = {
            "result":"",
            "err":""
        }
        
        try {
            //Clicking on 'Serving Store' button. This opens a dialog where pincode/area name has to be given and selected from auto-suggestion drop-down.

            document.getElementById("current_store").click()

            function setLocation() {
                console.log("Attempting to set location.");

                //Entering input text.
                document.querySelector("#locate-address").value = window.location_to_load;

                //Clicking on 'select delivery store' button.
                document.querySelector("div.SelectDelivery").querySelector("a.SelectDeliveryStore").click()

                //Clicking on first checkbox of list of stores.
                setTimeout(function() {
                        document.querySelector('ul.search_store_suggesion > li > input').click()
                        //Clicking on `select delivery location` button.
                        setTimeout(function () {
                            document.querySelector("div.SelectDelivery").querySelector("a.SelectDeliveryLocation1").click();
                        }, 2000);
                }, 2000);

            }

            //Call setLocation function.
            setTimeout(setLocation, 5000);

            console.log("Returning.")
            return script_out
        } catch (err) {
            script_out["err"] = err;
            return script_out;
        }"""
    driver.execute_script(script, 560047)

def do_clean(product_price):
    if product_price:
        clean = re.sub(r'[^0-9'+decimal_point_char+r']+', '', str(product_price))
        product_price = float(clean)

    return product_price

if __name__ == "__main__":
    start_time = time.time()
    crawl_time = datetime.datetime.now().strftime('%d-%m-%y-%H:%M:%S')

    driver          = None
    output          = []
    ouputHeaders    = None
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-i', '--input',    help = "Input CSV file containing links.", default = "./inputFiles/Benchmarking_300_all.csv")
    parser.add_argument('-o', '--output',   help = "Directory where result CSVs are stored.", default = "./scrapedData/")
    parser.add_argument('-w', '--website',  help = "Column name of the link for corresponding website.")
    parser.add_argument('-H', '--headless', help = "Run chromedriver headless.", default=True)
    parser.add_argument('-l', '--location', help = "Spencers location to search.")

    if len(sys.argv)==1:
        parser.print_help(sys.stderr)
        sys.exit()

    args = parser.parse_args()

    if sys.platform == 'win32':
        chromedriver_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "chromedriver.exe")
        if not os.path.exists(os.path.join(os.path.dirname(os.path.abspath(__file__)), "chromedriver.exe")):
            print (f'\nChrome Driver Not Found, Exiting..')
            sys.exit()
    elif 'linux' in sys.platform:
        chromedriver_path = "/usr/bin/chromedriver"
        if not os.path.exists("/usr/bin/chromedriver"):
            print (f'\nChrome Driver Not Found, Exiting..')
            sys.exit()
    else:
        print(f'Unknown OS, Exitting..')
        sys.exit()

    inputfile   = args.input
    
    filename_with_extension = os.path.basename(inputfile)
    filename    = os.path.splitext(filename_with_extension)[0]

    outdir      = args.output
    website     = args.website
    headless    = False if args.headless == "False" else True
    inputLocation = args.location.lower()

    #print(f"Input file is taken as {inputfile}")
    #print(f"Chromedriver is loaded with headless = {headless}, {type(headless)}")

    if not os.path.isfile(inputfile):
        print("Input file doesn't exists.")
        sys.exit()
    
    with open(inputfile, "r") as csv_file:
        csv_reader  = csv.DictReader(csv_file, delimiter = ",")
        line_count  = 0
        links_count = 0
        for row in csv_reader:
            if line_count == 0:
                line_count          = line_count +1 
                website_row_name    = f"{website.lower()} url"
                if not website_row_name in row:
                    print(f"No column named {website}.")
                    sys.exit()
                
                chrome_options = webdriver.ChromeOptions()
                chrome_options.add_argument("--window-size=1920,1080")
                chrome_options.add_argument("--start-maximized")
                chrome_options.add_argument('--ignore-certificate-errors')
                chrome_options.add_argument('--ignore-ssl-errors')
                chrome_options.add_argument('log-level=3')
                
                if headless:
                    chrome_options.add_argument('--headless')
                    chrome_options.add_argument('--no-sandbox')
                    #driver = webdriver.Chrome(executable_path = chromedriver_path, options=chrome_options)
                    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)
                else:
                    #driver = webdriver.Chrome(executable_path = chromedriver_path, options=chrome_options)
                    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)

                driver.get("https://mcprod.spencers.in/")

                selectLocation(driver, inputLocation)
                time.sleep(15)
                

            product_url         = row[website_row_name]
            product_price       = None
            product_brand       = None
            product_name        = None
            product_mrp         = None
            comments            = ""
            product_quantity    = None

            #print(f"{row['Item_Desc']}, {product_url}")
            if product_url.startswith("https://mcprod.spencers.in") or product_url.startswith("https://www.spencers.in"):
                print(f"{row['item_desc']}\n\t{product_url}")
                
                
                retry = 0
                while retry < 5:
                    try:
                        driver.get(product_url)
                        break
                    except Exception:
                        pass
                    retry += 1
                
                
                #driver.get(product_url)
                #screenshots_dir = f"./screenshots/{inputLocation}/{crawl_time}"
                #if not os.path.isdir(screenshots_dir):
                #    os.makedirs(screenshots_dir, exist_ok=True)

                #print("Taking Screenshot..")
                #take_screenshot(driver, f"screenshots/{row['Item_Desc']}.png")

                content = driver.page_source.encode('utf-8').strip()
                soup = BeautifulSoup(content, features="html.parser")

                pageNotFound = driver.execute_script("""
                    let ele = document.querySelector("div.pagenotfound-container");
                    if (ele) {
                        return "404";
                    }
                """)

                if pageNotFound == "404":
                    #print("\t\tPage not found.")
                    comments = "Not Found"
                else:
                    productNameJsQuery = driver.execute_script("""
                        var script_out = {
                            "result":"",
                            "err":""
                        }
                        try {
                            element = document.querySelector("h1.page-title").querySelector("span.base").innerText;
                            script_out["result"] = element;
                            return script_out
                        } catch (err) {
                            script_out["err"] = err;
                            return script_out;
                        }
                    """)
                    product_name =  productNameJsQuery["result"]
                    #print(f"Product Name : {product_name}")

                    if not product_name:
                        product_quantity = ""
                        # sys.exit()
                    else:
                        product_quantity = "".join(product_name.split(' ')[-1])
                        #print(product_name, row['Item_Desc'])

                    #print(f"Product Quantity : {product_quantity}")


                    jsScriptOut = driver.execute_script("""
                        var crawler_out = {
                            "result":"",
                            "err":""
                        };

                        try {
                                let element = undefined;
                                element = document.querySelector("div.product-info-price > div.price-box > span.price-container > span > span.price");

                                if(!element) {
                                    element = document.querySelector("div.product-info-price > div.price-box > span.special-price > span.price-container > span.price-wrapper > span.price")
                                }

                                console.log(element);
                                crawler_out["result"] = element.innerText;
                            } catch(err) {
                                //console.log(err);
                                crawler_out["err"] = err;
                            }

                        return crawler_out;
                        """
                    )  

                    product_price = do_clean(jsScriptOut["result"])
                    

                    if product_price:
                        if int(product_price) == 0:
                            jsScriptOut = driver.execute_script("""
                                var crawler_out = {
                                    "result":"",
                                    "err":""
                                };
                                
                                try {
                                    let element = undefined;
                                    element = document.querySelector("div.footer_post").querySelector("span.price-container > span.price-wrapper");
                                    crawler_out["result"] = element.innerText;
                                } catch (err) {
                                    crawler_out["err"] = err;
                                }

                                return crawler_out
                            """) 

                        product_price = do_clean(jsScriptOut["result"])

                    #print(f"Product Price: {product_price}")

                    brandJsQuery = driver.execute_script("""
                        var script_out = {
                            "result":"",
                            "err":""
                        };
                        try {
                            elements = document.querySelectorAll("div._2yfKw");
                            for (element of elements) {
                                try {
                                    aEle = element.querySelector("a.rippleEffect");
                                    if (aEle.getAttribute("context") == "brandlabel") {
                                        script_out["result"] = aEle.innerHTML;
                                        return script_out;
                                    }
                                } catch (err) {
                                    console.log(err);
                                    crawler_out["err"] = err;
                                }
                            }
                        } catch (err) {
                            script_out["err"] = err;
                            return script_out;
                        }
                    """)
                    #print(brandJsQuery)

                    if brandJsQuery:
                        product_brand =  brandJsQuery["result"]

                    mrpJsQuery = driver.execute_script("""
                        var script_out = {
                            "result":"",
                            "err":""
                        }
                        try {
                            element =  document.querySelector("div.product-info-price > div.price-box > span.old-price");
                            final_price_element = document.querySelector("div.product-info-price > div.price-box > span.price-final_price");

                            if (element) {
                                element = element.querySelector("span.price-container > span.price-wrapper > span.price");
                            } else if (final_price_element) {
                                element = element.querySelector("span.price-wrapper > span.price");
                            }
                            script_out["result"] = element.innerText;
                            return script_out
                        } catch (err) {
                            console.log(err);
                            script_out["err"] = err;
                            return script_out;
                        }
                    """)
                    #print(mrpJsQuery)
                    product_mrp =  mrpJsQuery["result"]
                    
                    if comments:
                        print (f"{comments}")
                    else:
                        if product_brand:
                            print (f"\t\t{product_name} | {product_quantity} | {product_mrp} | {product_price} | {product_brand}")
                        else:
                            print (f"\t\t{product_name} | {product_quantity} | {product_price}")
                
                links_count += 1

            out = {
                "ITEM_DESC"     : row["item_desc"],
                "PRODUCT_NAME"  : product_name,
                "QUANTITY"      : product_quantity,
                "PRICE"         : product_mrp,
                "SELLING_PRICE" : product_price,
                "BRAND"         : product_brand,
                "WEBSITE_LINK"  : product_url,
                "Comments"      : comments,
                "REGION"        : row["region"],
                "Category"      : row["category"],
                "Sub_Category"  : row["sub_category"],
                "ITEM CODE"     : row["item code"],
                "Standard_UOM"  : row["standard_uom"],
                "UOM"           : row["uom"],
                "SKU TYPE"      : row["sku type"],
                "Type"          : row["type"],
                "Normalization" : row["spencers needs normalization"],
                "Store Name"    : row["store name"],
                "Store Code"    : row["store code"],
                "Pincode"       : row["pincode"],
                "Competitor"    : row["competitor"],
                "Spar Price"    : row["spar price"],
                "Margin"        : row["margin"]
            }

            if not ouputHeaders:
                #print("Setting output headers.")
                ouputHeaders = out.keys()
            output.append(out)
            #print(output)
            #break
            # time.sleep(3)

    if not os.path.isdir(outdir):
        print("Creating output directory.")
        os.mkdir(outdir)
    
    with open(f"{outdir}/{filename}_out.csv", "w", newline='', encoding="utf-8-sig") as wf:    
        csv_writer = csv.DictWriter(wf, ouputHeaders)
        csv_writer.writeheader()
        csv_writer.writerows(output)
    
    if links_count:
        minutes , seconds = divmod((time.time() - start_time), 60)
        print (f'\nInput      : {inputfile}')
        print   (f'Output     : {outdir}/{filename}_out.csv')
        print   (f'Website    : Spencers')
        print   (f'Location   : {inputLocation.title()}')
        print   (f'Links      : {links_count}')
        print   (f'Headless   : {headless}')
        if int(minutes) > 0:
            print (f'Time Taken : {int(minutes)}.{int(seconds)} minutes')
        else:
            print (f'Time Taken : {int(seconds)} seconds')
    else:
        print (f'\nNo Spencers URLs found in the input CSV file')
    driver.quit()
