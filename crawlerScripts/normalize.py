import re, argparse, csv, sys, traceback, os


def normalize(item):
    print("\n")
    print(item)
    product_quantity = item.get("quantity")
    product_price = item.get("selling_price")
    if not product_price:
        return None, None

    print(f"{item.get('product_name')} - {product_quantity} - {product_price}")
    normalized_price = None
    if product_quantity:
        # print(f"Product Quantity String: {product_quantity}")
        combo = re.findall(r"(?i)combo", product_quantity)
        if combo:
            print(f"Combo item: {combo}")
            item["comments"] = f"{item['comments']} \n Combo Item"
            return None, None

        qty_with_no_regex = "\d+x\d+"
        qty_regex = "(\d+(?:\.\d+)?)"

        qty_with_no = re.findall(r"{}".format(qty_with_no_regex), product_quantity)

        if qty_with_no:
            print("qty with no hit..")
            quantity = qty_with_no[0].split("x")[0]
            uom = 'pcs' #re.sub(r"{}".format(qty_with_no_regex), "", product_quantity)
        else:
            quantity = re.findall(r"{}".format(qty_regex), product_quantity)[0]
            uom = re.sub(r"{}".format(qty_regex), "", product_quantity)

        formatted_uom = uom.lower().strip().encode('ascii', 'ignore').decode('utf-8').strip()
        print(f"Quantity: {quantity}, uom: {uom}, formatted_uom: {formatted_uom}")

        if formatted_uom in ['kg', 'l', 'g', 'gm', 'gram', 'grams', 'l pouch', 'pcs', 'g pouch', 'g pack of', 'u','pc', 'millilitre', 'count', 'piece']:
            normalized_price = float(product_price)/float(quantity)
        else:
            print("UOM miss..")
            print(f"Quantity: {quantity}")
            print(item)
            sys.exit()


        print(f"Normalization needed. {product_quantity} - Normalized quantity {quantity} - {uom}")
        print(f"Price {product_price} - Normalized price - {normalized_price}")
        product_quantity = f"{quantity} {uom}"

        return normalized_price, product_quantity
    else:
        print("no product quantity")
        # print(item)
        # sys.exit()
        return normalized_price, product_quantity

def do_normalize(output):
    normalized_out = []
    for item in output:
        # print(item)
        item = dict(item)
        if not "normalized_price" in item:
            item["normalized_price"] = ""

        if item.get("needs_normalization") == "True":
            print("normalizing")
            try:
                # normalize(item)
                item["normalized_price"], item["quantity"] = normalize(item)

                if not item["normalized_price"]:
                    item["comments"] = f"{item['comments']} \n Cannot normalize."
            except Exception as e:
                print(e)
                item["comments"] = f"{item['comments']} \nError in calcualting normalized price."
                traceback.print_exc()
        
        normalized_out.append(item)
    
    return normalized_out

def writeToFile(output, outdir, outputHeaders):
    if not os.path.isdir(outdir):
        print("Creating output directory.")
        os.mkdir(outdir)

    with open(f"{outdir}/{website}.csv", "w") as wf: 
        print("Writing to CSV file.")   
        csv_writer = csv.DictWriter(wf, outputHeaders)
        csv_writer.writeheader()
        csv_writer.writerows(output) 


if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--OUTDIR', help = "Directory where result CSVs are stored.", default = "./scrapedData/")
    parser.add_argument('--WEBSITE', help = "Column name of the link for corresponding website.", default=None)
    args = parser.parse_args()

    outdir = args.OUTDIR
    website = args.WEBSITE

    with open(f"{outdir}/{website}.csv", "r") as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter = ",")
        jsonb = list(csv_reader)
        # print(jsonb)
        jsonb = do_normalize(jsonb)
        outputHeaders = jsonb[0].keys()
        writeToFile(jsonb, outdir, outputHeaders)

