import argparse, sys, csv, time, re, os, json
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import StaleElementReferenceException

import traceback

def highlight(element):
    driver = element._parent
    def apply_style(s):
        driver.execute_script("arguments[0].setAttribute('style', arguments[1]);",
                              element, s)
    original_style = element.get_attribute('style')
    apply_style("background: yellow; border: 2px solid red;")
    time.sleep(.1)
    apply_style(original_style)
    
def set_location(driver, input_pincode, location_name): 
    DELAY = 20
    
    input_pin_xpath = '//*[@id="__next"]/div/div/div[2]/div/input'
    store_availibility_xpath = '//*[@id="__next"]/div[2]/div/div/div/div'
    select_first_store_xpath = '//*[@id="__next"]/div[2]/div/div/div/div/div[1]/div[2]' # change
    select_first_store_confirm_xpath = '//*[@id="__next"]/div[2]/div/div/div/div/div[2]/div/button[1]'
    selected_location_xpath = '//*[@id="__next"]/div/div/div[1]/div/div[1]/div[1]/div[3]/div/p'
    is_deliverable_xpath = '//*[@id="__next"]/div/div/div[2]/p'
    is_deviverable = ''

    driver.get('https://shop.moreretail.in/location')
    print("Setting location...")

    try:
        pincode_textbox = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, input_pin_xpath)))
        highlight(pincode_textbox)
        pincode_textbox.clear()
        pincode_textbox.send_keys(input_pincode)
        time.sleep(2)
        pincode_textbox.send_keys(Keys.ENTER)
        time.sleep(5)

        try:
            DELAY = 15
            store_availibility_textbox = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, store_availibility_xpath)))
            listed_stores = driver.find_elements(By.XPATH, "//div[starts-with(@class, 'store-selection-popup_units')]")
            for each_store in listed_stores:
                if location_name in each_store.text.lower():
                    highlight(each_store)
                    each_store.click()
                    break
                #print (a.text)
            #print (store_availibility_textbox.text)

            #select_first_store = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, select_first_store_xpath)))
            #highlight(select_first_store)
            #select_first_store.click()

            confirm_store = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, select_first_store_confirm_xpath)))
            highlight(confirm_store)
            confirm_store.click()

        except Exception as err:
            traceback.print_exc()
        
        try:
            is_deviverable = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, is_deliverable_xpath)))
        except Exception as err:
            pass

        DELAY = 20
        
        if not is_deviverable:
            selected_location = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, selected_location_xpath))).get_attribute("innerHTML")
            print (f'\nSelected Location: {selected_location}\n')
            store_id = driver.current_url.split('?store=')[1]
            return selected_location, store_id
            
        elif 'We are currently not delivering at this location' in is_deviverable.text:
            print (f'\nNot delivering at the selected location, Exiting..')
            driver.quit()
            sys.exit()

    except Exception as err:
        traceback.print_exc()
        print (f'Error in Setting Location: {err}\n, Exiting..')
        driver.quit()
        sys.exit()

def read_csv(input_csv, path, website_name, location_name, input_pincode, headless, chromedriver_path):
    website_name = website_name.lower().replace('url', '').strip()
    line_count = 0
    links_count = 0
    is_headless = ''
    is_location_set = False
    is_remove_file = False
    with input_csv:
        csv_reader = csv.reader(input_csv, delimiter=',')
        with open(path+"//"+website_name+"_"+location_name+".csv", 'w', newline='') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(['item_desc','product_name','quantity','price','selling_price', 'brand','website_link', 'comments'])
            for row in csv_reader:
                if line_count == 0:
                    website_name_flag = False
                    item_desc_flag = False
                    unit_flag = False
                    column_index = url_index = item_desc_index = unit_index = 0
                    for column_name in row:
                        if column_name.lower() == f"{website_name} url":
                            url_index = column_index
                            website_name_flag = True

                        elif column_name.lower() == 'item_desc':
                            item_desc_index = column_index
                            item_desc_flag = True
                            
                        elif column_name.lower() == 'uom':
                            unit_index = column_index
                            unit_flag = True
                        
                        if website_name_flag and item_desc_flag and unit_flag:
                            break
                        column_index += 1
                        
                    if not website_name_flag or not item_desc_flag:
                        print (f'\nColumn Header "item_desc" or "{website_name} url" NOT found in {input_csv.name} file, Exiting..')
                        is_remove_file = True
                        break
                else:
                    if not is_location_set:
                        chrome_options = webdriver.ChromeOptions()
                        chrome_options.add_argument("--window-size=1920,1080")
                        chrome_options.add_argument("--start-maximized")
                        chrome_options.add_argument('--ignore-certificate-errors')
                        chrome_options.add_argument('--ignore-ssl-errors')
                        chrome_options.add_argument('log-level=3')
                        
                        if headless:
                            is_headless = 'Yes'
                            chrome_options.add_argument('--headless')
                            chrome_options.add_argument('--no-sandbox')
                            driver = webdriver.Chrome(executable_path = chromedriver_path, options=chrome_options)
                        else:
                            is_headless = 'No'
                            driver = webdriver.Chrome(executable_path = chromedriver_path, options=chrome_options)
                            
                        user_location, store_id = set_location(driver, input_pincode, location_name)
                        is_location_set = True

                    prod_desc = row[item_desc_index]
                    url = row[url_index]
                    if url.startswith('https://') and 'moreretail.in' in url:
                        unit = ''
                        if unit_flag:
                            unit = row[unit_index]

                        product_name, orig_quan, mrp, selling_price, brand, comments = read_moreretail(prod_desc, url, unit, store_id, driver)
                        csvwriter.writerow([prod_desc, product_name, orig_quan, mrp, selling_price, brand, url, comments])
                        links_count += 1
                        time.sleep(2)
                    else:
                        csvwriter.writerow([prod_desc, '', '', '', '', '', url, ''])
                        
                line_count += 1
                csvfile.flush()
            
            if links_count > 0:
                minutes , seconds = divmod((time.time() - start_time), 60)
                print (f'\nInput      : {input_csv.name}')
                print   (f'Output     : {path}\{website_name}.csv')
                print   (f'Website    : More Retail')
                print   (f'Location   : {user_location} - Store ID: {store_id}')
                print   (f'Links      : {links_count}')
                print   (f'Headless   : {is_headless}')
                if int(minutes) > 0:
                    print   (f'Time Taken : {int(minutes)}.{int(seconds)} minutes')
                else:
                    print   (f'Time Taken : {int(seconds)} seconds')
    
    if is_remove_file:
        if os.path.isfile(path+"//"+website_name+"_"+location_name+".csv"):
            os.remove(path+"//"+website_name+"_"+location_name+".csv")
        sys.exit()

    if is_location_set:
        driver.quit()
        
def read_moreretail(prod_desc, url, unit, store_id, driver):
    print (f'{prod_desc}')
    print (f'\t{url}')
    
    units = re.findall(r"[-+]?\d*\.\d+|\d+", unit)
    
    DELAY = 20
    get_json = product_data = product_name = mrp = selling_price = brand = quantity = comments = ''
    
    search_product_xpath = '//*[@id="__next"]/header/div[2]/div/input'
    search_suggestions_xpath = '//*[@id="__next"]/div/section/div'
    
    is_product_in_search = False
    is_required_data_found = False
    
    driver.get(url)
    
    get_json = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.ID, '__NEXT_DATA__'))).get_attribute("innerHTML")
    
    if get_json:
        product_data = json.loads(get_json)
        
        if 'props' in product_data:
            if 'pageProps' in product_data['props']:
                if 'product' in product_data['props']['pageProps']:
                    if 'product_name' in product_data['props']['pageProps']['product']:
                        product_name = product_data['props']['pageProps']['product']['product_name']
                        driver.get('https://shop.moreretail.in/search?store=' + str(store_id))
                        search_product = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, search_product_xpath)))
                        search_product.clear()
                        search_product.send_keys(product_name)

                        start = time.time()
                        while True:
                            search_results = driver.find_elements_by_class_name("search-suggestions_items__3Oqc6")
                            if search_results:
                                for each_search_item in search_results:
                                    try:
                                        if product_name == each_search_item.text:
                                            highlight(each_search_item)
                                            each_search_item.click()
                                            is_product_in_search = True
                                            break
                                    except StaleElementReferenceException:
                                        pass
                            if (time.time() - start) > 20 or is_product_in_search:
                                break
                                    
                        if is_product_in_search:
                            all_listed_products = ''
                            try:
                                all_listed_products = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.CLASS_NAME , "productlist-container_list__b0wLI")))
                            except Exception as err:
                                pass

                            if all_listed_products:
                                listed_products = driver.find_elements_by_class_name("product-card_desc__21IXE")
                                if listed_products:
                                    for each_listed_product in listed_products:
                                        if  product_name == each_listed_product.text:
                                            highlight(each_listed_product)
                                            each_listed_product.click()
                                            get_required_json = ''
                                            try:
                                                get_required_json = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.ID, '__NEXT_DATA__'))).get_attribute("innerHTML")
                                            except Exception as err:
                                                print (f"Error fetching product data: {err}")
                                            if get_required_json:
                                                required_product_data = json.loads(get_required_json)
                                                is_required_data_found = True
                                            break
                                else:
                                    comments = "Product NOT found in selected location"
                        else:
                            comments = "Product NOT found in selected location"
    
    def get_product_data(product_data_json):
        product_name = mrp = selling_price = quantity = comments = ''
        if 'props' in product_data_json:
            if 'pageProps' in product_data_json['props']:
                if 'product' in product_data_json['props']['pageProps']:
                    if 'product_name' in product_data_json['props']['pageProps']['product']:
                        product_name = product_data_json['props']['pageProps']['product']['product_name']
                        if 'units' in product_data_json['props']['pageProps']['product']:
                            if product_data_json['props']['pageProps']['product']['units']:
                                quantities = product_data_json['props']['pageProps']['product']['units']
                                for each_quantity in reversed(quantities):
                                    
                                    mrp = f"{float(each_quantity['price']):g}"
                                    selling_price =  f"{float(each_quantity['special_price']):g}"
                                    quantity = each_quantity['units']
                                    
                                    if units:
                                        units_in_webpage = re.findall(r"[-+]?\d*\.\d+|\d+", each_quantity['units'])
                                        if any(item in units_in_webpage for item in units):
                                            break
        return product_name, mrp, selling_price, quantity, comments

    if is_required_data_found:
        product_name, mrp, selling_price, quantity, comments = get_product_data(required_product_data)

    else:
        if get_json:
            product_name, mrp, selling_price, quantity, comments = get_product_data(product_data)
            comments = ""

    if comments:
        print (f'\t\t{product_name} | {comments}\n')
    else:
        print (f'\t\t{product_name} | {selling_price} | {mrp} | {quantity}\n')

    return product_name, quantity, mrp, selling_price, brand, comments

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', type=argparse.FileType("r"), help="Input CSV file")
    parser.add_argument('-o', '--output', type=str, help="Output Directory")
    parser.add_argument('-w', '--website', type=str, help="CSV Column Name")
    parser.add_argument('-l', '--location', type=str, help="More Retail Location")
    parser.add_argument('-H', '--headless', help="Run Chrome headless or not. Default is True", default=True)
    args = parser.parse_args()
    
    headless = False if args.headless == "False" else True
    
    if sys.platform == 'win32':
        chromedriver_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "chromedriver.exe")
    elif 'linux' in sys.platform:
        chromedriver_path = "/usr/bin/chromedriver"
    else:
        print(f'\nUnknown OS, Exiting..')
        sys.exit()
    
    if os.path.exists(os.path.join(os.path.dirname(os.path.abspath(__file__)), "cities_pincode.csv")):
        check_location = False
        with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), "cities_pincode.csv"), 'r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                city = row[0]
                pincode = row[1]
                if args.location.lower() == city.lower():
                    input_pincode = pincode.strip()
                    check_location = True
                    break
    else:
        print (f'\ncities_pincode.csv NOT found, Exiting..')
        sys.exit()

    if check_location:
        if isinstance(int(input_pincode), int):
            if len(input_pincode) != 6:
                print (f'\nPincode {input_pincode} of {args.location} is not 6 digits, Exiting..')
                sys.exit()
        else:
            print (f'\nPincode {input_pincode} of {args.location} is not valid, Exiting..')
            sys.exit()
    else:
        print (f'\nLocation ({args.location}) NOT found in cities_pincode.csv, Exiting..')
        sys.exit()    

    read_csv(args.input, args.output, args.website, args.location.lower(), input_pincode, headless, chromedriver_path)

if __name__ == "__main__":
    start_time = time.time()
    main()