import requests
import json
import time
def upload_data_to_model(file_path,file_name,server_name='https://test.iamdave.ai'):
    url = "{}/check_csv/spar_data".format(server_name)
    payload={}
    files=[('csv_to_check',(file_name,open(file_path,'rb'),'text/csv'))]
    headers = {
            'Accept': 'application/json, text/plain, */*',
             'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36',
              'X-I2CE-ENTERPRISE-ID': 'spar_insights',
              'X-I2CE-USER-ID': 'ananth+spar_insights@i2ce.in',
              'X-I2CE-API-KEY': 'YW5hbnRoIHNwYXJfaW5zaWdodHNAaTJjZS5pbjE2NzkzOTY0NDcuNTI_'
    }
    print(url)
    response = requests.request("POST", url, headers=headers, data=payload, files=files)
    if response.status_code==200:
        response= response.text
        return response
    else:
        print("API ERROR RESPONSE CODE:: :{}".format(response.status_code))
        print("API ERROR Response :: {} ".format(response.text))
        
def check_status(check_id, server_name="https://test.iamdave.ai"):
    url = "{}/status_check_csv/{}".format(server_name,check_id)
    payload = {}
    headers ={
              'Content-Type': 'application/json',
              'X-I2CE-ENTERPRISE-ID': 'spar_insights',
              'X-I2CE-USER-ID': 'ananth+spar_insights@i2ce.in',
              'X-I2CE-API-KEY': 'YW5hbnRoIHNwYXJfaW5zaWdodHNAaTJjZS5pbjE2NzkzOTY0NDcuNTI_'
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    if response.status_code==200:
        response = response.json()
        return response
    return response



if __name__=="__main__":
    import os
    print(os. getcwd())
    server_name="https://test.iamdave.ai" 
    file_path="/home/bharath/price_crawler/crawlerScripts/latest_data.csv"
    file_name="latest_data.csv"
    check_id = upload_data_to_model(file_path,file_name,server_name)
    print("Check CSV Status code :: {}".format(check_id))
    if check_id:
        completed=False
        while not completed:
            response= check_status(check_id,server_name)
            if isinstance(response, dict):
                completed=response.get("completed",False)
                total_records= response.get("total_records",0)
                validated_records= response.get("validated_records",0)
                print("data uploaded :: {}  out of {}".format(validated_records,total_records))
                time.sleep(30)
            else:
                print(response)
                time.sleep(10)
        if completed:
            print("File uploaded sucessfully")



    

        



    
