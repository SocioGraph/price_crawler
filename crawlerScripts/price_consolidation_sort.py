# -*- coding: utf-8 -*-
import argparse, sys, csv, os, glob, math
import xlsxwriter
import chardet
import pandas as pd
from pandas.api.types import CategoricalDtype
from datetime import datetime

def read_csvs(directory_path, locations):
    csv_files = glob.glob(os.path.join(directory_path, "*.csv"))
    if csv_files:
        all_crawled_prices = {}
        for file in csv_files:
            if os.path.getsize(file) > 0:   # File size > 0
                filename_with_extension = os.path.basename(file)
                filename = filename_with_extension.split('.', 1)[0]
                if '_' in filename:
                    website_name = filename.split("_")[0].lower()
                    file_name_location = filename.split("_")[1].lower()
                    if file_name_location.lower() in locations:
                        
                        with open(file, 'rb') as rawdata:
                            data_encoding = chardet.detect(rawdata.read(100000))
                        df = pd.read_csv(file, delimiter=',', encoding= data_encoding['encoding'])
                        content_in_list = [list(each_row) for each_row in df.values]
                        content_in_list.insert(0, df.columns.to_list())
                        all_crawled_prices[website_name] = content_in_list

        if all_crawled_prices:
            return all_crawled_prices
            
        else:
            print (f'\n[-] No crawled price result CSV files found in {directory_path} for the locations "{", ". join([_.capitalize() for _ in locations])}"')
    else:
        print (f"\n[-] No CSV files found in {directory_path}")
        
def read_spar_price(spar_csv):
    ''' Read Spar price from CSV file and return it as a list'''
    with open(spar_csv, newline='') as f:
        reader = csv.reader(f, delimiter = ';')
        spar_price_list = list(reader)
        return spar_price_list

def create_consolidated(input_file, location, all_crawled_prices, spar_csv):
    spar_mismatch = []
    output_list = []
    products_count = 0
    with open(input_file, 'r') as csvfile:
            csv_reader = csv.reader(csvfile)
        #with open(output_directory+"//consolidated_data_"+locations[0]+ "_" + datetime.now().strftime("%d-%m-%Y_%H_%M_%S") + ".csv", 'w', newline='') as csvfile:
            #csvwriter = csv.writer(csvfile)
            for line_count, input_csv_row in enumerate(csv_reader):
                if line_count == 0:
                    if spar_csv:
                        spar_price_list = read_spar_price(spar_csv)
                        spar_price_header = [x.lower() for x in spar_price_list[0]]
                        spar_code_index = spar_price_header.index('item')
                        spar_price_index = spar_price_header.index('price')
                    
                    input_header = input_csv_row
                    input_header = [y.lower() for y in input_header]

                    if 'grofers url' in input_header and 'blinkit' in all_crawled_prices.keys():
                        all_crawled_prices['grofers'] = all_crawled_prices.pop('blinkit')

                    for each_website in all_crawled_prices.keys():
                        if each_website + ' url' in input_header:
                            pass
                        else:
                            print (f'\n\t[-] Column for "{each_website.title()}" NOT found in the input file "{input_file}"\n')
                    
                    ''' Initilizing output header for price consolidated data '''
                    output_header = ['Division', 'Sub Category', 'ITEM_DESC', 'Item Code', 'UOM', 'spar']
                    output_header.extend(all_crawled_prices.keys())
                    output_list.append(output_header)
                    #csvwriter.writerow(output_header)
                    
                    ''' Writing column number of normalized websites into a dictionary '''
                    normalized_sites = {}
                    for site_name in all_crawled_prices.keys():
                        if site_name + " needs normalization" in input_header:
                            normalized_sites[site_name] = input_header.index(site_name + " needs normalization")
                        elif site_name.startswith('star') and "starquick needs normalization" in input_header:
                            normalized_sites[site_name] = input_header.index("starquick needs normalization")
                        elif site_name.startswith('jio') and "jiomart needs normalization" in input_header:
                            normalized_sites[site_name] = input_header.index("jiomart needs normalization")
                        elif site_name.startswith('more') and "moreretail needs normalization" in input_header:
                            normalized_sites[site_name] = input_header.index("moreretail needs normalization")
                    spar_normalize = [_ for _ in input_header if _.startswith('spar needs')]
                    if spar_normalize:
                        normalized_sites["spar"] = input_header.index(spar_normalize[0])

                    print (f'[+] Normalization column for {len(normalized_sites)} websites found')
                    print (f'    |- {", ". join([c.capitalize() for c in normalized_sites])}')
                    
                    ''' Printing Website names if normalization column not found in product data template '''
                    normalized_not_found = list(set(all_crawled_prices.keys()).difference(normalized_sites.keys()))
                    if normalized_not_found:
                        normalized_not_found = [y.title() for y in normalized_not_found]
                        print (f'\n\t[-] Normalization column NOT found for "{", ". join([c.capitalize() for c in normalized_not_found])}"\n')
                    
                    ''' Initilizing dictionary to get total count of URLs for each competitor website '''
                    links_for_each_website = dict.fromkeys(all_crawled_prices.keys(), 0)

                elif any(input_csv_row):#else:
                    division = input_csv_row[input_header.index('division')]
                    sub_category = input_csv_row[input_header.index('sub category')]
                    item_desc = input_csv_row[input_header.index('item_desc')]
                    item_code = input_csv_row[input_header.index('item code')]
                    uom = input_csv_row[input_header.index('uom')]

                    output_row = [division, sub_category, item_desc, item_code, uom]
                    
                    output_row += (len(output_header)-len(output_row)) * [None]
                    
                    ''' Getting Spar price and apply normalization if applicable '''
                    if spar_csv:
                        for each_spar_item in spar_price_list:
                            spar_code = each_spar_item[spar_code_index]
                            if item_code.strip() == spar_code.strip():
                                spar_price = each_spar_item[spar_price_index]
                                if "spar" in normalized_sites:
                                    spar_normalized_value = input_csv_row[normalized_sites["spar"]]
                                    if spar_normalized_value:
                                        spar_normalized_value = spar_normalized_value.replace('*', '')
                                        if isinstance(float(spar_normalized_value), float):
                                            spar_price = float(each_spar_item[spar_price_index]) * float(spar_normalized_value)

                                output_row[output_header.index('spar')] = spar_price
                                break

                    for column_index, each_item in enumerate(input_csv_row):
                        if each_item.startswith('http'):
                            website_in_header = input_header[column_index]
                            website_in_header = website_in_header.replace('url', '').strip()

                            if website_in_header in all_crawled_prices.keys():
                                if len(all_crawled_prices[website_in_header]) > line_count:
                                    crawled_data_for_each_product = all_crawled_prices[website_in_header][line_count]
                                    product_description_input_csv = crawled_data_for_each_product[0]
                                    product_description_crawled_csv = input_csv_row[input_header.index('item_desc')]
                                    if product_description_input_csv == product_description_crawled_csv:
                                        selling_price = crawled_data_for_each_product[4]    # Selling Price
                                        if not math.isnan(float(selling_price)):
                                            if website_in_header in normalized_sites:
                                                normalized_value = input_csv_row[normalized_sites[website_in_header]]
                                                if normalized_value:
                                                    normalized_value = normalized_value.replace('*', '')
                                                    if isinstance(float(normalized_value), float):
                                                        selling_price = float(selling_price) * float(normalized_value)  # Selling price * normalized value

                                            wesite_index_in_output = output_header.index(website_in_header)
                                            output_row[wesite_index_in_output] = selling_price
                                            links_for_each_website[website_in_header] += 1

                                            #print (line_count, website_in_header, input_csv_row[2], selling_price, product_description_input_csv)
                                    else:
                                        print (f'\t[-] Product description in Row. {line_count+1} of input file "{input_file}" is NOT matching with the description for "{website_in_header}"')
                
                    output_list.append(output_row)
                    #csvwriter.writerow(output_row)
                    products_count += 1
                    #print (output_row)
                    
            print (f'[+] Total {products_count} products in the input file "{input_file}"')
            if spar_mismatch:
                print (f'\n\t[-] Product Item Code in Rows {", ". join([str(i) for i in spar_mismatch])} of Spar price file "{os.path.basename(spar_csv)}" is NOT matching with the description in "{os.path.basename(input_file)}"\n')

            return output_list, links_for_each_website

def excel_sort_colour(output_list, links_for_each_website, output_directory):
    new_list = [item for item in output_list if len(item[0]) != 0]
    #new_list.sort()

    df = pd.DataFrame(new_list[1:], columns=new_list[0])
    df.columns = map(str.title, df.columns)
    
    ''' Getting a list of websites including Spar '''
    spar_column_index = df.columns.get_loc("Spar")
    website_column_names = df.columns[spar_column_index:].tolist()

    ''' Converting Item Code column into float '''
    df['Item Code'] = (df['Item Code'].apply(pd.to_numeric))
    
    ''' Interchanging competitor columns in a given order (desired_column_order) '''
    desired_column_order = ["Bigbasket", "Blinkit", "Spencers", "Star-Quick", "Jio Mart", "More-Retail", "Dmart"]
    unwanted_columns = []
    for each_static_site in desired_column_order:
        if not each_static_site in website_column_names[1:]:
            unwanted_columns.append(each_static_site)
    desired_column_order = [x for x in desired_column_order if x not in unwanted_columns]
    
    for each_site in website_column_names[1:]:
        if not each_site in desired_column_order:
            if "Jio Mart" in desired_column_order:
                desired_column_order.insert(desired_column_order.index("Jio Mart"), each_site)
            elif "More-Retail" in desired_column_order:
                desired_column_order.insert(desired_column_order.index("More-Retail"), each_site)
            elif "Dmart" in desired_column_order:
                desired_column_order.insert(desired_column_order.index("Dmart"), each_site)    
            else:
                desired_column_order.append(each_site)
    
    restructured_column_header = df.columns[:spar_column_index+1].tolist() + desired_column_order
    df = df[restructured_column_header]

    ''' Sorting Sub Category column in a given order (desired_subcategory_sort) '''
    ''' Sorting Spar prices in ascending order '''
    desired_subcategory_sort = ["MH-Vegetables", "MH-Spl Cats - F/V", "MH-Fruits", "Dry Fruits", "Edible Oil / Fats", "Flours", "Grains", "Pulses", "Sugar / Salt", "MH-Fish/Sea Food", "MH-Meat/Poultry"]
    available_sub_catg = df["Sub Category"].unique()
    
    for each_default_catg in desired_subcategory_sort:
        if not each_default_catg in available_sub_catg:
            desired_subcategory_sort.remove(each_default_catg)
    
    for each_available_sub_catg in available_sub_catg:
        if not each_available_sub_catg in desired_subcategory_sort:
            desired_subcategory_sort.append(each_available_sub_catg)

    df["Spar"] = df["Spar"].apply(pd.to_numeric)

    sort_values_type = CategoricalDtype(desired_subcategory_sort, ordered=True)
    df["Sub Category"] = df["Sub Category"].astype(sort_values_type)

    spar_prices_list = df["Spar"].tolist().sort()
    spar_prices_catogery = CategoricalDtype(spar_prices_list, ordered=True)
    df["Spar"] = df["Spar"].astype(spar_prices_catogery)

    df = df.sort_values(["Sub Category", "Spar"])
    
    ''' Converting all prices columns into float '''
    df[website_column_names] = df[website_column_names].apply(pd.to_numeric)
    df[website_column_names] = df[website_column_names].round(2)

    ''' Finding if Spar price greater than competitor price '''
    df['Max Priced Value'] = df[website_column_names[1:]].max(axis=1)
    df['Max Priced Value'] = df['Max Priced Value'].fillna(df['Spar']-1)    # Filling nan with one value less than Spar for highlighting if no competitor prices present

    df['Max Priced Seller'] = df['Spar'] > df['Max Priced Value']
    df.drop('Max Priced Value', inplace=True, axis=1)
    
    df = df.reset_index(drop=True)
    
    ''' Writing sorted dataframe into Excel '''
    writer = pd.ExcelWriter(os.path.join(output_directory, 'Price_Benchmark_' + datetime.now().strftime("%d-%b-%Y %I.%M %p") + '.xlsx'), engine='xlsxwriter')
    df.to_excel(writer, sheet_name='Price', columns = df.columns[:-1], startrow = 1, index=False)
    
    workbook  = writer.book
    worksheet = writer.sheets['Price']
    
    def colnum_string(n):
        ''' Function to convert column number into Excel letter '''
        string = ""
        while n > 0:
            n, remainder = divmod(n - 1, 26)
            string = chr(65 + remainder) + string
        return string
    
    ''' Getting Excel letters of Spar column and last column '''
    spar_col_letter = colnum_string(spar_column_index+1)
    last_col_letter = colnum_string(len(df.columns)-1)
    
    ''' Adding Header (First row) of excel '''
    merge_format = workbook.add_format({'align': 'center'})
    worksheet.merge_range('A1:'+last_col_letter+'1', 'Price Benchmark ' + datetime.now().strftime("%d-%b-%Y %I:%M %p"), merge_format)

    ''' All Border properties to cells '''
    all_borders = workbook.add_format({'border':1})
    worksheet.conditional_format(xlsxwriter.utility.xl_range(0, 0, len(df)+1, len(df.columns)-2), {'type': 'no_errors', 'format': all_borders})
    
    ''' Highlighting Higher Spar price cells with light orange colour '''
    light_orange = workbook.add_format({'bg_color': '#FFC000'})
    #for spar_high_index in df.index[df['Max Priced Seller'] == 'Spar'].tolist():
    for spar_high_index in df.index[df['Max Priced Seller'] == True].tolist():
        spar_col_cell = spar_col_letter + str(spar_high_index + 3)
        criteria = '=' + spar_col_cell + '>0'
        worksheet.conditional_format(spar_col_cell, {'type': 'formula', 'criteria': criteria, 'format': light_orange})

    ''' Autofit columns width '''
    for column in df:
        column_width = max(df[column].astype(str).map(len).max(), len(column))
        col_idx = df.columns.get_loc(column)
        writer.sheets['Price'].set_column(col_idx, col_idx, column_width)

    writer.save()
    
    print (f'[+] Output saved to Excel file')
    for key, value in links_for_each_website.items():
        print (f'    |- {key.title().ljust(12)} - {str(value)}')
    print (f"\nOutput: {os.path.join(output_directory, 'Price_Benchmark_' + datetime.now().strftime('%d-%b-%Y %I.%M %p') + '.xlsx')}\n")
    
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', required=True, help="Input CSV file")
    parser.add_argument('-d', '--directory', required=True, help="Directory path containing crawled price CSVs")
    parser.add_argument('-l', '--location', type=str, required=True, action='append', nargs='+', help="Locations of crawled websites (Eg: Bangalore, Delhi)")
    parser.add_argument('-o', '--output', help="Output Directory")
    parser.add_argument('-s', '--spar', help="Spar Price CSV")

    if len(sys.argv)==1:
        parser.print_help(sys.stderr)
        sys.exit()

    args = parser.parse_args()

    if not args.input:
        parser.print_help(sys.stderr)
        print (f'\nError: Provide Input CSV file')
        sys.exit()

    if args.directory:
        if not os.path.isdir(args.directory):
            print (f'\nError: Input Directory Not Found, Exiting..')
            sys.exit()
    else:
        parser.print_help(sys.stderr)
        print (f'\nError: Provide Input Directory path, Exiting..')
        sys.exit()
    
    if not args.location:
        parser.print_help(sys.stderr)
        print (f'\nError: Provide Location of crawled websites, Exiting..')
        sys.exit()
    else:
        locations = [item.lower() for sublist in args.location for item in sublist]

    if args.output:
        output_directory = args.output
        if not os.path.isdir(output_directory):
            print (f'\nError: Output Directory Not Found, Exiting..')
            sys.exit()
    else:
        output_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)))

    if args.spar:
        spar_csv = args.spar
    else:
        spar_csv = False

    all_crawled_prices = read_csvs(args.directory, locations)
    if all_crawled_prices:
        print (f'\nInput: {args.input}')
        if spar_csv:
            print (f'       {spar_csv}')
        print (f'\n[+] {len(all_crawled_prices)} CSV files found matching the locations: "{", ". join([_.capitalize() for _ in locations])}"\n    |- {", ". join([c.capitalize() for c in all_crawled_prices])}')
        output_list, links_for_each_website = create_consolidated(args.input, locations[0], all_crawled_prices, spar_csv)
        excel_sort_colour(output_list, links_for_each_website, output_directory)

if __name__ == "__main__":
    main()

