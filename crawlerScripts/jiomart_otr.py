import sys, time
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains

from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

import logging, traceback
logging.getLogger('seleniumwire.proxy').setLevel(logging.FATAL)

from bs4 import BeautifulSoup

DELAY = 20

chrome_options = webdriver.ChromeOptions()
#chrome_options.add_argument("--window-size=1920,1080")
#chrome_options.add_argument('--headless')
chrome_options.add_argument("--start-maximized")
chrome_options.add_argument('--ignore-certificate-errors')
chrome_options.add_argument('--ignore-ssl-errors')
chrome_options.add_argument('log-level=3')

driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)

def highlight(element):
    driver = element._parent
    def apply_style(s):
        driver.execute_script("arguments[0].setAttribute('style', arguments[1]);",
                              element, s)
    original_style = element.get_attribute('style')
    apply_style("background: yellow; border: 2px solid red;")
    time.sleep(.01)
    apply_style(original_style)

def take_screenshot(save_as):
    #print(f"Taking Screenshot.. -> {save_as}")
    driver.get_screenshot_as_file(save_as)

def set_location(input_pincode): 
    driver.get("https://www.jiomart.com")
    
    try:
        select_location = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.ID, 'btn_delivery')))
        highlight(select_location)
        select_location.click()
        
        select_pincode = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.ID, 'btn_enter_pincode')))
        highlight(select_pincode)
        select_pincode.click()
        
        pincode_textbox = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.ID, 'rel_pincode')))
        highlight(pincode_textbox)
        pincode_textbox.click()
        pincode_textbox.clear()
        pincode_textbox.send_keys(input_pincode)
        
        submit_pincode = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.ID, 'btn_pincode_submit')))
        highlight(submit_pincode)
        submit_pincode.click()

        selected_location = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.ID, 'delivery_city_pincode')))
        highlight(selected_location)
        return selected_location.text
    except Exception as err:
        print (f'Error: {err}\n, Exiting..')
        traceback.print_exc()
        driver.quit()
        sys.exit()

def read_jiomart(url):
    DELAY = 20
    #print (f'{prod_desc}')
    print (f'\t{url}')
    product_name = orig_quan = mrp = selling_price = brand = comments = None
    product_highlight_html = description_html = brand = manufacturer = manufacturer_address = manufacturer_email = manufacturer_website = sold_by = cus_care_email = cus_care_phone = origin = quantity = net_quantity = net_weight = table_data = image_urls = screenshot = article_id = None
    
    driver.get(url)
    page_source = driver.page_source
    #print (page_source)
    if 'Products are currently not available at the selected PIN code' not in page_source:

        brand_xpath = '/html/body/main/section/section[2]/div[1]/div/div[2]/div/section[1]/div[1]'
        brand_element = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, brand_xpath)))
        brand = brand_element.text
        highlight(brand_element)
        product_name_xpath = '//*[@id="pdp_product_name"]'
        product_name_element = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, product_name_xpath)))
        product_name = product_name_element.text
        highlight(product_name_element)
        prices_xpath = '//*[@id="price_section"]'
        price_element = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, prices_xpath)))
        prices = price_element.text.split()

        buttons = WebDriverWait(driver, DELAY).until(EC.presence_of_all_elements_located((By.XPATH, "//button[text()='More Details']")))

        for button in buttons:
            try:
                button.click()
            except:
                #traceback.print_exc()
                pass
        DELAY = 3
        try:
            product_highlight_xpath = '/html/body/main/section/section[2]/div/div/div[2]/section[6]/div[2]/ul'
            product_highlight_1 = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.CLASS_NAME, 'product-key-features-list')))
            highlight(product_highlight_1)
            product_highlight_html = product_highlight_1.get_attribute("outerHTML")
        except Exception as err:
            pass
        
        try:
            description_xpath = '//*[@id="pdp_description"]'
            description_1 = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, description_xpath)))
            highlight(description_1)
            description = description_1.text#.split('\n')[1]
            description_html_raw = description_1.get_attribute("outerHTML")
            description_soup = BeautifulSoup(description_html_raw, 'html.parser')
            
            for element in description_soup.find_all(class_="jm-btn"):
                element.extract()

            for paragraph in description_soup.find_all('p'):
                if paragraph.find('b', text='Disclaimer:'):
                    paragraph.extract()

            description_html = str(description_soup)
        except Exception as err:
            pass
        
        images_xpath = "/html/body/main/section/section[2]/div/div/div[1]/div/div[1]/div[1]/div[1]"
        if '491093909' in url:
            images_xpath = "/html/body/main/section/section[2]/div/div/div[1]/div/div[1]/div[1]/div[1]/divv"
        if '603722466' in url:
            images_xpath = "/html/body/main/section/section[2]/div/div/div[1]/div/div[1]/div[1]/div[1]/divv"
        images = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, images_xpath)))
        highlight(images)
        images_html = images.get_attribute("outerHTML")

        soup = BeautifulSoup(images_html, 'html.parser')

        img_tags = soup.find_all('img')

        image_urls = []
        for img_tag in img_tags:
            image_url = img_tag.get('src')
            if image_url.startswith('https://www.jiomart.com'):
                if ".jpg" in image_url:
                    image_url = image_url.split(".jpg")[0] + ".jpg"
                elif ".png" in image_url:
                    image_url = image_url.split(".png")[0] + ".png"
                if ".svg" not in image_url:
                    image_urls.append(image_url)

        
        full_product_info_xpath = '//*[@id="pdp_product_information"]'
        try:
            try:
                full_product_info = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, full_product_info_xpath)))
            except Exception as err:
                full_product_info_xpath = '//*[@id="pdp_tech_specifications"]'
                full_product_info = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, full_product_info_xpath)))
        
            element_source = (full_product_info.get_attribute("outerHTML"))

            soup = BeautifulSoup(element_source, 'html.parser')

            div_element = soup.find('div', class_='product-specifications-wrapper', id='pdp_product_information')

            table_data = {}

            tables = div_element.find_all('table')

            for table in tables:
                rows = table.find_all('tr')
                for row in rows:
                    columns = row.find_all(['th', 'td'])
                    if len(columns) == 2:
                        key = columns[0].get_text(strip=True)
                        value = columns[1].get_text(strip=True)
                        table_data[key] = value

            brand = table_data.get('Brand')
            manufacturer = table_data.get('Manufacturer')
            manufacturer_address = table_data.get('Manufacturer Address')
            manufacturer_email = table_data.get('Manufacturer Email')
            manufacturer_website = table_data.get('Manufacturer Website')
            sold_by = table_data.get('Sold By')
            cus_care_email = table_data.get('JioMart Customer Care Email')
            cus_care_phone = table_data.get('JioMart Customer Care Phone')
            origin = table_data.get('Country of Origin')
            quantity = table_data.get('Quantity')
            net_quantity = table_data.get('Net Quantity')
            net_weight = table_data.get('Net Weight')
        
        except Exception as err:
            pass

        keyword = 'Article ID: '
        article_id_raw = driver.find_element("xpath", f'//*[contains(text(), "{keyword}")]')
        article_id = article_id_raw.text.replace(keyword, '')   
           
        if prices:
            if price_element.text.startswith('M.R.P:'):
                selling_price = prices[1].replace('₹', '').replace(',', '').replace('.00', '')
                highlight(price_element)
            else:
                selling_price_xpath = '//*[@id="price_section"]/div[1]/span[1]'
                selling_price_element = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, selling_price_xpath)))
                selling_price = selling_price_element.text.replace('₹', '').replace(',', '').replace('.00', '')
                highlight(selling_price_element)
                mrp_xpath = '//*[@id="price_section"]/div[2]/span[1]'
                mrp_element = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, mrp_xpath)))
                mrp = mrp_element.text.replace('₹', '').replace(',', '').replace('.00', '')
                highlight(mrp_element)
        else:
            comments = 'Product not available at the selected PIN code'
    else:
        comments = 'Product not available at the selected PIN code'
    
    '''
    if comments:
        print (f'\t\t{comments}\n')
    else:
        print (f'\t\t{product_name} | {brand} | {selling_price} | {mrp}\n')
    
    if product_name:      
        try:
            buttons = WebDriverWait(driver, DELAY).until(EC.presence_of_all_elements_located((By.XPATH, "//button[text()='More Details']")))

            for button in buttons:
                try:
                    button.click()
                except:
                    traceback.print_exc()
                    pass
        except:
            pass
        #height = driver.execute_script('return document.documentElement.scrollHeight')
        #width  = driver.execute_script('return document.documentElement.scrollWidth')
        
        #driver.set_window_size(width, height)  # the trick
        
        #time.sleep(2)
        screenshot = f"{product_name}_{int(time.time())}.png"
        #driver.save_screenshot(screenshot)
    '''
    return {'Jio Product Title': product_name,
            'Jio Product Highlights': product_highlight_html,
            'Jio Product Description': description_html,
            'Jio Product Information': None,
            'Jio Brand': brand,
            'Jio Manufacturer': manufacturer,
            'Jio Manufacturer Address': manufacturer_address,
            'Jio Manufacturer Email': manufacturer_email,
            'Jio Manufacturer Website': manufacturer_website,
            'Jio Sold By': sold_by,
            'Jio Customer Care Email': cus_care_email,
            'Jio Customer Care Phone': cus_care_phone,
            'Jio Food Type': None,
            'Jio Country of Origin': origin,
            'Jio Quantity': quantity,
            'Jio Net Quantity': net_quantity,
            'Jio Net Weight': net_weight,
            'JioMart URL': None,
            'Comments': comments,
            'Jio Image URLs': image_urls,
            'Screenshot': screenshot,
            'table_data': table_data,
            'Jio Article ID': article_id
            }
            
    #return product_name, orig_quan, mrp, selling_price, brand, comments

def infinite_scroll(Y=8000, times = 10):
    while times > 0:
        print(f"Scrolling {times}")
        driver.execute_script(f"window.scrollTo(0, 5000)")
        times = times - 1
        time.sleep(5)
        driver.execute_script(f"window.scrollTo(0, document.body.scrollHeight)")
        time.sleep(5)

def scrollWebPage(page_num):
    print(f"Scrolling page {page_num}...")
    #driver.execute_script(f"window.scrollTo(0, 5000)")
    #time.sleep(5)
    #driver.execute_script(f"window.scrollTo(0, document.body.scrollHeight)")
    #time.sleep(5)
    time.sleep(2)
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(5)

def crawl(search_result_xpath):
    search_result = WebDriverWait(driver, DELAY).until(EC.element_to_be_clickable((By.XPATH, search_result_xpath)))
    highlight(search_result)
    return BeautifulSoup(search_result.get_attribute("outerHTML"), 'html.parser')

def search_products(product_title, filtered_url = None, first_product = True, scroll = False, infinite_scroll = False, no_of_scrolls=30):
    title = url = screenshot_file = None
    if filtered_url:
        driver.get(filtered_url)
    else:
        driver.get(f"https://www.jiomart.com/search/{product_title}")

    if infinite_scroll:
        try:
            infinite_scroll()
        except Exception as e:
            print(e)
            traceback.print_exc()

    search_results_xpath = '//*[@id="algolia_hits"]/div'
    pd_list =[]
    while True:
        pd = {}
        all_pd_list = []
        search_results = WebDriverWait(driver, DELAY).until(EC.element_to_be_clickable((By.XPATH, search_results_xpath)))
        highlight(search_results)
        if 'jiomart_loading' not in search_results.get_attribute("outerHTML"):
            if 'Sorry! No product found' in search_results.get_attribute("outerHTML"):
                url = 'No product found'
                #print("break1")
                break

            if first_product:
                search_result_xpath = '//*[@id="algolia_hits"]/div/ol/li[1]'
            else:
                search_result_xpath = '//*[@id="algolia_hits"]/div/ol'
            
            #print(search_result_xpath)
            if scroll:
                previous_url = current_url = None
                page_num = 1
                while no_of_scrolls > 0:
                    soup = crawl(search_result_xpath) #WebDriverWait(driver, DELAY).until(EC.element_to_be_clickable((By.XPATH, search_result_xpath)))
                    #print (len(soup.findAll('li')))
                    all_pd_list.extend(soup.findAll('li'))
                    no_of_scrolls =  no_of_scrolls -1
                    scrollWebPage(page_num)
                    page_num += 1
                    
                    current_url = driver.current_url
                    #print (f"Current: {current_url}")
                    if current_url == previous_url:
                        #print (f"Break: {previous_url}")
                        #time.sleep(300)
                        break
                    else:
                        previous_url = current_url
                    
            else:
                soup = crawl(search_result_xpath)
                all_pd_list = soup.findAll('li')

  
            #print(all_pd_list)
            all_pd_list = list(dict.fromkeys(all_pd_list))
            #print(len(all_pd_list))
            for ele in all_pd_list:
                link_element = ele.find('a')
                title = link_element.get('title')
                href = link_element.get('href')
                url = f"https://www.jiomart.com{href}"
                #screenshot_file = f"{product_title.replace('/', '')}.png"
                #take_screenshot(screenshot_file)
                pd = {
                    'title': title,
                    'url': url
                    # 'screenshot': screenshot_file
                    }
                if pd not in pd_list:
                    pd_list.append(pd)
            break

    return pd_list
