import argparse, sys, csv, time, re, os, datetime, json, requests
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

import traceback

from urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

import logging
logging.getLogger('seleniumwire.proxy').setLevel(logging.FATAL)

DELAY = 20

def highlight(element):
    driver = element._parent
    def apply_style(s):
        driver.execute_script("arguments[0].setAttribute('style', arguments[1]);",
                              element, s)
    original_style = element.get_attribute('style')
    apply_style("background: yellow; border: 2px solid red;")
    time.sleep(.01)
    apply_style(original_style)

def take_screenshot(driver, save_as):
    print(f"Taking Screenshot.. -> {save_as}")
    driver.get_screenshot_as_file(save_as)

def check_pincode(input_pincode):
    response = requests.get(f'https://www.jiomart.com/mst/rest/v1/5/pin/{input_pincode}', verify=False)
    if response.status_code == 200:
        pass
    elif response.status_code == 400:
        response = response.json()
        if response.get('status') == 'fail':
            print (f'\nThe pincode {input_pincode} is invalid.\n')
            sys.exit()
    else:
        print (f'\nStatus Code: {response.status_code}\n')
        sys.exit()

def set_location(driver, input_pincode): 
    driver.get("https://www.jiomart.com")
    
    try:
        select_location = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.ID, 'btn_delivery')))
        highlight(select_location)
        select_location.click()
        
        select_pincode = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.ID, 'btn_enter_pincode')))
        highlight(select_pincode)
        select_pincode.click()
        
        pincode_textbox = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.ID, 'rel_pincode')))
        highlight(pincode_textbox)
        pincode_textbox.click()
        pincode_textbox.clear()
        pincode_textbox.send_keys(input_pincode)
        
        submit_pincode = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.ID, 'btn_pincode_submit')))
        highlight(submit_pincode)
        submit_pincode.click()

        selected_location = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.ID, 'delivery_city_pincode')))
        highlight(selected_location)
        return selected_location.text
    except Exception as err:
        print (f'Error: {err}\n, Exiting..')
        driver.quit()
        sys.exit()
   
def read_csv(input_csv, path, website_name, input_pincode, headless, filename):
    website_name = website_name.lower().replace('url', '').strip()
    links_count = 0
    is_headless = ''
    is_location_set = False
    is_remove_file = False
    fileame_with_dir = path+"//"+filename+"_out.csv"
    with input_csv:
        csv_reader = csv.reader(input_csv, delimiter=',')
        with open(fileame_with_dir, 'w', newline='', encoding='utf-8') as csvfile:
            csvwriter = csv.writer(csvfile)
            for line_count, row in enumerate(csv_reader):
                if line_count == 0:
                    website_name_flag   = False
                    item_desc_flag      = False
                    
                    column_name         = [_.lower() for _ in row]
                    
                    region_index        = column_name.index("region")
                    category_index      = column_name.index("category")
                    sub_category_index  = column_name.index("sub_category")
                    item_code_index     = column_name.index("item code")
                    std_uom_index       = column_name.index("standard_uom")
                    sku_type_index      = column_name.index("sku type")
                    url_norm_index      = column_name.index(f"{website_name.lower()} needs normalization")
                    store_name_index    = column_name.index("store name")
                    store_code_index    = column_name.index("store code")
                    pincode_index       = column_name.index("pincode")
                    competitor_index    = column_name.index("competitor")
                    type_index          = column_name.index("type")
                    spar_price_index    = column_name.index("spar price")
                    margin_index        = column_name.index("margin")
                    
                    url_index           = column_name.index(f"{website_name.lower()} url")
                    item_desc_index     = column_name.index("item_desc")
                    uom_index           = column_name.index("uom")
                    
                else:
                    if not is_location_set:
                        chrome_options = webdriver.ChromeOptions()
                        chrome_options.add_argument("--window-size=1920,1080")
                        chrome_options.add_argument("--start-maximized")
                        chrome_options.add_argument('--ignore-certificate-errors')
                        chrome_options.add_argument('--ignore-ssl-errors')
                        chrome_options.add_argument('log-level=3')
                        
                        if headless:
                            is_headless = 'Yes'
                            chrome_options.add_argument('--headless')
                            chrome_options.add_argument('--no-sandbox')
                            driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)
                        else:
                            is_headless = 'No'
                            driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)
                            
                        user_location = set_location(driver, input_pincode)
                        is_location_set = True
                        csvwriter.writerow(['ITEM_DESC','PRODUCT_NAME','QUANTITY','PRICE','SELLING_PRICE', 'BRAND','WEBSITE_LINK', 'Comments', 'REGION', 'Category', 'Sub_Category', 'ITEM CODE', 'Standard_UOM', 'UOM', 'SKU TYPE', 'Type', 'Normalization', 'Store Name', 'Store Code', 'Pincode', 'Competitor', 'Spar Price', 'Margin'])

                    prod_desc = row[item_desc_index]
                    url = row[url_index]
                    if url.startswith('https://www.jiomart.com/p'):
                            product_name, orig_quan, mrp, selling_price, brand, comments = read_jiomart(prod_desc, url, driver)
                            csvwriter.writerow([prod_desc, product_name, orig_quan, mrp, selling_price, brand, url, comments, row[region_index], row[category_index], row[sub_category_index], row[item_code_index], row[std_uom_index], row[uom_index], row[sku_type_index], row[type_index], row[url_norm_index], row[store_name_index], row[store_code_index], row[pincode_index], row[competitor_index], row[spar_price_index], row[margin_index]])
                            links_count += 1
                        
                    else:
                        csvwriter.writerow([prod_desc, '', '', '', '', '', url, '', row[region_index], row[category_index], row[sub_category_index], row[item_code_index], row[std_uom_index], row[uom_index], row[sku_type_index], row[type_index], row[url_norm_index], row[store_name_index], row[store_code_index], row[pincode_index], row[competitor_index], row[spar_price_index], row[margin_index]])

                csvfile.flush()
                
            if links_count > 0:
                minutes , seconds = divmod((time.time() - start_time), 60)
                print (f'\nInput      : {input_csv.name}')
                print   (f'Output     : {fileame_with_dir}')
                print   (f'Website    : JioMart')
                print   (f'Location   : {user_location}')
                print   (f'Links      : {links_count}')
                print   (f'Headless   : {is_headless}')
                if int(minutes) > 0:
                    print   (f'Time Taken : {int(minutes)}.{int(seconds)} minutes')
                else:
                    print   (f'Time Taken : {int(seconds)} seconds')
            else:
                is_remove_file = True
                print (f'\nNo JioMart URLs found in the input CSV file')
    
    if is_remove_file:
        if os.path.isfile(fileame_with_dir):
            os.remove(fileame_with_dir)
        sys.exit()

    if is_location_set:
        driver.quit()
        
def read_jiomart(prod_desc, url, driver):
    print (f'{prod_desc}')
    print (f'\t{url}')
    product_name = orig_quan = mrp = selling_price = brand = comments = ''
    
    driver.get(url)

    page_source = driver.page_source
    if 'Products are currently not available at the selected PIN code' not in page_source:

        brand_xpath = '/html/body/main/section/section[2]/div[1]/div/div[2]/div/section[1]/div[1]'
        brand_element = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, brand_xpath)))
        brand = brand_element.text
        highlight(brand_element)
        product_name_xpath = '//*[@id="pdp_product_name"]'
        product_name_element = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, product_name_xpath)))
        product_name = product_name_element.text
        highlight(product_name_element)
        prices_xpath = '//*[@id="price_section"]'
        price_element = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, prices_xpath)))
        prices = price_element.text.split()
        if prices:
            if price_element.text.startswith('M.R.P:'):
                selling_price = prices[1].replace('₹', '').replace(',', '').replace('.00', '')
                highlight(price_element)
            else:
                selling_price_xpath = '//*[@id="price_section"]/div[1]/span[1]'
                selling_price_element = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, selling_price_xpath)))
                selling_price = selling_price_element.text.replace('₹', '').replace(',', '').replace('.00', '')
                highlight(selling_price_element)
                mrp_xpath = '//*[@id="price_section"]/div[2]/span[1]'
                mrp_element = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, mrp_xpath)))
                mrp = mrp_element.text.replace('₹', '').replace(',', '').replace('.00', '')
                highlight(mrp_element)
        else:
            comments = 'Product not available at the selected PIN code'
            #take_screenshot(driver, f'{prod_desc}.png')
    else:
        comments = 'Product not available at the selected PIN code'
    
    if comments:
        print (f'\t\t{comments}\n')
    else:
        print (f'\t\t{product_name} | {brand} | {selling_price} | {mrp}\n')
    
    return product_name, orig_quan, mrp, selling_price, brand, comments

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input',    type=argparse.FileType("r"), help="Input CSV file")
    parser.add_argument('-o', '--output',   type=str, help="Output Directory")
    parser.add_argument('-w', '--website',  type=str, help="CSV Column Name")
    parser.add_argument('-l', '--location', type=str, help="JioMart Location")
    parser.add_argument('-H', '--headless', help="Run Chrome headless or not. Default is True", default=True)
    args = parser.parse_args()

    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit()
        
    if not args.input:
        parser.print_help(sys.stderr)
        print (f'\nError: Provide Input CSV file')
        sys.exit()
    if not args.output:
        parser.print_help(sys.stderr)
        print (f'\nError: Provide output Directory')
        sys.exit()
    if not args.website:
        parser.print_help(sys.stderr)
        print (f'\nError: Provide JioMart column name from CSV')
        sys.exit()
    if not args.location:
        parser.print_help(sys.stderr)
        print (f'\nError: Provide JioMart Location')
        sys.exit()
    headless = False if args.headless == "False" else True
    
    filename_with_extension = os.path.basename(args.input.name)
    filename = os.path.splitext(filename_with_extension)[0]
    
    check_pincode(args.location.lower().strip())
    read_csv(args.input, args.output, args.website, args.location.lower().strip(), headless, filename)

if __name__ == "__main__":
    start_time = time.time()
    main()