# -*- coding: utf-8 -*-
import argparse, sys, csv, time, re, json, random, datetime, os
from seleniumwire import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from bs4 import BeautifulSoup
import codecs
import logging
logging.getLogger('seleniumwire.proxy').setLevel(logging.FATAL)

chromedriver_path = None

def highlight(element):
    driver = element._parent
    def apply_style(s):
        driver.execute_script("arguments[0].setAttribute('style', arguments[1]);",
                              element, s)
    original_style = element.get_attribute('style')
    apply_style("background: yellow; border: 2px solid red;")
    time.sleep(.5)
    apply_style(original_style)

def set_location(driver, inputlocation): 

    DELAY = 300
    location_placeholder = "//input[@placeholder='Type your city Society/Colony/Area']"
    non_servicable = '//*[@id="app"]/div/div[2]/div[2]/header/div[2]/div[2]/div/div/div[2]'
    location_selected = '//*[@id="app"]/div/div[2]/div[2]/header/div[1]/a/div[2]'
    location_selected = '//*[@id="app"]/div/div[2]/div[2]/header/div[1]/a/div/div/div[2]/div[2]'
    #//*[@id="app"]/div/div[2]/div[2]/header/div[1]/a/div/div/div[2]/div[2]/text()
    
    driver.get("https://www.grofers.com")    
    print("Setting location...")
    
    placeholder = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, location_placeholder)))
    highlight(placeholder)
    placeholder.send_keys(inputlocation) 
    time.sleep(2)
    placeholder.send_keys(' ')    
    time.sleep(2)

    all_locations = driver.find_elements_by_class_name("pac-matched")
    if all_locations:
        first_location = all_locations[0]
        highlight(first_location)
        first_location.click()
    else:
        print ('Location NOT found')
        driver.quit()
        sys.exit()
        
    DELAY = 120
    try:
        check_servicecable = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, non_servicable)))  
        if 'We are not yet in' in check_servicecable.get_attribute("innerHTML"):
            print ('No Delivery in specified Location.')
            driver.quit()
            sys.exit()
        else:
            user_location = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.XPATH, location_selected)))
            selected_location = user_location.text.replace('\nSorry, we are temporarily unavailable', '').split('delivery')[0].strip()
            print (f'\nSelected Location: {selected_location}\n')
            time.sleep(5)
            return selected_location
          
    except Exception as err:
        print ('Error: ' + str(err))
    
def read_csv(input_csv, path, website_name, inputlocation, headless, crawl_time):
    with input_csv:
        csv_reader = csv.reader(input_csv, delimiter=',')
        print (f"{path}//{website_name}.csv")
        with open(path+"//"+website_name+".csv", 'w', encoding='utf-8-sig', newline='') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(['ITEM_DESC','PRODUCT_NAME','QUANTITY','PRICE','SELLING_PRICE', 'BRAND','WEBSITE_LINK', 'Comments'])
            line_count = 0
            links_count = 0
            is_headless = 'No'
            for row in csv_reader:
                if line_count == 0:
                    website_name_flag = False
                    item_desc_flag = False
                    column_index = 0
                    url_index = 0
                    item_desc_index = 0
                    for column_name in row:
                        #print (column_name.lower(), f"{website_name.lower()} url")
                        if column_name.lower() == f"{website_name.lower()} url":
                            website_name_flag = True
                            url_index = column_index
                            chrome_options = webdriver.ChromeOptions()
                            chrome_options.add_argument("--window-size=1920,1080")
                            chrome_options.add_argument("--start-maximized")
                            # chrome_options.add_argument('--ignore-certificate-errors')
                            # chrome_options.add_argument('--ignore-ssl-errors')
                            chrome_options.add_argument('log-level=3')
                            options_wire = {'disable_encoding': True}
                            if headless:
                                is_headless = 'Yes'
                                chrome_options.add_argument('--headless')
                                chrome_options.add_argument('--no-sandbox')
                                driver = webdriver.Chrome(executable_path = chromedriver_path, options=chrome_options, seleniumwire_options=options_wire)
                            else:
                                chrome_options.add_argument('--no-sandbox')
                                driver = webdriver.Chrome(executable_path = chromedriver_path, options=chrome_options, seleniumwire_options=options_wire)
                            user_location = set_location(driver, inputlocation)
                        
                        elif column_name.lower() == 'item_desc':
                            item_desc_index = column_index
                            item_desc_flag = True
                        
                        if website_name_flag and item_desc_flag:
                            break
                            
                        column_index += 1
                        
                    if not website_name_flag:
                        print ("No matching website column name found")
                        sys.exit()
                else:
                    prod_desc = row[item_desc_index]
                    if row[url_index].startswith('https://grofers.com/') and '/prid/' in row[url_index]:
                        units = re.findall(r"[-+]?\d*\.\d+|\d+", prod_desc)
                        units.sort()
                        product_name, orig_quan, mrp, selling_price, brand, comments = read_grofers(prod_desc, row[url_index], units, driver, inputlocation,crawl_time)
                        csvwriter.writerow([prod_desc, product_name, orig_quan, mrp, selling_price, brand, row[url_index], comments])
                        links_count += 1
                        sleep_time = random.randint(1, 10)
                        print (f'\t      Sleeping {sleep_time} sec')
                        time.sleep(sleep_time)
                    else:
                        csvwriter.writerow([prod_desc, '', '', '', '', '', row[url_index], ''])
                line_count += 1
                csvfile.flush()
            if links_count > 0:
                minutes , seconds = divmod((time.time() - start_time), 60)
                print (f'\nInput      : {input_csv.name}')
                print   (f'Output     : {path}\{website_name}.csv')
                print   (f'Website    : Grofers')
                print   (f'Location   : {user_location}')
                print   (f'Links      : {links_count}')
                print   (f'Headless   : {is_headless}')
                print   (f'Time Taken : {int(minutes)}.{int(seconds)} minutes')
            driver.quit()

def read_grofers(prod_desc, url, units, driver, inputlocation, crawl_time):
    product_name = orig_quan = mrp = selling_price = brand = comments = ''  
    product_id = int(re.search(r'\d+', url.split('/prid/')[1]).group())
    print (prod_desc)
    print ('\t' + url)
    
    screenshots_dir = f"./screenshots/{inputlocation}/{crawl_time}"
    if not os.path.isdir(screenshots_dir):
        os.makedirs(screenshots_dir, exist_ok=True)

    i = 0
    while i < 3:
        json_response = visit_url(driver, url, product_id, screenshots_dir)
        if json_response is not None:
            break
        else:
            time.sleep(10)
            i += 1
    
    if json_response is not None:
        if 'product_details' in json_response:
            if 'success' in json_response['product_details']:
                if json_response['product_details']['success'] == False:
                    comments = 'Product not found'
        else:
            if 'data' in json_response:
                if 'product' in json_response['data']:

                    if 'name' in json_response['data']['product']:
                        product_name = json_response['data']['product']['name']
                        
                    if 'unit' in json_response['data']['product']:
                        orig_quan = json_response['data']['product']['unit']
                        orig_quan_num = re.findall(r"[-+]?\d*\.\d+|\d+", orig_quan)
                        orig_quan_num.sort()
                    
                    check_variants_present = False
                    search_unit_flag = False
                    if 'variants_info' in json_response['data']:
                        if json_response['data']['variants_info']:
                            check_variants_present = True
                        else:
                            search_unit_flag = True
                    
                    is_same = False
                    if check_variants_present:
                        if units == orig_quan_num:
                            search_unit_flag = True
                            is_same = True
                    
                    if units and check_variants_present and not is_same:
                        changing_unit = False
                        if not any(item in orig_quan_num for item in units):
                            for variants in json_response['data']['variants_info']:
                                if 'unit' in variants:
                                    cc = re.findall(r"[-+]?\d*\.\d+|\d+", variants['unit'])
                                    if any(item in cc for item in units):
                                        orig_quan = variants['unit']
                                        if 'mrp' in variants:
                                            mrp = variants['mrp']
                                        if 'price' in variants:
                                            selling_price = variants['price']
                                        changing_unit = True
                                        search_unit_flag = False
                                        break
                        if not changing_unit:
                            search_unit_flag = True

                    if not units:
                        search_unit_flag = True
                    
                    if search_unit_flag:
                        if 'mrp' in json_response['data']['product']:
                            mrp = json_response['data']['product']['mrp']
                        if 'price' in json_response['data']['product']:
                            selling_price = json_response['data']['product']['price']

                    if 'brand' in json_response['data']['product']:
                        brand = json_response['data']['product']['brand']
                    comments = ''
    else:
        print ('Json Response is None!')
    if comments == '':
        print (f'\t   {product_name} | {orig_quan} | {mrp} | {selling_price} | {brand}')
    else:
        print (f'\t   {comments}')
    return product_name, orig_quan, mrp, selling_price, brand, comments

def take_screenshot(driver, save_as):
    print(f"Taking Screenshot.. -> {save_as}")
    driver.get_screenshot_as_file(save_as)

def visit_url(driver, url, product_id, screenshots_dir):
    del driver.requests
    driver.get(url)

    print("Taking Screenshot..")
    take_screenshot(driver, f"{screenshots_dir}/grofers_{product_id}.png")


    prod_name_xpath = '//*[@id="app"]/div/div[2]/div[5]/div/div/div[2]/div[1]/div/div/div[1]/div[2]'
    try:
        WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.XPATH, prod_name_xpath)))
    except TimeoutException as ex:
        pass
 
    url_response = None
    for request in driver.requests:
        #print (request)
        if request.response:
            if (request.url.startswith('https://grofers.com/v6/merchant/')) and (request.url.endswith(str(product_id) + '/') or request.url.endswith(str(product_id) + '?current_screen=pdp')):
                url_response = request.response.body
                #print (url_response)
                break

    if url_response is not None:
        try:
            #print (f'Before converting - {type(url_response)}\n\n{url_response}\n')
            #url_response_decoded = url_response.decode("utf-8")
            #print(json.detect_encoding(url_response))
            url_response_decoded = codecs.decode(url_response, 'UTF-8', errors='ignore')
            #print (f'After converting - {type(url_response_decoded)}\n\n{url_response_decoded}\n')
            if 'You are being rate limited' in url_response_decoded:
                print ('Too many simultaneous requests, sleeping for 5 minutes.')
                time.sleep(300)
                url_response = None
                return url_response
            else:
                new_str = url_response_decoded
                #print ((new_str))
                json_response = json.loads(new_str)
                #print (json_response)
                return json_response
        except Exception as err:
            print ('Error: ' + str(err))
            return None
    else:
        return url_response

def main():
    crawl_time = datetime.datetime.now().strftime('%d-%m-%y-%H:%M:%S')
    global chromedriver_path
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', type=argparse.FileType("r"), help="Input CSV file")
    parser.add_argument('-o', '--output', type=str, help="Output Directory")
    parser.add_argument('-w', '--website', type=str, help="CSV Column Name")
    parser.add_argument('-l', '--location', type=str, help="Gofers Location")
    parser.add_argument('--headless', help="Browser headless or not (True/False). Default is True", default=True)

    args = parser.parse_args()

    if sys.platform == 'win32':
        chromedriver_path = "chromedriver.exe"
    elif 'linux' in sys.platform:
        chromedriver_path = "/usr/bin/chromedriver"
        # chromedriver_path = "./chromedriver/chromedriver"
    else:
        print(f'Unknown OS, Exitting..')
        sys.exit()

    headless = False if args.headless == "False" else True
    
    read_csv(args.input, args.output, args.website, args.location, headless, crawl_time)

# grofers.py -i grofers.csv -o C:\Users\CTI\Desktop\bb -w WEBSITE_LINK -l bangalore --headless False --OS windows
if __name__ == "__main__":
    start_time = time.time()
    main()
