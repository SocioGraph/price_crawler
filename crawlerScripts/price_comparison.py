import argparse, glob, os, csv, chardet, pytz
import pandas as pd
from datetime import datetime, timezone
from UliPlot.XLSX import auto_adjust_xlsx_column_width

margin = {
"KVI": {"GROCERY": "3%", "FMCG FOODS": "7%", "FRUIT AND VEG": "3%", "DAIRY AND FROZEN": "3%", "FISH AND MEAT": "5%", "FMCG N FOODS": "7%"},
"PSS": {"GROCERY": "5%", "FMCG FOODS": "7%", "FRUIT AND VEG": "3%", "DAIRY AND FROZEN": "5%", "FISH AND MEAT": "5%", "FMCG N FOODS": "7%"}
}

def get_crawled_price_csv_list(cralwled_csvs_directory):
    csv_files = glob.glob(os.path.join(cralwled_csvs_directory, "*.csv"))
    csv_f_names = []
    if csv_files:
        for file in csv_files:
            if os.path.getsize(file) > 0:   # File size > 0
                abs_path = os.path.abspath(file)
                filename_with_extension = os.path.basename(file)
                filename = os.path.splitext(filename_with_extension)[0]
                if filename.endswith('_out'):
                    csv_f_names.append(abs_path)
    csv_f_names.sort()
    return csv_f_names

def get_date():
    now = datetime.now(timezone.utc)
    if now.hour < 7:
        folder_name = f'{now.strftime("%d_%b_%Y")}_Morning'
    else:
        folder_name = f'{now.strftime("%d_%b_%Y")}_Evening'
    return folder_name

def write_to_excel_csv(df, out_dir):
    date = get_date()
    with pd.ExcelWriter(f'{out_dir}//Combined_Report_{date}.xlsx') as writer:
        df.to_excel(writer, sheet_name="Combined_Report", index=False)
        auto_adjust_xlsx_column_width(df, writer, sheet_name="Combined_Report", margin=0.5, index=False)
        print (f'\nReport saved to {out_dir}//Combined_Report_{date}.xlsx')
    df.to_csv(f'{out_dir}//Combined_Report_{date}.csv', index=None)
    print (f'\nReport saved to {out_dir}//Combined_Report_{date}.csv')

def combain_csv_files(csv_f_names):
    
    # Identifying encoding scheme of each csv file
    csv_f_names_with_encoding = {}
    for f_name in csv_f_names:
        with open(f_name, 'rb') as rawdata:
            data_encoding = chardet.detect(rawdata.read(100000))
            csv_f_names_with_encoding[f_name] = data_encoding['encoding']
    
    # Concatenate csv files into a dataframe
    df_concat = pd.concat([pd.read_csv(f_name, encoding = encoding_scheme) for f_name, encoding_scheme in csv_f_names_with_encoding.items()], ignore_index=True)

    df_concat = df_concat.drop(['PRODUCT_NAME', 'QUANTITY', 'PRICE', 'BRAND', 'WEBSITE_LINK', 'Comments', 'Standard_UOM'], axis=1)

    df_concat['Normalization'] = df_concat['Normalization'].fillna(1)   # fill NaN with integer 1
    df_concat['Normalization'] = df_concat['Normalization'].astype(str) # Convert Normalization column type to str
    df_concat['Normalization'] = df_concat['Normalization'].str.replace('*', '')    # Remove * from Normalization column cells
    df_concat['Normalization'] = df_concat['Normalization'].astype(float)   # Convert Normalization column type to float
    
    #df_concat['Norm_Float'] = df_concat['Normalization'].str[1:]
    #df_concat['Norm_Float'] = df_concat['Normalization']
    #df_concat['Norm_Float'] = pd.to_numeric(df_concat['Norm_Float'])

    df_concat["SELLING_PRICE"] = df_concat["SELLING_PRICE"]*df_concat['Normalization']  # Selling Price = Selling Price * Normalization

    df_concat = df_concat[['REGION', 'Store Name', 'Store Code', 'Pincode', 'SKU TYPE', 'Type', 'Category', 'Sub_Category', 'ITEM CODE', 'ITEM_DESC', 'UOM', 'Margin', 'Spar Price', 'SELLING_PRICE', 'Competitor']]
    
    df_concat.rename(columns = {'Store Name':'STORE NAME', 'Store Code': 'STORE CODE', 'Pincode':'PINCODE', 'Category':'CATEGORY', 'Sub_Category':'SUB CATEGORY', 'ITEM_DESC':'ITEM', 'Margin':'MARGIN', 'Spar Price':'SPAR PRICE', 'SELLING_PRICE':'COMPETITOR PRICE', 'Competitor':'COMPETITOR', 'Suggested Price':'SUGGESTED PRICE'}, inplace = True) # Renaming Column Names
    
    df_concat.loc[df_concat['SKU TYPE'] == 'PSS', 'SUGGESTED PRICE'] = df_concat['COMPETITOR PRICE']
    df_concat.loc[(df_concat['SKU TYPE'] == 'KVI') & (df_concat['SUB CATEGORY'] == 'Edible Oil / Fats'),'SUGGESTED PRICE'] = df_concat['COMPETITOR PRICE'] - 0.25
    df_concat.loc[(df_concat['SKU TYPE'] == 'KVI') & (df_concat['SUB CATEGORY'] != 'Edible Oil / Fats'), 'SUGGESTED PRICE'] = df_concat['COMPETITOR PRICE'] - df_concat['COMPETITOR PRICE']*3/100
    
    formatted_time = datetime.now(pytz.timezone('Asia/Kolkata')).strftime("%Y-%m-%d %I:%M:%S %p %z")
    df_concat['SYNC TIME'] = ''
    df_concat.at[0, 'SYNC TIME'] = formatted_time

    return df_concat

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--directory', required=True, help="Directory path containing crawled price CSV files")
    parser.add_argument('-o', '--output', help="Output directory to save report", default = ".")
    args = parser.parse_args()
    
    csv_f_names = get_crawled_price_csv_list(args.directory)
    print (f'\nFound {len(csv_f_names)} csv files')
    df = combain_csv_files(csv_f_names)
    print (f'\nTotal Rows: {df.shape[0]}')
    write_to_excel_csv(df, args.output)

if __name__ == "__main__":
    main()
