import argparse, sys, csv, time, re, random, os
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import os, datetime

product_title_id = 'productTitle'
availability_id = 'availability'
sell_price_id = 'priceblock_ourprice'
sell_price_multiple_xpath = '//*[@id="olp_feature_div"]/div[2]/span/a/span[2]'
deal_price_id = 'priceblock_dealprice'
mrp_xpath = '//*[@id="price"]/table/tbody/tr[1]/td[2]/span[1]'
product_info_id = 'productDetails_techSpec_section_1'
product_details_id = 'detailBullets_feature_div'
product_overview_id = 'productOverview_feature_div'
additional_product_details_id = 'productDetails_db_sections'

def highlight(element):
    driver = element._parent
    def apply_style(s):
        driver.execute_script("arguments[0].setAttribute('style', arguments[1]);",
                              element, s)
    original_style = element.get_attribute('style')
    apply_style("background: yellow; border: 2px solid red;")
    time.sleep(.5)
    apply_style(original_style)

def set_location(driver, inputlocation): 
    DELAY = 20
    select_address_id = 'glow-ingress-line2'
    pincode_id = 'GLUXZipUpdateInput'
    
    driver.get('https://amazon.in/')
    
    select_address = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.ID, select_address_id)))
    highlight(select_address)
    select_address.click()
    
    input_pincode = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.ID, pincode_id)))
    highlight(input_pincode)
    input_pincode.clear()
    input_pincode.send_keys(inputlocation)
    input_pincode.send_keys(Keys.ENTER)
    
    start = time.time()
    while True:
        try:
            get_location = WebDriverWait(driver, DELAY).until(EC.presence_of_element_located((By.ID, select_address_id)))
            if 'Select your address' != get_location.text.strip():
                print (f'\nSelected Location: {get_location.text.strip()}')
                return get_location.text.strip()
        except Exception:
            pass
        if (time.time() - start) > 15:
            break

def read_csv(input_csv, path, website_name, location_name, inputlocation, headless, chromedriver_path, crawl_time):
    with input_csv:
        csv_reader = csv.reader(input_csv, delimiter=',')
        with open(path+"//"+website_name+"_"+location_name+".csv", 'w', newline='') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(['item_desc','product_name','quantity','price','selling_price', 'brand','website_link', 'comments'])
            line_count = 0
            links_count = 0
            is_headless = 'No'
            for row in csv_reader:
                if line_count == 0:
                    website_name_flag = False
                    item_desc_flag = False
                    column_index = 0
                    url_index = 0
                    item_desc_index = 0
                    for column_name in row:
                        if column_name.lower() == f"{website_name.lower()} url":
                            website_name_flag = True
                            url_index = column_index
                            chrome_options = webdriver.ChromeOptions()
                            chrome_options.add_argument("--window-size=1920,1080")
                            chrome_options.add_argument("--start-maximized")
                            chrome_options.add_argument('--ignore-certificate-errors')
                            chrome_options.add_argument('--ignore-ssl-errors')
                            chrome_options.add_argument('log-level=3')
                            if headless:
                                is_headless = 'Yes'
                                chrome_options.add_argument('--headless')
                                chrome_options.add_argument('--no-sandbox')
                                driver = webdriver.Chrome(executable_path = chromedriver_path, options=chrome_options)
                            else:
                                driver = webdriver.Chrome(executable_path = chromedriver_path, options=chrome_options)
                            user_location = set_location(driver, inputlocation)

                        elif column_name.lower() == 'item_desc':
                            item_desc_index = column_index
                            item_desc_flag = True
                        
                        if website_name_flag and item_desc_flag:
                            break

                        column_index += 1
                    if not website_name_flag:
                        print (f'No matching website column name found')
                        sys.exit()
                else:
                    #print(len(row), url_index)
                    #print(row[url_index])
                    prod_desc = row[item_desc_index]
                    if row[url_index].startswith('https://www.amazon.in'):
                        units = re.findall(r"[-+]?\d*\.\d+|\d+", prod_desc)
                        units.sort()

                        screenshots_dir = f"./screenshots/{inputlocation}/{crawl_time}"
                        #if not os.path.isdir(screenshots_dir):
                        #    os.makedirs(screenshots_dir, exist_ok=True)

                        product_name, orig_quan, mrp, selling_price, brand, comments = read_amazon(prod_desc, row[url_index].split('/ref=')[0], units, driver, screenshots_dir)
                        csvwriter.writerow([prod_desc, product_name, orig_quan, mrp, selling_price, brand, row[url_index], comments])
                        links_count += 1
                        sleep_time = random.randint(1, 10)
                        print (f'\t      Sleeping {sleep_time} sec')
                        time.sleep(sleep_time)
                    else:
                        csvwriter.writerow([prod_desc, '', '', '', '', '', row[url_index], ''])
                line_count += 1
                csvfile.flush()
            if links_count > 0:
                minutes , seconds = divmod((time.time() - start_time), 60)
                print (f'\nInput      : {input_csv.name}')
                print   (f'Output     : {path}\{website_name}.csv')
                print   (f'Website    : Amazon')
                print   (f'Location   : {user_location}')
                print   (f'Links      : {links_count}')
                print   (f'Headless   : {is_headless}')
                print   (f'Time Taken : {int(minutes)}.{int(seconds)} minutes')
            driver.quit()

def take_screenshot(driver, save_as):
    print(f"Taking Screenshot.. -> {save_as}")
    driver.get_screenshot_as_file(save_as)

def read_amazon(prod_desc, url, units, driver, screenshots_dir):
    product_name = orig_quan = mrp = selling_price = brand = comments = ''

    print (f'{prod_desc}')
    print (f'\t {url}')
    driver.get(url)

    #take_screenshot(driver, f"./{screenshots_dir}/amazon_{prod_desc}.png")
    
    try:
        prod_name_parse = WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.ID, product_title_id)))
        product_name = prod_name_parse.text
    except Exception:
        product_name = ''
    
    if len(product_name) > 0:
        check_availibility = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, availability_id)))
        if 'Currently unavailable' not in check_availibility.text:
            try:
                selling_price_parse = driver.find_element_by_id(sell_price_id)
                selling_price = selling_price_parse.text.replace('₹', '').strip()
            except Exception:
                try:
                    selling_price_parse = WebDriverWait(driver, 2).until(EC.presence_of_element_located((By.XPATH, sell_price_multiple_xpath)))
                    selling_price = selling_price_parse.text.replace('₹', '').strip()
                except Exception:
                    try:
                        selling_price_parse = WebDriverWait(driver, 2).until(EC.presence_of_element_located((By.ID, deal_price_id)))
                        selling_price = selling_price_parse.text.replace('₹', '').strip()
                    except Exception:
                        pass

            try:
                mrp_parse = WebDriverWait(driver, 2).until(EC.presence_of_element_located((By.XPATH, mrp_xpath)))
                mrp = mrp_parse.text.replace('₹', '').strip()
            except Exception as err:
                pass
        else:
            comments = 'Currently unavailable'    

        brand_flag = orig_quan_flag = False
        try:     
            product_overview = WebDriverWait(driver, 2).until(EC.presence_of_element_located((By.ID, product_overview_id)))
            product_overview_lines = product_overview.text.splitlines()
            for each_overview in product_overview_lines:
                if each_overview.startswith('Brand '):
                    brand = each_overview.replace('Brand ', '')
                    brand_flag = True
                if each_overview.startswith('Weight	'):
                    orig_quan = each_overview.replace('Weight ', '')
                    orig_quan_flag = True
                if brand_flag and orig_quan_flag:
                    break
        except Exception:
            pass
        
        try:
            prod_info = driver.find_element_by_id(product_info_id) # Technical Details
            prod_info_lines = prod_info.text.splitlines()
        except Exception:
            try:
                prod_info = driver.find_element_by_id(product_details_id)
                prod_info_lines = prod_info.text.splitlines()
            except Exception:
                prod_info_lines = ''        
        
        if len(prod_info_lines) > 0:
            orig_quan_flag_1 = orig_quan_flag_2 = brand_flag_1 = False
            if orig_quan_flag:
                orig_quan_flag_1 = orig_quan_flag_2 = True
            if brand_flag:
                brand_flag_1 = True
            
            for each_info in prod_info_lines:
                if orig_quan_flag_1 and orig_quan_flag_2 and brand_flag_1:
                    break
                    
                if each_info.startswith('Net Quantity'):
                    if each_info.startswith('Net Quantity ‏ : ‎ ') or each_info.startswith('Net Quantity : '):
                        orig_quan_1 = each_info.replace('Net Quantity ‏ : ‎ ', '').replace('Net Quantity : ', '')
                    else:
                        orig_quan_1 = each_info.replace('Net Quantity ', '')
                    orig_quan_list_1 = re.findall(r"[-+]?\d*\.\d+|\d+", orig_quan_1)
                    orig_quan = orig_quan_1
                    if any(item in orig_quan_list_1 for item in units):                
                        orig_quan_flag_1 = orig_quan_flag_2 = True
                
                elif each_info.startswith('Weight'):
                    orig_quan_2 = each_info.replace('Weight ', '')
                    orig_quan_list_2 = re.findall(r"[-+]?\d*\.\d+|\d+", orig_quan_2)
                    orig_quan = orig_quan_2
                    if any(item in orig_quan_list_2 for item in units):
                        orig_quan_flag_1 = orig_quan_flag_2 = True
                
                elif each_info.startswith('Item Weight ‏ : ‎ '):
                    orig_quan_2 = each_info.replace('Item Weight ‏ : ‎ ', '')
                    orig_quan_list_2 = re.findall(r"[-+]?\d*\.\d+|\d+", orig_quan_2)
                    orig_quan = orig_quan_2
                    if any(item in orig_quan_list_2 for item in units):                
                        orig_quan_flag_1 = orig_quan_flag_2 = True
                        
                elif each_info.startswith('Brand') and not brand_flag_1:
                    brand = each_info.replace('Brand ', '')
                    brand_flag_2 = True
        
        if orig_quan == '':
            orig_quan_flag_1 = orig_quan_flag_2 = False
            try:
                additional_details = WebDriverWait(driver, 2).until(EC.presence_of_element_located((By.ID, additional_product_details_id))) # Additional Information
                additional_details_lines = additional_details.text.splitlines()
            except Exception:
                additional_details_lines = ''
            
            if len(additional_details_lines) > 0:
                for each_addi_detail in additional_details_lines:
                    if each_addi_detail.startswith('Item Weight'):
                        orig_quan_1 = each_addi_detail.replace('Item Weight ', '')
                        orig_quan_list_1 = re.findall(r"[-+]?\d*\.\d+|\d+", orig_quan_1)
                        orig_quan = orig_quan_1
                        if any(item in orig_quan_list_1 for item in units):
                            orig_quan_flag_1 = orig_quan_flag_2 = True
                    elif each_addi_detail.startswith('Net Quantity'):
                        orig_quan_2 = each_addi_detail.replace('Net Quantity ', '')
                        orig_quan_list_2 = re.findall(r"[-+]?\d*\.\d+|\d+", orig_quan_2)
                        orig_quan = orig_quan_2
                        if any(item in orig_quan_list_2 for item in units):
                            orig_quan_flag_1 = orig_quan_flag_2 = True
                    if orig_quan_flag_1 and orig_quan_flag_2:
                        break

    else:
        comments = 'Product NOT found'
    
    if comments != 'Product NOT found':
        print (f'\t   {product_name} | {orig_quan} | {mrp} | {selling_price} | {brand} | {comments}')
    else:
        print (f'\t   {comments}')
    return product_name, orig_quan, mrp, selling_price, brand, comments

def main():
    crawl_time = datetime.datetime.now().strftime('%d-%m-%y-%H:%M:%S')
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', type=argparse.FileType("r"), help="Input CSV file")
    parser.add_argument('-o', '--output', type=str, help="Output Directory")
    parser.add_argument('-w', '--website', type=str, help="CSV Column Name")
    parser.add_argument('-l', '--location', type=str, help="Amazon Location")
    parser.add_argument('-H', '--headless', help="Run Chrome headless or not", default=True)
    args = parser.parse_args()
    headless = False if args.headless == "False" else True
    
    if sys.platform == 'win32':
        chromedriver_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "chromedriver.exe")
    elif 'linux' in sys.platform:
        chromedriver_path = "/usr/bin/chromedriver"
    else:
        print(f'Unknown OS, Exitting..')
        sys.exit()
    
    check_location = False
    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), "cities_pincode.csv"), 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            city = row[0]
            pincode = row[1]
            if args.location.lower() == city.lower():
                inputpincode = pincode
                check_location = True
                break
    if not check_location:
        print (f'Location NOT found in cities_pincode.csv, Exitting..')
        sys.exit()

    read_csv(args.input, args.output, args.website, args.location.lower(), inputpincode, headless, chromedriver_path, crawl_time)

if __name__ == "__main__":
    start_time = time.time()
    main()

