import csv, time, sys
import pandas as pd
from PIL import Image
from jiomart_otr import read_jiomart, set_location
import openpyxl
from openpyxl.drawing.image import Image

input_csv_file = sys.argv[1]

output_file = f"{input_csv_file.rsplit('/')[-1].split('.')[0]}_output"

set_location('110018')

with open(input_csv_file, mode='r') as input_file:
    csv_reader = csv.DictReader(input_file)

    fieldnames = csv_reader.fieldnames
    
    if 'Jio Product Title' not in fieldnames:   fieldnames.append('Jio Product Title')
    if 'Jio Product Highlights' not in fieldnames:   fieldnames.append('Jio Product Highlights')
    if 'Jio Product Description' not in fieldnames:   fieldnames.append('Jio Product Description')
    if 'Jio Product Information' not in fieldnames:   fieldnames.append('Jio Product Information')
    if 'Jio Brand' not in fieldnames:   fieldnames.append('Jio Brand')
    if 'Jio Manufacturer' not in fieldnames:   fieldnames.append('Jio Manufacturer')
    if 'Jio Manufacturer Address' not in fieldnames:   fieldnames.append('Jio Manufacturer Address')
    if 'Jio Manufacturer Email' not in fieldnames:   fieldnames.append('Jio Manufacturer Email')
    if 'Jio Manufacturer Website' not in fieldnames:   fieldnames.append('Jio Manufacturer Website')
    if 'Jio Sold By' not in fieldnames:   fieldnames.append('Jio Sold By')
    if 'Jio Customer Care Email' not in fieldnames:   fieldnames.append('Jio Customer Care Email')
    if 'Jio Customer Care Phone' not in fieldnames:   fieldnames.append('Jio Customer Care Phone')
    if 'Jio Food Type' not in fieldnames:   fieldnames.append('Jio Food Type')
    if 'Jio Country of Origin' not in fieldnames:   fieldnames.append('Jio Country of Origin')
    if 'Jio Quantity' not in fieldnames:   fieldnames.append('Jio Quantity')
    if 'Jio Net Quantity' not in fieldnames:   fieldnames.append('Jio Net Quantity')
    if 'Jio Net Weight' not in fieldnames:   fieldnames.append('Jio Net Weight')
    if 'Jio Article ID' not in fieldnames:   fieldnames.append('Jio Article ID')
    #if 'Screenshot' not in fieldnames:   fieldnames.append('Screenshot')
    
    #fieldnames.append('Screenshot')
    url_list = ['url' + str(i) for i in range(1, 16)]
    fieldnames.extend(url_list)
    
    modified_rows = []
    error_rows = []

    key_set = set()
    for num, row in enumerate(csv_reader):
            jiomart_url = row.get("url")
            if jiomart_url:
                if jiomart_url.startswith('jiomart.com'):
                    jiomart_url = 'https://www.' + jiomart_url
                if jiomart_url.startswith('https://www.jiomart.com/p'):
                    #jiomart_url = 'https://www.jiomart.com/p/groceries/closeup-everfresh-red-hot-gel-toothpaste-150-g-pack-of-4/604515794'  # For testing purposes
<<<<<<< HEAD
<<<<<<< HEAD
=======
>>>>>>> a08e14c54356e11bd5bc119204c4bfdd6b116611
                    try:
                        result = read_jiomart(jiomart_url)
                        #time.sleep(300)    # For testing purposes
                        row['Jio Product Title'] = result['Jio Product Title']
                        row['Jio Product Highlights'] = result['Jio Product Highlights']
                        if result['Jio Product Description']:
                            row['Jio Product Description'] = result['Jio Product Description'].split('Disclaimer:', 1)[0]
                        row['Jio Product Information'] = result['Jio Product Information']
                        row['Jio Brand'] = result['Jio Brand']
                        row['Jio Manufacturer'] = result['Jio Manufacturer']
                        row['Jio Manufacturer Address'] = result['Jio Manufacturer Address']
                        row['Jio Manufacturer Email'] = result['Jio Manufacturer Email']
                        row['Jio Manufacturer Website'] = result['Jio Manufacturer Website']
                        row['Jio Sold By'] = result['Jio Sold By']
                        row['Jio Customer Care Email'] = result['Jio Customer Care Email']
                        row['Jio Customer Care Phone'] = result['Jio Customer Care Phone']
                        row['Jio Food Type'] = result['Jio Food Type']
                        row['Jio Country of Origin'] = result['Jio Country of Origin']
                        row['Jio Quantity'] = result['Jio Quantity']
                        row['Jio Net Quantity'] = result['Jio Net Quantity']
                        row['Jio Net Weight'] = result['Jio Net Weight']
<<<<<<< HEAD
=======
                        row['Jio Article ID'] = result['Jio Article ID']
                        #print (row)
>>>>>>> a08e14c54356e11bd5bc119204c4bfdd6b116611
                        #row['Screenshot'] = result['Screenshot']

                        for key in row:
                            if key in [f'url{i}' for i in range(1, 16)]:
                                row[key] = result['Jio Image URLs'].pop(0) if result['Jio Image URLs'] else None

                        if result['Jio Product Title']:
                            print (f"\t\t\t{num+1} - {result['Jio Product Title']}")
                            num += 1
<<<<<<< HEAD
                            #if num > 3:  break   # For testing purposes
=======
                            #if num > 30:  break   # For testing purposes
>>>>>>> a08e14c54356e11bd5bc119204c4bfdd6b116611
                        if result['table_data']:
                            for key in result['table_data']:
                                key_set.add(key)
                        modified_rows.append(row)
                    except Exception as err:
                        print (f"\t\t\tERROR retrieving data")
                        error_rows.append(row)
<<<<<<< HEAD
=======
                    #try:
                    result = read_jiomart(jiomart_url)
                    #time.sleep(300)    # For testing purposes
                    row['Jio Product Title'] = result['Jio Product Title']
                    row['Jio Product Highlights'] = result['Jio Product Highlights']
                    if result['Jio Product Description']:
                        row['Jio Product Description'] = result['Jio Product Description'].split('Disclaimer:', 1)[0]
                    row['Jio Product Information'] = result['Jio Product Information']
                    row['Jio Brand'] = result['Jio Brand']
                    row['Jio Manufacturer'] = result['Jio Manufacturer']
                    row['Jio Manufacturer Address'] = result['Jio Manufacturer Address']
                    row['Jio Manufacturer Email'] = result['Jio Manufacturer Email']
                    row['Jio Manufacturer Website'] = result['Jio Manufacturer Website']
                    row['Jio Sold By'] = result['Jio Sold By']
                    row['Jio Customer Care Email'] = result['Jio Customer Care Email']
                    row['Jio Customer Care Phone'] = result['Jio Customer Care Phone']
                    row['Jio Food Type'] = result['Jio Food Type']
                    row['Jio Country of Origin'] = result['Jio Country of Origin']
                    row['Jio Quantity'] = result['Jio Quantity']
                    row['Jio Net Quantity'] = result['Jio Net Quantity']
                    row['Jio Net Weight'] = result['Jio Net Weight']
                    row['Jio Article ID'] = result['Jio Article ID']
                    #print (row)
                    #row['Screenshot'] = result['Screenshot']

                    for key in row:
                        if key in [f'url{i}' for i in range(1, 16)]:
                            row[key] = result['Jio Image URLs'].pop(0) if result['Jio Image URLs'] else None

                    if result['Jio Product Title']:
                        print (f"\t\t\t{num+1} - {result['Jio Product Title']}")
                        num += 1
                        #if num > 30:  break   # For testing purposes
                    if result['table_data']:
                        for key in result['table_data']:
                            key_set.add(key)
                    modified_rows.append(row)
                    #except Exception as err:
                    #    print (f"\t\t\tERROR retrieving data")
                    #    error_rows.append(row)
=======
>>>>>>> a08e14c54356e11bd5bc119204c4bfdd6b116611
            else:
                modified_rows.append(row)
>>>>>>> fb1a92d16eb8d4aa1b639b3f5408f6c87f21357c


df = pd.DataFrame(modified_rows, columns=fieldnames)
df = df.applymap(lambda x: x.encode('unicode_escape').decode('utf-8') if isinstance(x, str) else x) # escapes the unicode characters if they exist
output_excel_file = f"{output_file}.xlsx"

with pd.ExcelWriter(output_excel_file, engine='openpyxl') as writer:
    df.to_excel(writer, sheet_name=output_file, index=False)

    workbook = writer.book
    worksheet = writer.sheets[output_file]
    
    '''
    num_rows = df.shape[0]
    for i in range(num_rows):
        image_path = df.at[i, 'Screenshot']
        if image_path:

            img = Image(image_path)
            img.width = 100  # Adjust the image width as needed
            img.height = 100  # Adjust the image height as needed

            cell = worksheet.cell(row=i + 2, column=fieldnames.index('Screenshot') + 1)
            worksheet.add_image(img, cell.coordinate)
            
            cell.value = ""
    '''
    workbook.save(output_excel_file)
    print (f"\nSaved to {output_excel_file}")

if error_rows:
    df = pd.DataFrame(error_rows, columns=fieldnames)
    error_csv_file = f"{input_csv_file.rsplit('/')[-1].split('.')[0]}_error.csv"
    df.to_csv(error_csv_file, index=False)
    print(f"\n{len(error_rows)} URLs encountered error:\n\tSaved to {error_csv_file}")
else:
    print(f"\nNo URLs encountered any unexpected errors")


