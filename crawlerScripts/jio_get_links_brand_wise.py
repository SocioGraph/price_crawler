import csv, time
import pandas as pd
import argparse, sys, csv
from PIL import Image
import openpyxl
from openpyxl.drawing.image import Image
from jiomart_otr import *
import traceback

set_location('110018')

# input_csv_file = sys.argv[1]

urls = ["https://www.jiomart.com/search/NAVNEET?prod_mart_master_vertical%5BhierarchicalMenu%5D%5Bcategory_tree.level0%5D%5B0%5D=Category&prod_mart_master_vertical%5BrefinementList%5D%5Bbrand%5D%5B0%5D=Navneet",
"https://www.jiomart.com/search/ONGS?prod_mart_master_vertical%5BhierarchicalMenu%5D%5Bcategory_tree.level0%5D%5B0%5D=Category&prod_mart_master_vertical%5BrefinementList%5D%5Bbrand%5D%5B0%5D=Ong%27s",
"https://www.jiomart.com/search/FLYING%20BIRD?prod_mart_master_vertical%5BhierarchicalMenu%5D%5Bcategory_tree.level0%5D%5B0%5D=Category&prod_mart_master_vertical%5BrefinementList%5D%5Bbrand%5D%5B0%5D=Flying%20Bird",
"https://www.jiomart.com/search/AYUSH?prod_mart_master_vertical%5BhierarchicalMenu%5D%5Bcategory_tree.level0%5D%5B0%5D=Category&prod_mart_master_vertical%5BrefinementList%5D%5Bbrand%5D%5B0%5D=Ayush",
"https://www.jiomart.com/search/COKE?prod_mart_master_vertical%5BhierarchicalMenu%5D%5Bcategory_tree.level0%5D%5B0%5D=Category&prod_mart_master_vertical%5BrefinementList%5D%5Bbrand%5D%5B0%5D=Coca%20Cola",
"https://www.jiomart.com/search/COLOMBIAN%20BREW?prod_mart_master_vertical%5BhierarchicalMenu%5D%5Bcategory_tree.level0%5D%5B0%5D=Category&prod_mart_master_vertical%5BrefinementList%5D%5Bbrand%5D%5B0%5D=Colombian%20Brew",
"https://www.jiomart.com/search/VEET?prod_mart_master_vertical%5BhierarchicalMenu%5D%5Bcategory_tree.level0%5D%5B0%5D=Category&prod_mart_master_vertical%5BrefinementList%5D%5Bbrand%5D%5B0%5D=Veet",
"https://www.jiomart.com/search/TOY%20MAXX?prod_mart_master_vertical%5BhierarchicalMenu%5D%5Bcategory_tree.level0%5D%5B0%5D=Category&prod_mart_master_vertical%5BrefinementList%5D%5Bbrand%5D%5B0%5D=TOYMAXX",
"https://www.jiomart.com/search/SLEEPY%20OWL?prod_mart_master_vertical%5BhierarchicalMenu%5D%5Bcategory_tree.level0%5D%5B0%5D=Category&prod_mart_master_vertical%5BrefinementList%5D%5Bbrand%5D%5B0%5D=Sleepy%20Owl",
"https://www.jiomart.com/search/VBE?prod_mart_master_vertical%5BhierarchicalMenu%5D%5Bcategory_tree.level0%5D%5B0%5D=Category&prod_mart_master_vertical%5BrefinementList%5D%5Bbrand%5D%5B0%5D=Vbeee",
"https://www.jiomart.com/search/PRINGLES?prod_mart_master_vertical%5BhierarchicalMenu%5D%5Bcategory_tree.level0%5D%5B0%5D=Category&prod_mart_master_vertical%5BrefinementList%5D%5Bbrand%5D%5B0%5D=Pringles",
"https://www.jiomart.com/search/RAGU?prod_mart_master_vertical%5BhierarchicalMenu%5D%5Bcategory_tree.level0%5D%5B0%5D=Category&prod_mart_master_vertical%5BrefinementList%5D%5Bbrand%5D%5B0%5D=Ragu",
"https://www.jiomart.com/search/MINIMALIST?prod_mart_master_vertical%5BhierarchicalMenu%5D%5Bcategory_tree.level0%5D%5B0%5D=Category&prod_mart_master_vertical%5BrefinementList%5D%5Bbrand%5D%5B0%5D=Minimalist",
"https://www.jiomart.com/search/MATRIKAS?prod_mart_master_vertical%5Bpage%5D=2&prod_mart_master_vertical%5BhierarchicalMenu%5D%5Bcategory_tree.level0%5D%5B0%5D=Category&prod_mart_master_vertical%5BrefinementList%5D%5Bbrand%5D%5B0%5D=MATRIKA",
"https://www.jiomart.com/search/MLESANA?prod_mart_master_vertical%5BhierarchicalMenu%5D%5Bcategory_tree.level0%5D%5B0%5D=Category&prod_mart_master_vertical%5BrefinementList%5D%5Bbrand%5D%5B0%5D=Mlsena",
"https://www.jiomart.com/search/MR.MUSCLE?prod_mart_master_vertical%5BhierarchicalMenu%5D%5Bcategory_tree.level0%5D%5B0%5D=Category&prod_mart_master_vertical%5BrefinementList%5D%5Bbrand%5D%5B0%5D=Mr%20Muscle",
"https://www.jiomart.com/search/MINUTE%20MAID?prod_mart_master_vertical%5BhierarchicalMenu%5D%5Bcategory_tree.level0%5D%5B0%5D=Category&prod_mart_master_vertical%5BrefinementList%5D%5Bbrand%5D%5B0%5D=Minute%20Maid",
"https://www.jiomart.com/search/LISTERINE?prod_mart_master_vertical%5BhierarchicalMenu%5D%5Bcategory_tree.level0%5D%5B0%5D=Category&prod_mart_master_vertical%5BrefinementList%5D%5Bbrand%5D%5B0%5D=Listerine",
"https://www.jiomart.com/search/JABSONS?prod_mart_master_vertical%5BhierarchicalMenu%5D%5Bcategory_tree.level0%5D%5B0%5D=Category&prod_mart_master_vertical%5BrefinementList%5D%5Bbrand%5D%5B0%5D=Jabsons",
"https://www.jiomart.com/search/EVEREADY?prod_mart_master_vertical%5BhierarchicalMenu%5D%5Bcategory_tree.level0%5D%5B0%5D=Category&prod_mart_master_vertical%5BrefinementList%5D%5Bbrand%5D%5B0%5D=Eveready",
"https://www.jiomart.com/search/GUJJUBHAI?prod_mart_master_vertical%5BhierarchicalMenu%5D%5Bcategory_tree.level0%5D%5B0%5D=Category&prod_mart_master_vertical%5BrefinementList%5D%5Bbrand%5D%5B0%5D=Gujjubhai",
"https://www.jiomart.com/search/COMPLAN?prod_mart_master_vertical%5BhierarchicalMenu%5D%5Bcategory_tree.level0%5D%5B0%5D=Category&prod_mart_master_vertical%5BrefinementList%5D%5Bbrand%5D%5B0%5D=Complan",
"https://www.jiomart.com/search/%20RS?prod_mart_master_vertical%5BhierarchicalMenu%5D%5Bcategory_tree.level0%5D%5B0%5D=Category&prod_mart_master_vertical%5BrefinementList%5D%5Bbrand%5D%5B0%5D=Rs"]

brand_name = ''

try:
    modified_rows = []
    
    for filtered_url in urls: 
        print(brand_name)
        results = []
        try:
            results = search_products(brand_name, filtered_url, False, True)
            modified_rows.extend(results)
        except:
            print (f"\tUnable to get data")
            traceback.print_exc()
        
    output_file = f"JioMart_Products_{brand_name}"
    fieldnames = ["title","url"]
    df = pd.DataFrame(modified_rows, columns=fieldnames)
    
    output_excel_file = f"{output_file}.xlsx"
    with pd.ExcelWriter(output_excel_file, engine='openpyxl') as writer:
        df.to_excel(writer, sheet_name='JioMart products', index=False)

        workbook = writer.book
        worksheet = writer.sheets['JioMart products']

        num_rows = df.shape[0]

        for i in range(num_rows):
            image_path = False # df.at[i, 'Screenshot']
            if image_path:
                img = Image(image_path)
                img.width = 100  # Adjust the image width as needed
                img.height = 100  # Adjust the image height as needed

                cell = worksheet.cell(row=i + 2, column=fieldnames.index('Screenshot') + 1)
                worksheet.add_image(img, cell.coordinate)

                cell.value = ""

        #workbook.save(output_excel_file)
        #print (f"\nSaved to {output_excel_file}")
        
        output_csv_file = f"{output_file}.csv"
        df.to_csv(output_csv_file, index=False)
        print(f"\nSaved to {output_csv_file}")

except KeyboardInterrupt:
    sys.exit()
    print("KeyboardInterrupt: Ctrl+C was pressed")       
except Exception as e:
    print(f"An exception occurred: {type(e).__name__} - {e}")   
    traceback.print_exc()



