import csv, time
import pandas as pd
import argparse, sys, csv
from PIL import Image
import openpyxl
from openpyxl.drawing.image import Image
from jiomart_otr import *

#set_location('110018')

input_csv_file = sys.argv[1]
output_file = f"{input_csv_file.rsplit('/')[-1].split('.')[0]}_urls"
modified_rows = []
fieldnames = None
try:
    with open(input_csv_file, mode='r') as input_file:
        csv_reader = csv.DictReader(input_file)

        fieldnames = csv_reader.fieldnames
        fieldnames.append("title")
        fieldnames.append("url")

        modified_rows = []
        
        for num, row in enumerate(csv_reader, 1): 
            product_id = row.get("Jio Mart Product ID", None)
            row['title'] = None
            row['url'] = None
            print (f"{num}. {product_id}")
            if product_id:
                results = {}
                try:
                    results = search_products(product_title=None, filtered_url = f'https://www.jiomart.com/search/"{product_id}"', first_product=False, scroll=False)
                    if results:
                        for each_result in results:
                            url = each_result.get('url', None)
                            title = each_result.get('title', None)
                            screenshot = each_result.get('screenshot', None)
                            row['title'] = title
                            row['url'] = url
                            modified_rows.append(row)
                    else:
                        modified_rows.append(row)
                        print (f"\tNo product found")
                except:
                    print (f"\tERROR: Unable to get data")

                #if num>15:  break  # For Testing purposes

except KeyboardInterrupt:
    sys.exit()
    print("KeyboardInterrupt: Ctrl+C was pressed")       
except Exception as e:
    print(f"An exception occurred: {type(e).__name__} - {e}")    


df = pd.DataFrame(modified_rows, columns=fieldnames)
output_csv_file = f"{output_file}.csv"
df.to_csv(output_csv_file, index=False)
print(f"\nSaved to {output_csv_file}")

