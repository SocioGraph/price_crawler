function do_crawling() {
	input_file=$1
	out_dir=$2

	if [ ! -d ./scrapedData ];then
		echo "Creating scraped data dir."
		mkdir ./scrapedData
	fi


	pushd ./crawlerScripts
	for location in bangalore delhi #mangalore chennai hyderabad
	do
		echo "Crawling for region $location."
		for competitor in bigbasket amazon grofers dmart spencers
		do
			echo "Crawling for competitor $competitor."
			python $competitor.py --input $input_file --output $out_dir --location $location --headless True --website $competitor
		done
	done
	popd
}

function create_directory_in_s3() {
	#create dirs in s3
	filename=$1
	echo "Creating directory in s3 - $filename."
	aws s3api put-object --bucket sparinsights.iamdave.ai --key downloadableFiles/$filename/ --acl public-read
}

function main() {
	#activate virtualenv
	source pyenv/bin/activate
	
	#setting variables
	date=`date +'%d-%m-%Y'`
 	generatedFiles=$(pwd)/generatedFiles/$date
	
	#creating dir to store generated files.
	if [ ! -d ./generatedFiles/ ];then
		echo "creating dir generatedFiles."
		mkdir ./generatedFiles
	fi	
	
	#create subdir
	mkdir $generatedFiles
	mkdir $generatedFiles/scrapedData

	#create dir in s3
	create_directory_in_s3 $date
	create_directory_in_s3 $date/scrapedData

	echo "Running the script to process input files."
	python processInputFiles.py --product_file ./inputFiles/$date/64_products_data.csv  --csp_file ./inputFiles/$date/64_products_csp.xlsx --output_dir $generatedFiles
        
	echo "Copying pre-processed files to s3"
	aws s3 sync $generatedFiles/ s3://sparinsights.iamdave.ai/downloadableFiles/$date/ --acl public-read

	echo "Running crawlers."
	do_crawling $generatedFiles/validProducts.csv $generatedFiles/scrapedData/
	
	#Copying crawled files to s3
	#aws s3 cp ./crawlerScripts/scrapedData/ s3://sparinsights.iamdave.ai/downloadableFiles/$date/ --acl public-read

	echo "Running frontend data generator."
	python generateFrontendData.py --GEN_FILES_DIR $generatedFiles/  --SCRAPED_DATA_DIR $generatedFiles/scrapedData/ --TIMESTAMP "$(date '+%d/%m/%y, %H:%M')"

	echo "Copying frontend data file to s3"
	aws s3 cp ./frontend/data_temp.js s3://sparinsights.iamdave.ai/ --acl public-read

	echo "Syncing files to s3 - moving newly generated files."
	aws s3 sync $generatedFiles/ s3://sparinsights.iamdave.ai/downloadableFiles/$date/ --acl public-read

	echo "Force killing all instances of chrome and chromedriver."
	killall chrome
	killall chromedriver
}

echo "Running crawlers..."
main
