import pandas as pd, argparse, sys, csv, os, json, requests

output_dir =  None
competitors = ["amazon", "bigbasket", "grofers", "spencers", "dmart"]

KEY = "key-5029bb836fa6cc7b062b9abcca868301"
DOMAIN = "https://api.mailgun.net/v3/mg.sociographsolutions.in/messages"


def sendEmails(to, content):
    data = {
                "from": "info@iamdave.ai",
                "to": [ 'sreenath@iamdave.ai' ],
                "subject": "This is a test mail, Nothing to panic.",
                "html": content
    
            }
    
    m=requests.post(DOMAIN, auth=('api', KEY), data=data)


def writeToFile(output, outfile, outputHeaders):

    #if not os.path.isdir(outdir):
    #    print("Creating output directory.")
    #    os.mkdir(outdir)

    with open(f"{outfile}.csv", "w") as wf:
        print(f"Writing to CSV file. {outfile}.csv")
        #print(json.dumps(output, indent = 4))
        csv_writer = csv.DictWriter(wf, outputHeaders)
        csv_writer.writeheader()
        csv_writer.writerows(output)


def processProductFile(product_filename):
    validRows = []
    invalidRows = []
    with open(product_filename, "r") as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter = ",")
            line_count = 0

            for row in csv_reader:
                no_url_count = 0
                # print(row)
                if line_count == 0:
                    line_count = line_count +1
                    #print(row.keys())
                else:
                    row = dict(row)
                    if not row["SparIndia URL"]:
                        pass
                        #invalidRows.append(row)
                        #continue

                    if not row["Item Code"]:
                        invalidRows.append(row)
                        continue
                    
                    for competitor in competitors:
                        if row[f"{competitor.title()} URL"].lower() in ["", "unavailable"]:
                            no_url_count = no_url_count + 1

                    if no_url_count == len(competitors):
                        invalidRows.append(row)
                    
                    else:
                        validRows.append(row)
    
    if len(invalidRows) > 0:
        writeToFile(invalidRows, f"{output_dir}/rowsWithNoUrls", invalidRows[0].keys())

    if len(validRows) > 0:
        writeToFile(validRows, f"{output_dir}/validProducts", validRows[0].keys())

    return validRows

def processWithCSP(validRows, csp_rows, location):
    productsWithCSP = []
    for product in validRows:
        for csp in csp_rows:
            #print(product["Item Code"], csp["Item Code"])
            if product["Item Code"] == csp["Item Code"]:
                product["selling_price"] = csp[f"CSP {location}"]
                productsWithCSP.append(product)
                break
            else:
                continue
            

    #print(productsWithCSP)
    #sys.exit()
    return productsWithCSP

def validate_csp_rows(locations):
    empty_price_rows = []
    valid_csp_rows = []
    for location in locations:
        with open(f"{output_dir}/csp_{location}.csv") as fp:
            csp_rows = csv.DictReader(fp, delimiter = ",")
            for row in csp_rows:
                if row[f"CSP {location}"] == "":
                    empty_price_rows.append(row)
                else:
                    valid_csp_rows.append(row)

            if valid_csp_rows:
                writeToFile(valid_csp_rows, f"{output_dir}/valid_csp_{location}", valid_csp_rows[0].keys())
            if empty_price_rows:
                writeToFile(empty_price_rows, f"{output_dir}/empty_price_rows_csp_{location}", empty_price_rows[0].keys())
            valid_csp_rows = []
            empty_price_rows = []

def main(product_filename, csp_filename):
    
    print("Processing CSP File")
    data = pd.ExcelFile(csp_filename)

    locations = data.sheet_names

    for location in locations:
        df = data.parse(location)
        df.to_csv(f"{output_dir}/csp_{location}.csv")

    validate_csp_rows(locations)

    validRows = processProductFile(product_filename)

    for location in locations:
        filename=f"{output_dir}/valid_csp_{location}.csv"
        if os.path.isfile(filename):
            with open(filename) as fp:
                csp_rows = csv.DictReader(fp, delimiter = ",")
                validCSProws = processWithCSP(validRows, list(csp_rows), location)
                writeToFile(validCSProws, f"{output_dir}/valid_products_with_CSP_{location}", validCSProws[0].keys())

    sys.exit()
        

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--product_file', type=str, help="product file")
    parser.add_argument('-c', '--csp_file', type=str, help="CSP file")
    parser.add_argument('-o', '--output_dir', type=str, help="Dir to save generated outputs")

    args = parser.parse_args()
    output_dir = args.output_dir

    if not os.path.isdir(output_dir):
        print("creating output dir.")
        os.mkdir(output_dir)

    main(args.product_file, args.csp_file)
